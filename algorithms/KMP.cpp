#include <iostream>
#include <vector>

using namespace std;

vector <int> create_aux(string key) {
    int i = 1, m = 0;
    vector<int> aux(key.length(), 0);

    while (i < key.length()) {
        if (key[i] == key[m]) {
            m++;
            aux[i] = m;
            i++;
        } 
        else if (m != 0) m = aux[m - 1];
        else aux[i] = 0, i++;
    }

    return aux;
}

int main(){
    string key, word;

    cin >> key >> word;

    vector<int> aux = create_aux(key);

    int i = 0, j = 0;

    while (j < word.length()) {
        if (word[j] != key[i]) {
            if (i == 0) j++;
            else i = aux[i - 1];
        } else {
            i++, j++;
            if (i == key.length()) {
                cout << j - i + 1 << '\n';
                return 0;
            }
        }
    }

    cout << "NOT FOUND\n";
}