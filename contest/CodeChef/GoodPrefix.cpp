#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, x, k, ans;
    string s;
    cin >> t;
    while(t--) {
        cin >> s >> k >> x;
        vector<int> count(27, 0);
        ans = 0;
        for (int i = 0; i < s.length(); i++) {
            count[s[i] - 'a']++;
            if (count[s[i] - 'a'] > x) {
                if (k == 0) break;
                k--;
            } else ans++; 

        }

        cout << ans << '\n';
    }
}