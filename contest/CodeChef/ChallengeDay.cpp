#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
    int t, n, m;
    cin >> t;
    int count = 0, ans = 25 * 25;
    string tmp;

    for(int i = 0; i < t; i++){
        cin >> n >> m;
        vector<vector<char> > grid(n, vector<char>(m)), space(n * 3, vector<char>(m * 3, '0'));
        for(int h = 0; h < n; h++){
            cin >> tmp; 
            for(int w = 0; w < m; w++)
                grid[h][w] = tmp[w];
        }
        for(int h = 0; h < n; h++){
            cin >> tmp; 
            for(int w = 0; w < m; w++)
                space[n + h][m + w] = tmp[w];
        }
        
        count = 0;
        ans = 25 * 25;
        for(int h1 = 0; h1 < 2 * n; h1++){
            for(int w1 = 0; w1 < 2 * m; w1++){
                count = 0;
                for(int h = 0; h < n; h++)
                for(int w = 0; w < m; w++)
                    if(grid[h][w] != space[h1 + h][w1 + w])
                        count++;
                ans = min(ans, count);
            }
        }
        cout << ans << '\n';
    }
    return 0;
}