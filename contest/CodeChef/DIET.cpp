#include <iostream>
#include <vector>

using namespace std;

int main(){
    int t, n, k, balance = 0;

    cin >> t;

    for(int i = 0; i < t; i++){
        cin >> n >> k;
        vector<int> protein(n);
        for(int j = 0; j < n; j++)
            cin >> protein[j];

        for(int j = 0; j < n; j++){
            balance = balance + protein[j] - k;
            if(balance < 0){
                cout << "NO " << j + 1 << '\n';
                break;
            }
            if(j == n - 1)
                cout << "YES\n";
        }

        balance = 0;
    }

    return 0;
}