#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int toint(string a, string b) {
    int sum = 0, tmp = 0;
    for (int i = 0; i < a.length(); i++) 
        tmp = tmp * 10 + (a[i] - '0');
    sum += tmp;
    tmp = 0;
    for (int i = 0; i < b.length(); i++) 
        tmp = tmp * 10 + (b[i] - '0');
    sum += tmp;
    return sum;
}

int main(){
    int t;

    string a, b;

    cin >> t;
    while (t--) {
        cin >> a >> b;
        int mx = -1;

        for (int i = 0; i < a.length(); i++) {
            for (int j = 0; j < b.length(); j++) {
                swap(a[i], b[j]);
                mx = max(mx, toint(a, b));
                swap(a[i], b[j]);
            }
        }

        cout << mx << '\n';
    }
}