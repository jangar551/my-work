#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int gcd(int a, int b) { 
    if (b == 0) 
        return a; 
    return gcd(b, a % b); 
} 

int main(){
    int t, n, m, lcm, ans, tmp, mx;
    cin >> t;
    while(t--) {
        cin >> n >> m;
        vector<int> nums(n);
        for (int i = 0; i < n; i++) cin >> nums[i];
        lcm = nums[0];
        for (int i = 1; i < n; i++) lcm = (lcm * nums[i]) / gcd(lcm, nums[i]);
        mx = -1;
        for (int i = 1; i <= m; i++) {
            tmp = (lcm * i) / gcd(lcm, i);
            cout << mx << ' ' << tmp << '\n';
            if (mx < tmp) ans = i, mx = tmp;
        }

        cout << ans << '\n';        
    }
}