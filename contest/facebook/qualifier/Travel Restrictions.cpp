#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    ifstream in ("test.in");
    ofstream out ("test.out");
    int t, n;
    string a, b;
    in >> t;
    for (int test = 1; test <= t; test++) {
        in >> n >> a >> b;
        vector<vector<char> > answer(n, vector<char>(n, 'N'));
        
        for (int i = 0; i < n; i++) {
            answer[i][i] = 'Y';
            for (int j = i - 1; j >= 0; j--) {
                if (a[j] == 'N' || b[j + 1] == 'N')
                    break;
                answer[i][j] = 'Y';
            }
            for (int j = i + 1; j < n; j++) {
                if (a[j] == 'N' || b[j - 1] == 'N')
                    break;
                answer[i][j] = 'Y';
            }
        }
        out << "Case #" << test << ":\n";
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) 
                out << answer[i][j];
            out << '\n';
        }
    }
    return 0;
}