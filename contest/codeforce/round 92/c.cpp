#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int search(string s, char a, char b) {
    bool f = true;
    int answer = 0;
    for (int i = 0; i < s.length(); i++) {
        if (f) {
            if (s[i] == a) {
                f = false;
                if (a == b)
                    answer++;
            } 
        } else {
            if (s[i] == b) {
                f = true;
                if (a == b)
                    answer++;
                else 
                    answer+=2;
            }
        }
    }
    return s.length() - answer;
}

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, answer;
    string s;
    cin >> t;
    while (t--) {
        cin >> s;
        answer = s.length();
        for (char i = '0'; i <= '9'; i++) {
            for (char j = '0'; j <= '9'; j++) {
                answer = min(answer, search(s, i, j));
            }
        }
        cout << answer << '\n';
    }
    return 0;
}