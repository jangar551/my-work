#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, a, b;
    cin >> t;
    while (t--) {
        cin >> a >> b;
        if (a * 2 > b)
            cout << -1 << ' ' << -1 << '\n'; 
        else
            cout << a << ' ' << a * 2 << '\n';
    }
    return 0;
}