#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main () {
    int t, n, k, z, del, l, ans;
    cin >> t;
    while(t--) {
        cin >> n >> k >> z; 
        ans = 0;
        vector<int> nums(n);
        for(int i = 0; i < n; i++) 
            cin >> nums[i];
        
        l = min((k + 1) / 2 + 1, z + 1);
        vector<vector<int> > save(k + 1, vector<int> (l));

        save[0][0] = nums[0];

        for(int i = 0; i < k; i++) 
            save[i + 1][0] = save[i][0] + nums[i + 1];
        
        for(int i = 1; i <= l - 1; i++) {
            save[0][i] = save[1][i - 1] + nums[0];
            for(int j = 1; j + i * 2 <= k; j++) 
                save[j][i] = max(save[j - 1][i], save[j + 1][i - 1]) + nums[j];
        }
        for(int i = 0; i <= l - 1; i++) 
        if(k - i * 2 >= 0)
            ans = max(save[k - i * 2][i], ans);
        

        cout << ans << "\n";
    }

    return 0;
}