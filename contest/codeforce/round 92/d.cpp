#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, k, l1, l2, r1, r2, con, met, minl;
    ll answer, now;
    cin >> t;
    while (t--) {
        cin >> n >> k >> l1 >> r1 >> l2 >> r2;
        answer = now = 0;
        if (r2 < r1) {
            swap(l1, l2);
            swap(r1, r2);
        }
        if (l2 < r1)
            now += (r1 - l2) * n;

        if (now >= k) {
            cout << 0 << '\n';
            continue;
        }
        minl = min(l1, l2);
        con = r2 - r1 + l1 - minl + l2 - minl;
        met = max(l2 - r1, 0);
        // cout << con << " hhe " << met << '\n';
        if (con < 2 * (r2 - minl + 1)) {
            for (int i = 0; i < n; i++) {
                if (met > (k - now) * 2)
                    break;
                if (now + r2 - minl <= k) {
                    answer += con;
                    now += r2 - minl;
                } else {
                    answer += met;
                    answer += k - now;
                    now = k;
                    break;
                }
            }
            // cout << now << '\n';
            if (now < k)
                answer += (k - now) * 2;
        } else {
            if (r2 - min(l1, l2) + now > k) {
                answer += met;
                answer += k - now;
            } else {
                answer += con;
                now = con;
                answer += (k - now) * 2;
            }
        }

        cout << answer << '\n';
    }
    return 0;
}