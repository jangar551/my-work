#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, cnt;
    string a, b;
    cin >> t;
    while (t--) {
        cin >> n >> a >> b;
        cnt = 0;
        for (int i = 0; i < n; i++) {
            if (a[i] > b[i])
                cnt++;
            else if (a[i] < b[i])
                cnt--;
        }

        if (cnt > 0)
            cout << "RED\n";
        else if (cnt == 0)
            cout << "EQUAL\n";
        else 
            cout << "BLUE\n";
    }
    return 0;
}