#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int n;
    cin >> n;
    
    if (n % 2 == 1) {
        cout << (n / 2 + 1) * (n / 2 + 2) * 2 << '\n';   
    } else {
        cout << (n / 2 + 1) * (n / 2 + 1) << '\n';
    }
    return 0;
}