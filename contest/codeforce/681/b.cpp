#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, a, b, ans;
    string s;
    cin >> t;
    while (t--) {
        cin >> a >> b >> s;
        ans = 0;
        int i = 0, cnt;
        while (i < s.length() && s[i] == '0')
            i++;
        if (i != s.length())
            ans += a;
        for (; i < s.length(); i++) {
            if (s[i] == '0') {
                cnt = 0;
                while (i < s.length() && s[i] == '0')
                    i++, cnt++;
                if (i == s.length())
                    break;
                ans += min(cnt * b, a);
            }
        }

        cout << ans << '\n';
    }
    return 0;
}