#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define int long long

using namespace std;

signed main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, back, ans;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<pair<int, int> > a(n);
        vector<int> b(n);
        back = 0;
        for (int i = 0; i < n; i++)
            cin >> a[i].ff, a[i].ss = i;
        for (int i = 0; i < n; i++)
            cin >> b[i], back += b[i];
        ans = back;

        sort(a.begin(), a.end());
        for (int i = 0; i < n; i++) {
            back -= b[a[i].ss];
            ans = min(ans, max(a[i].ff, back));
        }

        cout << ans << '\n';
    }
    return 0;
}