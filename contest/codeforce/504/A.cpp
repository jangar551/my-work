#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, m;
    string s, t;
    cin >> n >> m >> s >> t;
    int i;
    for (i = 0; i < n; i++) if (s[i] == '*') break;
    if (i == n) {
        if (n != m) {
            cout << "NO\n";
            return 0;
        }
        for (i = 0; i < n; i++) if (s[i] != t[i]) break;
        if (i != n) cout << "NO\n";
        else cout << "YES\n";
        return 0;
    }

    if (m < n - 1) cout << "NO\n";
    else {
        for (i = 0; i < n; i++) {
            if (s[i] == '*') break;

            if (s[i] != t[i]) {
                cout << "NO\n";
                return 0;
            }
        }

        for (int j = n - 1; j > i; j--) 
        if (s[j] != t[m - (n - j)]) {
            cout << "NO\n";
            return 0; 
        } 

        cout << "YES\n";
    }

    return 0;
}