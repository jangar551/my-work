#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, h, a, b, k, fa, ta, fb, tb;
    cin >> n >> h >> a >> b >> k;

    while(k--) {
        cin >> ta >> fa >> tb >> fb;

        if (ta == tb) cout << abs(fa - fb) << '\n';
        else if (fa > b && fb > b) cout << fa - b + fb - b + abs(ta - tb) << '\n';
        else if (fa < a && fb < a) cout << a - fa + a - fb + abs(ta - tb) << '\n';
        else cout << abs(fa - fb) + abs(ta - tb) << '\n';
    }

    return 0;
}