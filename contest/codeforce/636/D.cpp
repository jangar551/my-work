#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, k, mx, answer, sum, tmp;
    cin >> t;
    while(t--) {
        cin >> n >> k;
        vector<int> nums(n), cnt(2 * k + 1, 0);
        for (int i = 0; i < n; i++) cin >> nums[i];

        for (int i = 0; i < n / 2; i++) 
            cnt[nums[i] + nums[n - i - 1]]++;
        
        mx = 0;
        for (int i = 0; i <= 2 * k; i++) if (mx <= cnt[i]) mx = cnt[i], sum = i;
        cout << sum << '\n';
        
        answer = 0;
        for (int i = 0; i < n / 2; i++) {
            tmp = 0;
            if (nums[i] > sum || sum - nums[i] > k) tmp++;
            if (nums[n - i - 1] > sum || sum - nums[n - i - 1] > k) tmp++;
            cout << "ey " << tmp << '\n';
            if (tmp == 0 && nums[i] + nums[n - i - 1] != sum) tmp++;
            cout << "oy " << tmp << '\n';

            answer += tmp;
        }

        cout << answer << '\n';
    }
}