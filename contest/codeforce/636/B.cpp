#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, num;
    cin >> t;
    while(t--) {
        cin >> n;
        if ((n / 2) % 2 == 1) cout << "NO\n";
        else {
            cout << "YES\n";
            num = 2;
            for (int i = 0; i < n / 2; i++) cout << num << ' ', num += 2;
            num = 1;
            for (int i = 0; i < n / 2 - 1; i++) cout << num << ' ', num += 2;
            cout << num + (num + 1) / 2 << '\n';
        }
    }
}