#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, tmp, div;
    cin >> t;
    while(t--) {
        cin >> n;
        tmp = 2;
        div = 3;
        while (n % div != 0) tmp *= 2, div += tmp;


        cout << n / div << '\n';
    }

    return 0;
}