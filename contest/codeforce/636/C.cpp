#include <bits/stdc++.h>
#include <limits.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll t, n, ans, mx;
    bool pst;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<ll> nums(n);
        for (ll i = 0; i < n; i++) cin >> nums[i];
        ans = 0;

        for (ll i = 0; i < n; i++) {
            pst = true;
            if (nums[i] < 0) pst = false;
            if (pst) mx = -1;
            else mx = LLONG_MIN;

            while (i < n && ((pst && nums[i] > 0) || (!pst && nums[i] < 0))) {
                mx = max(mx, nums[i]);
                i++;
            }
            ans += mx;
            i--;
        }

        cout << ans << '\n';
    }
}