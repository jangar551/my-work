#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, tmp;
    cin >> t;
    while (t--) {
        cin >> n;
        tmp = n / 7 + 1;
        if ((tmp * 7 - n) % 2 != 0)
            tmp++;
        if ((tmp * 7 - n) > tmp * 4) {
            cout << -1 << '\n';
        } else {
            int dif = tmp * 7 - n;
            cout << dif / 4 << ' ' << (dif - (dif / 4) * 4) / 2 << ' ' << tmp - (dif - (dif / 4) * 4) / 2 - dif / 4 << '\n'; 
        }
    }
    return 0;
}