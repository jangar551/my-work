#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, k, indx, last, ans, cnt, side;
    string s;
    cin >> t;
    while (t--) {
        ans = 0;
        cin >> n >> k >> s;
        vector<int> space;
        last = side = cnt = 0;
        int indx = 0;
        while (indx < n && s[indx] != 'W') {
            side++;
            indx++;
        }
        for (int i = indx; i < n; i++) {
            if (s[i] == 'W') {
                ans ++;
                if (last)
                    ans ++;
                if (cnt != 0)
                    space.pb(cnt);
                cnt = 0;
                last = 1;
            } else {
                last = 0;
                cnt++;
            }
        }
        side += cnt;
        // cout << ans << ' ' << side << '\n';
        sort(space.begin(), space.end());
        for (int i = 0; i < space.size(); i++) {
            if (space[i] > k) {
                ans += k * 2;
                k = 0;
                break;
            } else {
                ans += space[i] * 2 + 1;
                k -= space[i];
            }
        }
        if (k > 0) {
            ans += min(side, k) * 2;
            if (side == n)
                ans -= 1;
        }
        cout << ans << '\n';
    }
    return 0;
}