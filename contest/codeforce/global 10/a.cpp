#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, sum, l, r;
    cin >> t;
    while (t--) {
        cin >> n;
        l = 0;
        r = 0;
        vector<int> nums(n), ans;
        for (int i = 0; i < n; i++) {
            cin >> nums[i];
            if (nums[i] < 0)
                l -= nums[i];
            else
                r += nums[i];
        }
        if (l == r) {
            cout << "NO\n";
            continue;
        }

        cout << "YES\n";
        sort(nums.begin(), nums.end());
        if (l > r) {
            for (int i = 0; i < n; i++)
                cout << nums[i] << ' ';
            cout << '\n';
        } else {
            for (int i = n - 1; i >=0; i--)
                cout << nums[i] << ' ';
            cout << '\n';
        }
    }
    return 0;
}