#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>
#include <bits/stdc++.h>
#define ll long long

using namespace std;

int main () {
    ll n, temp;
    cin >> n;

    vector<ll> answer(1e6 + 5, 0); 
    
    for(int i = 0; i < n; i++) {
        cin >> temp;
        answer[temp - i + 2e5] += temp;
    }
    
    sort(answer.rbegin(), answer.rend());

    cout << answer[0] << '\n';
    return 0;
}