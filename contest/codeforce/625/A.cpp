#include <iostream>
#include <vector>

using namespace std;

int main () {
    int n, x = 0, y = 0;
    cin >> n;

    vector<int> a1(n), a2(n);
    for (int i = 0; i < n; i++) cin >> a1[i];
    for (int i = 0; i < n; i++) cin >> a2[i];

    for (int i = 0; i < n; i++) {
        if (a1[i] > a2[i]) x++;
        if (a1[i] < a2[i]) y++;
    }

    if (x == 0) {
        cout << -1 << '\n';
        return 0; 
    }

    y++;
    int ans = y / x;
    if (y % x != 0) ans++;
    cout << ans << '\n';
    return 0;
}