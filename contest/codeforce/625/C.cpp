#include <bits/stdc++.h>
using namespace std;

const int mX[] = {-1, 1, 0, 0};
const int mY[] = {0, 1, 0, -1};

int n, answer = 0;
string str;

void rmv(int now) {
    n = str.size();
    for(int i = 0; i < n; i++) 
    if (str[i] == now + 'a') 
    for(int j = 0; j < 2; j++) {
        int indx = i + mX[j];

        if (indx < 0 || indx >= n) continue;

        int dif = str[i] - str[indx];

        if (dif == 1) {
            answer++;
            str = str.substr(0, i) + str.substr(i + 1);
            return rmv(now);
        }
    }
    
    
}

int main () {
    cin >> n >> str;
    for(int j = 0; j < 100; j++) 
    for(int i = 26 - 1; i >= 1; i--) 
        rmv(i);
    
    cout << answer << '\n';
    return 0;
}