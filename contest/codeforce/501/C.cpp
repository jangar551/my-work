#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll n, m, sum = 0, minsum = 0;

    cin >> n >> m;

    vector<int> size(n), cmprs(n);

    for (int i = 0; i < n; i++) {
        cin >> size[i], sum+= size[i];
        cin >> cmprs[i], minsum += cmprs[i], cmprs[i] = size[i] - cmprs[i];
    }

    if (minsum > m) {
        cout << -1 << '\n';
        return 0;
    } else if (sum <= m) {
        cout << 0 << '\n';
        return 0;
    }

    sort(cmprs.begin(), cmprs.end());

    for (int i = n - 1; i >= 0; i--) {
        sum -= cmprs[i];
        if (sum <= m) {
            cout << n - i << '\n';
            break;
        }
    }

    return 0;
}