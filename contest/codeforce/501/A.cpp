#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, m;
    cin >> n >> m;
    vector<pair<int, int> > seg(n);
    for (int i = 0; i < n; i++) cin >> seg[i].first >> seg[i].second;

    vector<int> ans;
    for (int i = 1; i <= m; i++) 
    for (int j = 0; j < n; j++) {
        if (seg[j].first <= i && seg[j].second >= i) break; 
        if (j == n - 1) ans.pb(i);
    }

    cout << ans.size() << '\n';
    for (int i = 0; i < ans.size(); i++) cout << ans[i] << ' ';
    cout << '\n'; 
    
    return 0;
}