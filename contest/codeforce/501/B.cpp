#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

vector<int> answer;

void print(int to, int from, string& s) {
    for (int i = from - 1; i >= to; i--) answer.pb(i + 1), swap(s[i], s[i + 1]); 
}

int main(){
    int n;
    string s, t;
    cin >> n >> s >> t;

    vector<int> count(27, 0);
    for (int i = 0; i < n; i++) count[s[i] - 'a']++;
    
    for (int i = 0; i < n; i++) count[t[i] - 'a']--;
    for (int i = 0; i < 27; i++) 
    if (count[i] != 0) {
        cout << -1 << '\n';
        return 0;
    }

    for (int i = 0; i < n; i++) {
        if (s[i] == t[i]) continue;
        for (int j = i + 1; j < n; j++) 
        if (s[j] == t[i]) {
            print(i, j, s);
            break;
        }
    }

    cout << answer.size() << '\n';
    for (int i = 0; i < answer.size(); i++) cout << answer[i] << ' ';
    cout << '\n';

    return 0;
}