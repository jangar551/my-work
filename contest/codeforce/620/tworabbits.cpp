#include <iostream>

using namespace std;

int main () {
    int j;
    cin >> j;
    
    long long b1, b2, k1, k2;
    for (int i = 0; i < j; i++) {
        cin >> b1 >> b2 >> k1 >> k2;

        b1 = b2 - b1;
        if (b1 % (k1 + k2) == 0) 
            cout << b1 / (k1 + k2) << '\n';
        else cout << -1 << '\n';
    }

    return 0;
}