#include <iostream>
#include <vector>

using namespace std;

int main(){
    int n, m;

    cin >> n >> m;

    vector<string> word(n);

    for (int i = 0; i < n; i++) cin >> word[i];
    
    vector<bool> used(n, 0);

    vector<string> answer;
    vector<string> palindrome;

    for (int i = 0; i < n; i++) {
        if (used[i])
            continue;
        for (int j = 0; j < n; j++) {
            if (i == j || used[j]) continue;
            

            int k;
            for (k = 0; k < m; k++) 
                if (word[i][k] != word[j][m - k - 1])
                    break;
            
            if (k == m) {
                used[i] = 1;
                used[j] = 1;
                answer.push_back(word[j]);
            }
        }
    }
    bool tmp = false;;
    for (int i = 0; i < n; i++) {
        if (tmp)
            break;
        if (used[i])
            continue;
        int j;
        for (j = 0; j < m / 2; j++) 
            if (word[i][j] != word[i][m - j - 1])
                break;
        if (j == m / 2) {
            tmp = true;
            used[i] = 1;
            palindrome.push_back(word[i]);
        }

    }

    cout << answer.size() * m * 2 + palindrome.size() * m<< '\n';

    for (int i = 0; i < answer.size(); i++) cout << answer[i];
    if (palindrome.size() != 0) cout << palindrome[0];
    for (int i = answer.size() - 1; i >= 0; i--) 
        for (int j = m - 1; j >= 0; j--) cout << answer[i][j];


    return 0;
}