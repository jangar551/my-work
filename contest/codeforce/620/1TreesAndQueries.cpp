#include <iostream>
#include <vector>
#include <algorithm>
#define int long long
#define INF 10000000000

using namespace std;

vector<vector<int> > conn;
vector<vector<int> > used, clear;
int x, y, a, b, k, a1, a2;

void find(int now, int sum) {
    if (now == b && sum != 0) {
        // cout << "-----------\n";
        if (sum % 2 == 1) a1 = min(a1, sum); 
        else a2 = min(a2, sum);
        return;
    }
    // cout << now + 1<< '\n';
    // cout << conn[now].size() << '\n';
    for (int i = 0; i < conn[now].size(); i++) {
        if (used[now][conn[now][i]] == 2) continue;

        used[now][conn[now][i]]++;
        used[conn[now][i]][now]++;

        find(conn[now][i], sum + 1);
    }

    return;
}

signed main(){
    int n;
    cin >> n;

    conn.resize(n);
    clear.resize(n, vector<int>(n, 0));

    for (int i = 0; i < n - 1; i++) {
        cin >> a >> b;
        a--;
        b--;

        conn[a].push_back(b);
        conn[b].push_back(a);
    }

    int q;
    cin >> q;

    for (int i = 0; i < q; i++) {
        cin >> x >> y >> a >> b >> k;
        x--;
        y--;
        a--;
        b--;
        a1 = a2 = INF;
        // cout << "working\n";
        used = clear;

        conn[x].push_back(y);
        conn[y].push_back(x);

        find(a, 0);
        // cout << a1 << ' ' << a2 <<'\n';
        if ((k % 2 == 1 && a1 <= k) || (k % 2 == 0 && a2 <= k)) cout << "YES\n";
        else cout << "NO\n";

        conn[x].pop_back();
        conn[y].pop_back();
    }
}