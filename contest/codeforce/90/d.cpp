#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n;
    ll answer, sum, add;
    cin >> t;
    while (t--) {
        cin >> n;
        vector <int> nums(n);
        sum = 0;
        for (int i = 0; i < n; i++) { 
            cin >> nums[i];
            if (i % 2 == 0) 
                sum += nums[i];
        }
        answer = add = 0;
        for (int i = 0; i < n - 1; i+=2) {
            add += nums[i + 1] - nums[i];
            answer = max(answer, add);
            if (add < 0) add = 0;
        }
        int i = n - 1;
        if (n % 2 == 0) 
            i--;
        add = 0;
        for (; i > 0; i-=2) {
            add += nums[i - 1] - nums[i];
            answer = max(answer, add);
            if (add < 0) add = 0;
        }
        

        cout << answer + sum << '\n';
    }
    return 0;
}