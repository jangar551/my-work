#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t;
    ll answer, sum;
    string n;
    cin >> t;
    while (t--) {
        cin >> n;
        sum = 0;
        answer = n.size();
        for (int i = 0; i < n.length(); i++) {
            if (n[i] == '-') 
                sum--;
            else
                sum++;
            
            if (sum < 0) {
                answer += i + 1;
                sum = 0;
            }
        }

        cout << answer << '\n';
    }
    return 0;
}