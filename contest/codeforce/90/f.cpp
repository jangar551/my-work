#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define int ll

using namespace std;

signed main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, indx, trans, zero = 0;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<int> city(n), tower(n), tmptower(n);
        for (int i = 0; i < n; i++) 
            cin >> city[i];
        for (int i = 0; i < n; i++) {
            cin >> tower[i];
            tmptower[i] = tower[i];
        }
        trans = 0;
        for (int i = 0; i < 2 * n; i++) {
            indx = i % n;
            if (trans >= city[indx] - tower[indx]) {
                trans = trans - city[indx] + tower[indx];
                if (trans > tmptower[indx]) trans = tmptower[indx];
                tmptower[indx] -= trans;
                if (tmptower[indx] < 0) tmptower[indx] = 0;
                city[indx] = 0;
                tower[indx] = 0;
            } else {
                city[indx] -= trans;
                trans = 0;
            }
        }
        bool possible = true;
        for (int i = 0; i < n; i++) {
            if (city[i] != 0) 
                possible = false;
        }
        if (possible) 
            cout << "YES\n";
        else
            cout << "NO\n"; 
    }
    return 0;
}