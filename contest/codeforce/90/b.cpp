#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, one, zero, count;
    string n;
    cin >> t;
    while (t--) {
        cin >> n;
        one = zero = count = 0;
        for (int i = 0; i < n.size(); i++) {
            if (n[i] == '1') 
                one++;
            else
                zero++;
            
            count += min(zero, one);
            if (zero > one) {
                zero -= one;
                one = 0;
            } else {
                one -= zero;
                zero = 0;
            }
        }

        if (count % 2 == 0) cout << "NET\n";
        else cout << "DA\n";
    }
    return 0;
}