#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll t, a, b, c;
    cin >> t;
    while (t--) {
        cin >> a >> b >> c;
        if (a < c) cout << 1 << ' ';
        else cout << -1 << ' ';

        if (a * b > c) cout << b << '\n';
        else cout << -1 << '\n';
    }
    return 0;
}