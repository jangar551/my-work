#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    long long t, x, y, a, b, answer;
    cin >> t;
    while(t--) {
        cin >> x >> y >> a >> b;
        answer = 0;
        if ((x < 0 && y < 0) || (x > 0 && y > 0)) {
            if (b < 2 * a) {
                answer += b * min(abs(x), abs(y));
                answer += a * abs(x - y);
            } else {
                answer += a * (abs(x) + abs(y));            }
        } else {
            answer += a * (abs(x) + abs(y)); 
        }

        cout << answer << '\n';
    }
}