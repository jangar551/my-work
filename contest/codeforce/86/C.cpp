#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, a, b, q, l, r;
    cin >> t;
    while(t--) {
        cin >> a >> b >> q;
        if (a < b) swap(a, b);
        while (q--) {
            cin >> l >> r;
            cout << (r/(((a * b)/__gcd(a, b))))*(r-l) + max((r%(((a * b)/__gcd(a, b))))-b, 0) - ((l-1)/(((a * b)/__gcd(a, b))))*(r-l) - max(((l-1)%(((a * b)/__gcd(a, b))))-b, 0) << ' ';  
        }
        cout << '\n';
    }
}