#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t;
    string s;
    cin >> t;
    while(t--) {
        cin >> s;
        int i;
        for (i = 1; i < s.length(); i++) if (s[i] != s[i - 1]) break;
        if (i == s.length()) cout << s << '\n';
        else {
            string ans = "";
            ans += s[0];
            for (int i = 1; i < s.length(); i++) {
                if (s[i] == s[i - 1]) {
                    if (s[i] == '1') ans += "01";
                    else ans += "10";
                } else {
                    ans += s[i];
                }
            }

            cout << ans << '\n';
        } 
    }
}