#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define INF 200005

using namespace std;

int main(){
    int t, n, tmp;
    cin >> t;

    vector<int> loc (INF);

    while(t--) {
        cin >> n;
        vector<int> port(n), color(n);
        vector<vector<int> > duplicate;
        for (int i = 0; i < n; i++) {
            cin >> port[i];
        }
        for (int i = 0; i < n; i++) cin >> color[i];


        vector<vector<int> > loops;

        vector<bool> used(n, false);
        for (int i = 0; i < n; i++) {
            if (used[i]) continue;
            vector<int> loop;
            loop.push_back(port[i]);
            tmp = port[i];
            while (tmp != i) {
                tmp = port[tmp];
                loc[tmp] = loops.size();               
                loop.push_back(tmp); 
            } 
            loops.push_back(loop);
        }

        
    }
}