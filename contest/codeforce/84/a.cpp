#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;

int main(){
    int t, n, k;
    cin >> t;
    while (t--) {
        cin >> n >> k;
        if (k > n) cout << "NO\n";
        else if (n % 2 == 0 && k % 2 == 0) cout << "YES\n";
        else if (n % 2 == 1 && k % 2 == 1) cout << "YES\n";
        else cout << "NO\n";
    }
}