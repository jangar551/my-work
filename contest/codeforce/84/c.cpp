#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;

int main(){
    int n, m, k, tmp;
    cin >> m >> n >> k;
    for (int i = 0; i < k; i++) cin >> tmp >> tmp >> tmp >> tmp;

    cout << (n - 1) + (m - 1) + (n * m - 1) << '\n';
    for (int i = 0; i < n - 1; i++) cout << "L";
    for (int i = 0; i < m - 1; i++) cout << "U";

    int x = 0, y = 0;

    while (y < m) {
        if (x != 0) while (x != 0) cout << "L", x--; 
        else while (x != n - 1) cout << "R", x++;
        y++;
        if (y == m) break;
        cout << "D";
    }

    cout << '\n';

    return 0;
}