#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>
#define int long long

using namespace std;

vector<int> teacher, student;

signed main(){
    int n, tmp;
    cin >> n;
    vector<int> tmp_vec(n);
    for (int i = 0; i < n; i++) cin >> tmp_vec[i];

    for (int i = 0; i < n; i++) {
        cin >> tmp;
        if (tmp_vec[i] > tmp) teacher.push_back(tmp_vec[i] - tmp);
        else student.push_back(tmp - tmp_vec[i]);
    }

    sort(student.begin(), student.end());

    int answer = 0;
    for (int i = 0 ; i < teacher.size(); i++) {
        answer += (upper_bound (student.begin(), student.end(), teacher[i] - 1) - student.begin());
    }
    
    answer += teacher.size() * (teacher.size() - 1) / 2;
    
    cout << answer << '\n';

    return 0;
}