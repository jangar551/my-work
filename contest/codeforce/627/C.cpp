#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>

using namespace std;

int main(){
    int t, n, idx, answer;
    cin >> t;

    string str;

    while (t--) {
        cin >> str;
        n = str.length();
        answer = 0;
        idx = -1;
        for (int i = 0; i < n; i++) {
            if (str[i] == 'R') {
                if (i - idx > answer) answer = i - idx;
                idx = i;
            }
        }
        if (answer < n - idx) answer = n - idx;

        cout << answer << '\n';
    }
}