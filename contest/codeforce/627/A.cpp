#include <bits/stdc++.h>
using namespace std;
    
int main () {
    int t, n, temp;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<int> cnt(2, 0);
        for (int i = 0; i < n; i++) {
            cin >> temp;
            cnt[temp % 2]++;
        }

        if (cnt[0] == 0 || cnt[1] == 0) cout << "YES\n";
        else cout << "NO\n";
    }

    return 0;
}
