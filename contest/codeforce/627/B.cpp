#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>

using namespace std;

int main(){
    int t, n, tmp;
    cin >> t;
    vector <int> found(5002, -1), clear(5002, -1);
    bool answer;
    while (t--) {
        answer = false;
        found = clear;

        cin >> n;
        for(int i = 0; i < n; i++) {
            cin >> tmp;
            if (found[tmp] != -1){
                if(i - found[tmp] > 1) answer = true;   
            }  else found[tmp] = i;
            
        }

        if (answer) cout << "YES\n";
        else cout << "NO\n";
    }
}