#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, tmp, answer;
    cin >> n;
    vector<int> loca(n + 1), locb(n + 1), dist(n + 1, 0);

    for (int i = 0; i < n; i++) {
        cin >> tmp;
        loca[tmp] = i;
    }
    for (int i = 0; i < n; i++) {
        cin >> tmp;
        locb[tmp] = i;
    }
    answer = -1;
    for (int i = 1; i <= n; i++) {
        if (loca[i] <= locb[i]) dist[locb[i] - loca[i]]++;
        else dist[(n - loca[i]) + locb[i]]++;
    }
    for (int i = 0; i <= n; i++) {
        answer = max(answer, dist[i]);
    }
    cout << answer << '\n'; 
}