#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n;
    bool zero, one;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<int> val(n), type(n);
        zero = one = false;
        for (int i = 0; i < n; i++) cin >> val[i];
        for (int i = 0; i < n; i++) {
            cin >> type[i];
            if (type[i] == 1) one = true;
            else zero = true;
        }
        if (zero && one) cout << "YES\n";
        else {
            int i;
            for (i = 1; i < n; i++) {
                if (val[i] < val[i - 1]) break;
            }
            if (i == n) cout << "YES\n";
            else cout << "NO\n";
        }
    }   
}