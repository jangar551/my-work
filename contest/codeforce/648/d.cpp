#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

vector<pair<int, int> > movement{mp(0, 1), mp(0, -1), mp(1, 0), mp(-1, 0)};

bool nearbad(vector<vector<char> >& grid, int r, int c, int& mn, int& mm) {
    int n, m;
    for (int i = 0; i < 4; i++) {
        n = r;
        m = c;
        n += movement[i].ff;
        m += movement[i].ss;
        if ((n >= 0 && n < mn) && (m >= 0 && m < mm)) {
            if (grid[n][m] == 'B') return true;
        }
    }
    return false;
}
bool neargood(vector<vector<char> >& grid, int r, int c, int& mn, int& mm) {
    int n, m;
    for (int i = 0; i < 4; i++) {
        n = r;
        m = c;
        n += movement[i].ff;
        m += movement[i].ss;
        if ((n >= 0 && n < mn) && (m >= 0 && m < mm)) {
            if (grid[n][m] == 'G') return true;
        }
    }
    return false;
}

int dig(vector<vector<char> >& grid, int r, int c, vector<vector<bool> >& passed, int& mn, int& mm) {
    int n, m, good = 0;
    if (grid[r][c] == 'G') good++;
    // cout << r << ' ' << c << '\n';
    passed[r][c] = true;
    for (int i = 0; i < 4; i++) {
        n = r;
        m = c;
        n += movement[i].ff;
        m += movement[i].ss;
        if ((n >= 0 && n < mn) && (m >= 0 && m < mm)) {
            if (grid[n][m] == '#') continue;
            if (passed[n][m]) continue;
            good += dig(grid, n, m, passed, mn, mm); 
        }
    }
    return good;
}

void block(vector<vector<char> >& grid, int r, int c, int mn, int mm) {
    int n, m;
    for (int i = 0; i < 4; i++) {
        n = r;
        m = c;
        n += movement[i].ff;
        m += movement[i].ss;
        if ((n >= 0 && n < mn) && (m >= 0 && m < mm)) {
            if (grid[n][m] == 'B') continue;
            grid[n][m] = '#';
        }
    }
}

int main(){
    int t, n, m, cnt;
    cin >> t;
    bool answer;
    while (t--) {
        cin >> n >> m;
        cnt = 0;
        answer =true;
        vector<vector<char> > grid(n, vector<char> (m));
        for (int i = 0; i < n; i++) 
        for (int j = 0; j < m; j++) {
            cin >> grid[i][j];
            if (grid[i][j] == 'G') cnt++;
            
        }
        for (int i = 0; i < n; i++) 
        for (int j = 0; j < m; j++) {
            if (grid[i][j] == 'B') {
                if (neargood(grid, i, j, n, m)) answer = false;
                block(grid, i, j, n, m);
            } 
        }
        vector<vector<bool> > passed(n, vector<bool> (m, 0));
        if (!answer) cout << "No\n";
        else if (nearbad(grid, n - 1, m - 1, n, m) && cnt != 0) cout << "No\n";
        else if (dig(grid, n - 1, m - 1, passed, n, m) == cnt) cout << "Yes\n";
        else cout << "No\n";
    }
}