#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, m, sum = 0, now, space;
    cin >> n >> m;
    vector<int> nums(m);
    for (int i = 0; i < m; i++) {
        cin >> nums[i];
        sum += nums[i];
    }

    if (sum < n) {
        cout << -1 << '\n';
        return 0;
    }
    sort(nums.begin(), nums.end(), greater<int>());
    space = sum - n;
    now = n;

    for (int i = 0; i < m; i++) {
        cout << max(now - nums[i] + 1, 1) << ' ';
        now = max(now - nums[i] + 1, 1);
    }
    cout << '\n';
    return 0;
}