#include <iostream>
#include <vector>
 
using namespace std;
 
void find() {
    int n;
    cin >> n;
    vector <int> n1(n), n2(n);
 
    for (int i = 0; i < n; i++) cin >> n1[i];
 
    for (int i = 0; i < n; i++) cin >> n2[i];
    
    int it;
 
    for (it = 0; it < n; it++) {
        if (n1[it] != n2[it]) 
            break;
    }
    int dif = n1[it] - n2[it];
 
    if (dif > 0) {
        cout << "NO\n";
        return;
    }
    for (; it < n; it++) {
        if (n1[it] - n2[it] != dif) break;
    }
    for (; it < n; it++) {
        if (n1[it] != n2[it]) {
            cout << "NO\n";
            return;
        }
    }
 
    cout << "YES\n";
    return;
}