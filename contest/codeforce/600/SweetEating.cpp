#include <algorithm>
#include <iostream>
#include <vector>
 
using namespace std;
 
int main() {
	int n, m;
	cin >> n >> m;
 
	vector<int> candy(n);
 
	for(int i = 0; i < n; i++) 
		cin >>candy[i];
 
	sort(candy.begin(), candy.end());
 
	vector<long long> memoz(n);
	for(int i = 0; i < n; i++) {
		if (i < m) {
			memoz[i] = 0;
			continue;
		}
		memoz[i] = memoz[i - m] +candy[i - m];
	}
 
	long long result = 0;
	for(int i = 0; i < n; i++) {
		result += memoz[i] + candy[i];
		cout << result << ' ';
	}
	cout << endl;
 
	return 0;
}