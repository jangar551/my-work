#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
using namespace std;
 
vector<int> minimize (int n, vector<int> array, vector<int> element) {
    vector<bool> check (n, 0);
    for (int i = 0; i < n; i++) {
        for (int j = array[i]; j > 0; j--) {
            if(!check[j - 1] && element[j] < element[j - 1]) {
                swap(array[i], array[element[j - 1] - 1]);
                swap(element[j], element[j - 1]);
                check[j - 1] = 1;
            } else {
                break;
            }
        }
    }
    return element;
}
 
int main () {
    int q, n;
    cin >> q;
    vector<vector<int> > answer;
    for (int i = 0; i < q; i++) {
        cin >> n;
        vector<int> array(n);
        vector<int> element(n);
        for (int j = 0; j < n; j++) {
            cin >> element[j];
            array[element[j] - 1] = j;
        }
        answer.push_back(minimize(n, array, element));
    }
    for (int i = 0; i < q; i++) {
        for(int j = 0; j < answer[i].size(); j++) {
            cout << answer[i][j] << " ";
        }
        cout << '\n';
    }
    return 0;
}