#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
#define int long long
 
using namespace std;
 
signed main(){
    int q, a, b, n, S;
 
    cin >> q;
 
    for(int i = 0; i < q; i++){
        cin >> a >> b >> n >> S;
 
        if(n * a + b < S)
            cout << "NO\n";
        else{
            int t = min(a, S/n);
 
            if( S - n * t <= b)
                cout << "YES\n";
            else
                cout << "NO\n";
        }
    }
 
    return 0;
}