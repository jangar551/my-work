#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, k;
    cin >> t;
    while (t--) {
        cin >> n >> k;
        vector<int> nums(n);
        for (int i = 0; i < n; i++)
            cin >> nums[i];
        
        int ans = INT_MAX;
        for (int c = 1; c <= 100; c++) {
            int cnt = 0;
            for (int i = 0; i < n; i+=k) {
                while (nums[i] == c)
                    i++;
                if (i >= n)
                    break;
                cnt++;
            }
            ans = min(ans ,cnt);
        }

        cout << ans << '\n';
    }
    return 0;
}