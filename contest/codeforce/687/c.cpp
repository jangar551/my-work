#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, p, k, x, y;
    string s;
    cin >> t;
    while (t--) {
        cin >> n >> p >> k >> s >> x >> y;
        p--;
        vector<int> cnt(k + 3, 0);
        for (int i = p; i < n; i++) {
            if (s[i] == '0')
                cnt[i % k]++;
        }

        int ans = INT_MAX;

        for (int i = p; i < n; i++) {
            ans = min(ans, (i - p) * y + cnt[i % k] * x);
            if (s[i] == '0')
                cnt[i % k]--;
        }

        cout << ans << '\n';
    }
    return 0;
}