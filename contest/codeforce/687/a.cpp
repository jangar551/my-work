#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, m, r, c;
    cin >> t;
    while (t--) {
        cin >> n >> m >> r >> c;   
        cout << max(c - 1, m - c) + max(r - 1, n - r) << '\n';
    }
    return 0;
}