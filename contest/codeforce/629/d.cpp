#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define INF 200005

using namespace std;

int main(){
    int t;
    cin >> t;
    
    int n, tmp, clr;
    bool done;
    while (t--) {
        cin >> n;
        clr = 2;
        vector<int> type(n), answer(n);
        for (int i = 0; i < n; i++) cin >> type[i];
        for (int i = 1; i < n; i++) {
            if (type[i] != type[0]) break;
            if (i == n - 1) clr = 1;
        }        
        // cout << " clr ----> " << clr << '\n';
        if (clr == 1) {
            for (int i = 0; i < n; i++) answer[i] = 1;
        }else if (n % 2 == 0) {
            clr = 2;
            for (int i = 0; i < n; i++) 
                if (i % 2 == 0) answer[i] = 1; 
                else answer[i] = 2;
        } else {
            done = false;
            clr = 2;
            answer[0] = 1;
            for (int i = 1; i < n; i++) {
                if (type[i - 1] == type[i] && !done) answer[i] = answer[i - 1], done = true; 
                else if (answer[i - 1] == 1) answer[i] = 2; else answer[i] = 1;
            }

            if (answer[0] == answer[n - 1] && type[0] != type[n - 1]) answer[n - 1] = 3, clr = 3;
        }

        cout << clr << '\n';
        for (int i = 0; i < n; i++) cout << answer[i] << ' ';
        cout << '\n'; 
    }

    return 0;
}