#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;

int main(){
    int t, n;
    string num, a, b;
    bool max;

    cin >> t;
    while(t--) {
        cin >> n >> num;

        a = b = "";
        max = true;

        a += '1';
        b += '1';

        for (int i = 1; i < n; i++) {
            if (max) {
                if (num[i] == '1') a += '1', b += '0', max = false;
                else if (num[i] == '2') a += '1', b += '1';
                else a += '0', b += '0';
            } else {
                a += '0';
                b += num[i];
            }
        }

        cout << a << '\n' << b << '\n';
    }
}