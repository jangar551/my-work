#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;

int main(){
    int t;
    cin >> t;
        int a, b;
    while(t--) {
        cin >> a >> b;

        if (a % b == 0) cout << 0 << '\n';
        else cout << b - (a % b) << '\n';
    }
}