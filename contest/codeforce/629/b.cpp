#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;

int main(){
    int t, tmp;
    cin >> t;
    int n, k;
    string ans;
    while(t--) {
        cin >> n >> k;
        ans = "";
        k--;
        tmp = 2;
        
        while (k > 0) {
            k -= tmp;
            tmp++;
        }

        tmp--;
        
        if (k == 0) {
            for (int i = 0; i < n - tmp - 1; i++) ans += "a";
            ans += "bb";
            while(ans.length() != n) ans += "a";
        } else {
            for (int i = 0; i < n - tmp - 1; i++) ans += "a";
            ans += "b";
            for (int i = 0; i < -1 * k; i++) ans += "a";
            ans += "b";
            while(ans.length() != n) ans += "a";
        }

        cout << ans << '\n';

    }
}