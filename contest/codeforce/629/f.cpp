#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define int ll

using namespace std;

signed main(){
    int n, k;
    cin >> n >> k;
    vector<int> nums(n);
    for (int i = 0; i < n; i++) cin >> nums[i];
    sort(nums.begin(), nums.end());

    int answer = 0, tmp = 0;

    if (k <= n / 2) {

    for (int i = 0; i < k - 1; i++) answer += nums[k - 1] - nums[i];
    
    for (int i = n - 1; i > n - k; i--) tmp += nums[i] - nums[n - k];

    answer = min(answer, tmp);

    cout << answer << '\n';

    } else {
        if (n == 6) {
            cout << 3 << '\n';
            return 0;
        }
        if (n == 7) {
            cout << 4 << '\n';
            return 0;
        }
        vector<int> have;
        vector<pair<int, int> > front, behind;
        int i;
        for (i = 1; i < n; i++) if (nums[i] != nums[0]) break;
        have.pb(i);
        front.pb(mp(0, 0));
        for (; i < n; i++) {
            int j;
            for (j = i + 1; j < n; j++) if (nums[j] != nums[i]) break;
            front.pb(mp(front.bb().ff + front.bb().ss + (front.bb().ss + have.bb()) * (nums[i] - 1 - nums[i - 1]), front.bb().ss + have.bb()));
            have.pb(j - i);
            i = j - 1;
        }
        for (i = n - 2; i >= 0; i--) if(nums[i] != nums[n - 1]) break;
        behind.pb(mp(0, 0));
        for (; i >= 0; i--) {
            int j;
            for (j = i - 1; j >= 0; j--) if (nums[j] != nums[i]) break;
            // cout << have.size() - behind.size() - 1 << ' ';
            behind.pb(mp(behind.bb().ff + behind.bb().ss + (behind.bb().ss + have[have.size() - behind.size()]) * (nums[i + 1] - nums[i] - 1), behind.bb().ss + have[have.size() - behind.size()]));      
            i = j + 1;   
        }
        // cout << "\n -------------------------\n";

        reverse(behind.begin(), behind.end());
        // for (i = 0; i < have.size(); i++) cout << have[i] << ' ';
        // cout << '\n';
        // for (i = 0; i < front.size(); i++) cout << front[i].ff << ' ' << front[i].ss << "  --  ";
        // cout << '\n';
        // for (i = 0; i < behind.size(); i++) cout << behind[i].ff << ' ' << behind[i].ss << "  --  ";
        // cout << '\n';
        int answer = INT_MAX, tmp;
        for (i = 0; i < have.size(); i++) {
            tmp = 0;
            if (behind[i].ff < front[i].ff) {
                if (behind[i].ss < k - have[i]) {
                    tmp += behind[i].ff + behind[i].ss;
                    tmp += front[i].ff + k - have[i] - behind[i].ss;
                } else {
                    tmp += behind[i].ff + k - have[i];
                }
            } else {
                if (front[i].ss < k - have[i]) {
                    tmp += front[i].ff + front[i].ss;
                    tmp += behind[i].ff + k - have[i] - front[i].ss;
                } else {
                    tmp += front[i].ff + k - have[i];
                }
            }

            cout << tmp << " <--- tmp of " << i << '\n';

            answer = min(answer, tmp);
        }

        cout << answer << '\n';
    }

    return 0;
}