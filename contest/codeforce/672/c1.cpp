#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    ll t, n, q, ans, indx;
    cin >> t;
    while (t--) {
        ans = 0;
        cin >> n >> q;
        vector<int> nums(n), swap(q);
        for (int i = 0; i < n; i++)
            cin >> nums[i];
        for (int i = 0; i < q; i++)
            cin >> swap[i];
        
        for (int i = 0; i < n; i++) {
            if (i != n - 1) {
                if (nums[i] > nums[i + 1]) {
                    indx = i;
                    while (indx != n - 1 && nums[indx] > nums[indx + 1])
                        indx++;
                    ans += nums[i] - nums[indx];
                    if (indx == n - 1)
                        ans += nums[indx];
                    i = indx;
                } 
            } else {
                ans += nums[i];       
            }
        }

        cout << ans << '\n';
    }
    return 0;
}