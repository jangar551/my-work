#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    ll t, n, bit, cnt, indx, answer;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<int> nums(n);
        for (int i = 0; i < n; i++) 
            cin >> nums[i];

        answer = 0;
        sort(nums.begin(), nums.end());       
        bit = 31;
        for (int i = n - 1; i >= 1; i--) {
            // cout << (nums[i] >> 2) << '\n';
            while(!(nums[i]>>bit)) {
                bit--;
            }
            // cout << bit << '\n';
            indx = i;
            while (indx >= 0 && (nums[indx]>>bit))
                indx--;
            // cout << (indx) << '\n';
            answer += (i - indx - 1) * (i - indx) / 2;
            i = indx + 1;
        }

        cout << answer << '\n';
    }
    return 0;
}