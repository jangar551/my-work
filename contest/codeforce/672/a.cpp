#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, cnt, mx;
    cin >> t;
    while (t--) {
        cin >> n;
        cnt = 0;
        vector<int> nums(n);
        for (int i = 0; i < n; i++) 
            cin >> nums[i];
    
        bool answer = false;
        mx = nums[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            if (nums[i] < mx) {
                answer = true;
            } 
            mx = (mx, nums[i]);
        }
        if (answer || n == 1) {
            cout << "YES\n";
            continue;
        }

        sort(nums.begin(), nums.end());
    
        for (int i = 1; i < n; i++) {
            if (nums[i] == nums[i - 1]) {
                cout << "YES\n";
                break;
            }
            if (i == n - 1)
                cout << "NO\n";
        }
    }
    return 0;
}