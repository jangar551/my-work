#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    ll t, n, q, ans, indx, a, b;
    cin >> t;
    while (t--) {
        ans = 0;
        cin >> n >> q;
        vector<int> nums(n);
        vector<pair<bool, int> > pike(n), bot(n);
        for (int i = 0; i < n; i++)
            cin >> nums[i];
        
        for (int i = 0; i < n; i++) {
            if (i != n - 1) {
                if (nums[i] > nums[i + 1]) {
                    indx = i;
                    while (indx != n - 1 && nums[indx] > nums[indx + 1])
                        indx++;
                    ans += nums[i] - nums[indx];
                    pike[i].ff = true;
                    pike[i].ss = indx;
                    if (indx == n - 1) 
                        ans += nums[indx];
                    else {
                        bot[indx].ff = true;
                        bot[indx].ss = i;
                    }
                    i = indx;
                } 
            } else {
                ans += nums[i];       
            }
        }

        cout << ans << '\n';

        for (int s = 0; s < q; s++) {
            cin >> a >> b;
            a--;
            b--;
            if (pike[a].ff) {
                
            }
        }
    }
    return 0;
}