#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, dir, answer;
    bool inpos;
    string a, b;
    cin >> t;
    while (t--) {
        inpos = false;
        answer = 0;
        cin >> n >> a >> b;
        for (int i = 0; i < n; i++) 
            if (a[i] > b[i])
                inpos = true;
        if (inpos) {
            cout << -1 << '\n';
            continue;
        }

        vector<priority_queue <int, vector<int>, greater<int>>> aim(30);
        for (int i = 0; i < n; i++)  {
            if (a[i] == b[i])
                continue;
            aim[a[i] - 'a'].push(b[i] - 'a');
        }
        
        for (int i = 0; i < 20; i++) {
            if (aim[i].size() == 0)
                continue;
            answer++;
            dir = aim[i].top();
            while(!aim[i].empty()) {
                if (aim[i].top() == dir) {
                    aim[i].pop();
                    continue;
                }
                aim[dir].push(aim[i].top());
                aim[i].pop();
            }
        }

        cout << answer << '\n';
    }
    return 0;
}