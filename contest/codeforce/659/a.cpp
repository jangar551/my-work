#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, run, mx;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<int> nums(n);
        for (int i = 0; i < n; i++)
            cin >> nums[i];
        
        string tmp = "";
        mx = nums[0];
        for (int i = 0; i < n; i++)
            mx = max(mx, nums[i]);
        if (mx == 0)
            mx = 1;
        for (int i = 0; i < mx; i++)
            tmp += 'a';

        cout << tmp << '\n';
        int now;
        for (int i = 0; i < n; i++) {
            now = nums[i];
            tmp[now]++;
            if (tmp[now] == 'z' + 1) tmp[now] = 'a';
            cout << tmp << '\n';
        }
    }
    return 0;
}