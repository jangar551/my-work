#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, k, l, now;
    bool pos, ind;
    cin >> t;
    while (t--) {
        pos = true;
        cin >> n >> k >> l;
        vector<int> nums(n), safe;
        for (int i = 0; i < n; i++)
            cin >> nums[i];
        safe.pb(-1);
        safe.pb(n);
        for (int i = 0; i < n; i++) {
            if (nums[i] + k <= l)
                safe.pb(i);
        }
        sort(safe.begin(), safe.end());
        // for (int i = 0; i < safe.size(); i++)
        //     cout << safe[i] << ' ';
        // cout << '\n';   
        for (int i = 0; i < safe.size() - 1; i++) {
            if (safe[i + 1] - safe[i] == 1)
                continue;
            ind = false;
            now = min(l - nums[safe[i] + 1], k);
            for (int j = safe[i] + 1; j < safe[i + 1]; j++) {
                if (nums[j] > l) {
                    pos = false;
                    break;
                }

                if (!ind) 
                    now = min(now - 1, l - nums[j] - 1);
                else {
                    if (nums[j] + now > l) {
                        pos = false;
                        break;
                    }
                    now++;
                }

                if (now == -1) {
                    ind = true;
                    now = 1;
                }
            }
            if (!pos) {
                break;
            }
        }

        if (pos)
            cout << "YES\n";
        else 
            cout << "NO\n";
    }
    return 0;
}