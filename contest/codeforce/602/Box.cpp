#include <iostream>
#include <vector>
 
using namespace std;
 
int main () {
    int p, now, n;
    cin >> p;
 
    while (p--) {
        cin >> n;
        vector<int> maxes(n), answer(n);
        now = -1;
 
        for(int i = 0; i < n; i++) {
            cin >> maxes[i];
            if(maxes[i] < now) 
                now = -2;
            now = maxes[i];
        }
 
        if(now == -2){
            cout << -1 << "\n";
            continue;
        }
 
        vector<bool> used(n + 1, 0);
        int next = 1;
 
        answer[0] = maxes[0];
        used[answer[0]] = 1;
 
        if(answer[0] == 1)
            next ++;
        
        int i;
 
        for(i = 1; i < n; i++) {
            if(maxes[i] > maxes[i - 1]) {
                answer[i] = maxes[i];
                used[answer[i]] = 1;
            } else {
 
                while(used[next] && next <= maxes[i])
                    next ++;
                
                if(next > maxes[i]) {
                    cout << -1 << "\n";
                    break;
                }
 
                answer[i] = next;
                used[next] = 1;
            }
        }
        if(i != n)
            continue;
 
        for(int i = 0; i < n; i++)
            cout << answer[i] << " ";
        
        cout << "\n";
    }
 
    return 0;
}