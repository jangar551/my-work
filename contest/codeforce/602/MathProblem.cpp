#include <iostream>
#include <vector>
#include <algorithm>
 
using namespace std;
bool secondsort(pair <long long, long long>&a, pair <long long, long long>&b) {
    return a.second < b.second;
}
 
int main () {
    long long t, n, a, b;
    cin >> t;
 
    for(int i = 0; i < t; i++){
        cin >> n;
        vector <pair <long long, long long> > length;
 
        for (long long i = 0; i < n; i++) {
            cin >> a >> b;
            length.push_back(make_pair(a, b));
        }
 
        if (n == 1) {
            cout << 0 << '\n';
            continue;
        }
        sort(length.begin(), length.end());
        a = length.back().first;
 
        sort(length.begin(), length.end(), secondsort);
        b = length[0].second;
 
        if (a - b < 0)
            cout << 0 << "\n";
        else
            cout << a - b << '\n';
    }   
 
	return 0;
}