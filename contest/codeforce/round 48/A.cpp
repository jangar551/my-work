#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll n, m, left = 0;
    cin >> n >> m;
    vector<int> name(n);

    for (int i = 0; i < n; i++) cin >> name[i];

    for (int i = 0; i < n; i++) cout << (left + name[i]) / m << ' ', left = (left + name[i]) % m;

    return 0;
}