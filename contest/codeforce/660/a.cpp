#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n;
    cin >> t;
    while (t--) {
        cin >> n;
        if (n > 6 + 10 + 14) {
            cout << "YES\n";
            if (n == 36 || n == 40 || n == 44)
                cout << 6 << ' ' << 10 << ' ' << 15 << ' ' << n - 1 - 6 - 10 - 14 << '\n';
            else
                cout << 6 << ' ' << 10 << ' ' << 14 << ' ' << n - 6 - 10 - 14 << '\n';
        } else {
            cout << "NO\n";
        }
    }
    return 0;
}