#include <bits/stdc++.h>
#define ff first 
#define ss second
#define ll long long 
#define pb push_back
#define mp make_pair
#define pi pair<int, int> 
#define FAST ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
using namespace std;
int dfs(int now, int last, vector<vector<int> >& path, vector<int>& past, vector<int>& p) {
    past[now] = p[now];
    for(int x : path[now]) {
        if(x != last) {
            past[now] += dfs(x, now, path, past, p);
        }
    }
    return past[now];
}
void go() {
    int n, m, tmp;
    cin >> n >> m;
    vector<vector<int> > path(n);
    vector<int> past(n, 0), p(n), h(n);
    for(int i = 0; i < n; i++)
        cin >> p[i];
    for(int i = 0; i < n; i++) 
        cin >> h[i];
    for(int i = 0; i < n - 1; i++) {
        int a, b;
        cin >> a >> b;
        a--, b--;
        path[a].pb(b);
        path[b].pb(a);
    }
    tmp = dfs(0, 0, path, past, p);
    for(int i = 0; i < n; i++) {
        if(((past[i] % 2) + h[i] % 2) % 2){
            cout << "NO\n";
            return;
        } 
        if(abs(h[i]) > past[i]) {
            cout << "NO\n";
            return;
        }
    }
    vector<bool> vis(n, 0);
    queue<int> searchbfs;
    vis[0] = 1;
    searchbfs.push(0);
    while(!searchbfs.empty()) {
        int now = searchbfs.front();
        searchbfs.pop();
        int newtx = (past[now] - h[now]) / 2;
        int ne = max(0, newtx - p[now]);
        int sum1 = 0;
        for(int x : path[now]) {
            if(!vis[x]) {
                sum1 += (past[x] - h[x]) / 2;
                vis[x] = 1;
                searchbfs.push(x);
            }
        }
        if(sum1 < ne) {
            cout << "NO\n";
            return;
        }
    }
    cout << "YES\n";
}
int main () {
    int t;
    cin >> t;
    while(t--) 
        go();
    
    return 0;
}