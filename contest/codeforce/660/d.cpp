#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define int ll

using namespace std;

vector<int> path, skip;

int ans = 0;

int maxans(vector<vector<int> > to, int now, vector<int> a, vector<int> b) {
    int tmp;
    for (int i = 0; i < to[now].size(); i++) {
        tmp = maxans(to, to[now][i], a, b);
        if(tmp < 0) {
            skip.pb(to[now][i] + 1);
            continue;
        }
        a[now] += tmp;
    }
    if (a[now] >= 0)
        path.pb(now + 1);
    else if (b[now] == -1)
        skip.pb(now + 1);

    ans += a[now];
    return a[now];
}

signed main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int n;
    cin >> n;
    vector<int> a(n), b(n);
    vector<vector<int> > to(n);
    for (int i = 0; i < n; i++)
        cin >> a[i];
    for (int i = 0; i < n; i++) {
        cin >> b[i];
        if (b[i] != -1)
            to[b[i] - 1].pb(i); 
    }

    for (int i = 0; i < n; i++) {
        if (b[i] == -1) 
            maxans(to, i, a, b);
    }
    
    for (int i = skip.size() - 1; i >= 0; i--) {
        path.pb(skip[i]);
    }

    cout << ans << '\n';
    for (int i = 0; i < path.size(); i++) {
        cout << path[i] << ' ';
    }
    cout << '\n';

    return 0;
}