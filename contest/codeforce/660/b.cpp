#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n;
    cin >> t;
    while (t--) {
        cin >> n;
        string answer = "";
        for (int i = 0; i < n * 3 / 4; i++) 
            answer += "9";
        

        for (int i = 0; i < n - n * 3 / 4; i++)
            answer += "8";
                    
        cout << answer << '\n';
    }
    return 0;
}