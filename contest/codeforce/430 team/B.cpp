#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int r, w, n, x, y, rs, ans = 0;

    cin >> r >> w >> n;

    double dist;

    for (int i = 0; i < n; i++) {
        cin >> x >> y >> rs;
        dist = sqrt(x * x + y * y);

        if (dist - rs >= r - w && dist + rs <= r) ans++; 
    }

    cout << ans << '\n';

    return 0;
}