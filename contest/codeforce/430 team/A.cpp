#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int l, r, x, y ,k;
    cin >> l >> r >> x >> y >> k;

    for (int i = l; i <= r; i++) {
        if (i % k == 0 && (i / k <= y && i / k >= x) ) {
            cout << "YES\n";
            return 0;
        } 
    }

    cout << "NO\n";

    return 0;
}