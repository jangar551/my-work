#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;

int main(){
    int n;
    cin >> n;
    int suma = 0, sumb = 0, tmp;

    for (int i = 0; i < n; i++) cin >> tmp, suma += tmp;
    for (int i = 0; i < n; i++) cin >> tmp, sumb += tmp;

    if (suma >= sumb) cout << "Yes\n";
    else cout << "No\n";

    return 0;
}