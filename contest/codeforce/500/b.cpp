#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define INF 100005

using namespace std;

int main(){
    int n, x, tmp;
    cin >> n >> x;

    vector<int> nums(n);
    vector<int> have(INF, 0), changed(INF, 0);

    for (int i = 0; i < n; i++) {
        cin >> nums[i];
        if (have[nums[i]] > 0) {
            cout << 0 << '\n';
            return 0;
        }
        have[nums[i]]++;
    }

    for (int i = 0; i < n; i++) {
        tmp = nums[i] & x;
        if (have[tmp] > 0 && tmp != nums[i] || have[tmp] > 1) {
            cout << 1 << '\n';
            return 0;
        }
        changed[tmp]++;
    }

    for (int i = 0; i < n; i++) {
        tmp = nums[i] & x;
        if (changed[tmp] > 1) {
            cout << 2 << '\n';
            return 0;
        }
    }


    cout << -1 << '\n';
    return 0;
}