#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;


int main(){
    int n, tmp;
    cin >> n;
  
    vector <int> cords(2 * n);

    for (int i = 0; i < 2 * n; i++) cin >> cords[i]; 
    sort(cords.begin(), cords.end());
    
    cout << (cords[n - 1] - cords[0]) * (cords[n * 2 - 1] - cords[n]) << '\n';

    return 0;
}