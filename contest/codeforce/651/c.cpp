#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

bool isprime(int n) {
    for (int i = 2; i <= sqrt(n); i++) 
        if (n % i == 0) return false;
    return true;
}
int main(){
    int t, n, cnt, tmp;
    cin >> t;
    while (t--) {
        cin >> n;
        cnt = 0;
        if (n == 1) {
            cout << "FastestFinger\n";
            continue;
        }
        if (n % 2 == 1 || n == 2) {
            cout << "Ashishgup\n";
            continue;
        }
        tmp = n;
        while (tmp % 2 == 0) tmp /= 2;
        
        if (tmp == 1 || isprime(n / 2)) cout << "FastestFinger\n";
        else cout << "Ashishgup\n";
    }
    return 0;
}