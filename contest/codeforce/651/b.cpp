#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, odd, even, num, cnt;
    cin >> t;
    while (t--) {
        cin >> n;
        odd = even = -1;
        cnt = 0;
        for (int i = 1; i <= 2 * n; i++) {
            cin >> num;
            if (cnt != n - 1) {
            if (num % 2 == 1) {
                if (odd == -1) odd = i;
                else {
                    cout << odd << ' ' << i << '\n'; 
                    odd = -1;
                    cnt++;
                }
            } else {
                if (even == -1) even = i;
                else {
                    cout << even << ' ' << i << '\n'; 
                    even = -1;
                    cnt++;
                }
            }
            }
        }
    }
    return 0;
}