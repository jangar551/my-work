#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, k, cnt = 0, block;
    cin >> n >> k;
    vector<pair<int, int> > nums(n);
    vector<pair<int, int> > lr(n);
    vector<bool> used(n, false);
    for (int i = 0; i < n; i++) {
        cin >> nums[i].ff;
        nums[i].ss = i;
        lr[i].ff = i;
        lr[i].ss = i;
    } 

    sort(nums.begin(), nums.end());
    bool possible;
    for (int i = 0; i < n; i++) {
        possible = true;
        block = 0;
        if ((nums[i].ss > 0 && used[nums[i].ss - 1]) && !(nums[i].ss < n - 1 && used[nums[i].ss + 1]) ) {
            if ((nums[i].ss - lr[nums[i].ss - 1].ff) % 2 == 0) cnt++;
            lr[lr[nums[i].ss - 1].ff].ss = nums[i].ss;
        } else if (nums[i].ss < n - 1 && used[nums[i].ss + 1]) {
            if ((lr[nums[i].ss + 1].ss - nums[i].ss) % 2 == 0) cnt++;
            lr[lr[nums[i].ss + 1].ss].ff = nums[i].ss;
        } else {
            if (lr[nums[i].ss + 1].ss - lr[nums[i].ss - 1].ff % 2 == 0) cnt++;
            lr[lr[nums[i].ss + 1].ss].ff = lr[nums[i].ss - 1].ff;
            lr[lr[nums[i].ss - 1].ff].ss = lr[nums[i].ss + 1].ss;
        }

        if (cnt == (k + 1) / 2) cout << nums[i].ff << '\n'; 
    }
    return 0;
}