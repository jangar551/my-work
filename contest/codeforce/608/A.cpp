#include <iostream>
#include <vector>
#define mp make_pair
#define pb push_back

using namespace std;

int main () {
    int t, s, v, j, c1, c2;
    cin >> t >> s >> v >> j >> c1 >> c2;

    long long ans = 0;

    s = min(s, v);

    if (c1 >= c2) {
        ans += min(t, j) * c1;

        j -= min(t, j);
        
        ans += min(s, j) * c2;

    } else {
        ans += min(s, j) * c2;

        j -= min(s, j);

        ans += min(t, j) * c1;
    }

    cout << ans << "\n";
    return 0;
}