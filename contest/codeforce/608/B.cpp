#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    cin >> n;
    string a;
    cin >> a;

    int w = 0, b = 0;
    for (int i = 0; i < a.size(); i++)
        if (a[i] == 'W') w++;
        else b++;
    
    if (b % 2 == 1 && w % 2 == 1) {
        cout << -1 << '\n';
        return 0;
    }

    if (b == 0 || w == 0) {
        cout << 0 << '\n';
        return 0;
    }


    vector<int> ans;
    if (w % 2 == 0) {
        for(int i = 0; i < a.size() - 1; i++) {
            if (a[i] == 'W') {
                if (a[i + 1] == 'W')  a[i + 1] = 'B';
                else a[i + 1] = 'W';
                ans.push_back(i + 1);
            }
        }
    } else {
        for(int i = 0; i < a.size() - 1; i++) {
            if (a[i] == 'B') {
                if (a[i + 1] == 'W')  a[i + 1] = 'B';
                else a[i + 1] = 'W';
                ans.push_back(i + 1);
            }
        }
    }
    
    cout << ans.size() << '\n';

    for (int i = 0; i < ans.size(); i++)
        cout << ans[i] << ' ';
    cout << '\n';
    return 0;
}