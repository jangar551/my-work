#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, k, mn, ans;
    cin >> t;
    while (t--) {
        cin >> n >> k;
        vector<int> nums(n);
        for (int i = 0; i < n; i++)
            cin >> nums[i];
        sort(nums.begin(), nums.end());
        ans = 0;
        for (int i = 1; i < n; i++) {
            ans += (k - nums[i]) / nums[0];
            nums[i] += ((k - nums[i]) / nums[0]) * nums[0];
        }    
        sort(nums.begin(), nums.end());
        ans += (k - nums[0]) / nums[1];    

        cout << ans << '\n';  
    }
    return 0;
}