#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

bool custom(pair<int, int> a, pair<int, int> b) {
    if (a.ff > b.ff)
        return false;
    else if (a.ff < b.ff)
        return true;
    else
        return a.ss < b.ss;
}

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<int> nums(n);
        vector<int> last(n + 1);
        vector<pair<int, int> > dis(n + 1, make_pair(n + 10, n + 10));
        for (int i = 0; i < n; i++) {
            cin >> nums[i];
            last[i + 1] = -1;
            dis[nums[i]].ff = 0;
            dis[nums[i]].ss = nums[i];
        }

        for (int i = 0; i < n; i++) {
            dis[nums[i]].ff = max(dis[nums[i]].ff, i - last[nums[i]]);
            last[nums[i]] = i;
        }
        
        for (int i = 0; i < n; i++)
            dis[nums[i]].ff = max(dis[nums[i]].ff, n - last[nums[i]]);
            
        sort(dis.begin(), dis.end(), custom);

        int indx = 0, ans = n + 10;
        for (int i = 1; i <= n; i++) {
            while(indx <= n && dis[indx].ff <= i) {
                // cout << dis[indx].ff << ' ' << dis[indx].ss << '\n';
                ans = min(ans, dis[indx].ss);
                indx++;
            }
            // cout << ans << '\n';

            if (ans == n + 10)
                cout << -1 << ' ';
            else
                cout << ans << ' ';
        }
        cout << '\n';
    }
    return 0;
}