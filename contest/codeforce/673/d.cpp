#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    ll t, n, sum;
    cin >> t;
    while (t--) {
        cin >> n;
        sum = 0;
        vector<ll> nums(n);
        for (int i = 0; i < n; i++) {
            cin >> nums[i];
            sum += nums[i];
        }
        
        if (sum % n != 0) {
            cout << -1 << '\n';
            continue;
        }
        cout << 3 * n - 3 << '\n';

        for (int i = 1; i < n; i++) {
            cout << 1 << ' ' << i + 1 << ' ' << ((i + 1) - nums[i] % (i + 1)) % (i + 1) << '\n';
            nums[i] += ((i + 1) - nums[i] % (i + 1)) % (i + 1);
            cout << i + 1 << ' ' << 1 << ' ' << nums[i] / (i + 1) << '\n'; 
        }

        for (int i = 1; i < n; i++) {
            cout << 1 << ' ' << i + 1 << ' ' << sum / n << '\n';
        }
    }
    return 0;
}