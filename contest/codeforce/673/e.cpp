#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

struct node { 
    int val; 
    node* child;
    node* parent;
}; 

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int n;
    ll answer = 0, x = 0;
    cin >> n;
    node* left = new node();
    node* right = new node();
    node* head = new node();
    left->val = -1;
    right->val = -1;
    node* now = left; 
    for (int i = 0; i < n; i++) {
        node* tmp = new node();
        cin >> tmp->val;
        now->child = tmp;
        now = tmp;
    }
    now->child = right;

    ll one, zero, bit = 30, osum, zsum;
    while(bit != -1) {
        now = left->child;
        one = zero = osum = zsum = 0;
        cout << "bit-> " << bit << '\n';
        while (now->val != -1) {
            cout << now->val << '\n';
            if ((now->val) & (1 << bit)) {
                osum += zero;
                one++;
            } else {
                zsum += one;
                zero++;
            }
            now = now->child;
        }

        cout << one << ' ' << zero << ' ' << osum << ' ' << zsum << '\n';
        if (osum < zsum) 
            answer += (1 << bit);
        x += min(osum, zsum);
        
        now = left->child;
        while (now->val != -1) {
            bool rmv = false;
            if (now->val & (1 << bit)) {
                if (osum < zsum)
                    rmv = true;
            } else {
                if (osum > zsum)
                    rmv = true;
            }
            if (rmv) {
                now->parent->child = now->child;
                now->child->parent = now->parent;
            }
            now = now->child;
        }
        bit--;
    }
    cout << x << ' ' << answer << '\n';
    
    return 0;
}