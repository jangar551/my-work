#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    ll t, n, T, num, div;
    cin >> t;
    while (t--) {
        cin >> n >> T;
        unordered_map<ll, bool> used;
        div = 1;
        for (int i = 0; i < n; i++) {
            cin >> num;
            if (used.find(T - num) == used.end()) {
                cout << 0 << ' ';
                used[num] = true;
            } else {
                if (num * 2 == T) {
                    if (div) 
                        cout << 1 << ' ';
                    else
                        cout << 0 << ' ';
                    div = 1 - div;
                } else
                    cout << 1 << ' ';
            }
        }
        cout << '\n';
    }
    return 0;
}