#include <bits/stdc++.h>
#define ll long long
#define ss second
#define ff first
#define pb push_back
#define mp make_pair

using namespace std;

int main () {
    int t, n, same, tmp1, tmp2, ans;
    cin >> t;

    while(t--) {
        cin >>n;
        same = 0;
        vector<int> nums(n), cnt(n + 1, 0);
        unordered_set<int> set;
        for(int i = 0; i < n; i++) {
            cin >> nums[i];
            set.insert(nums[i]);
            cnt[nums[i]]++;
        }

        for(int i = 0; i < n; i++) 
            same = max(cnt[nums[i]], same);
        
        tmp1 = set.size();
        tmp2 = same;
        ans = 0;
        ans = max(min(tmp1 - 1, tmp2), ans);
        ans = max(ans, min(tmp1, tmp2 - 1));
        cout << ans <<"\n";
    }
    return 0;
}