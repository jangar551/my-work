#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, a, b;
    cin >> t;
    while(t--) {
        cin >> n >> a >> b;
        string s = "";
        char tmp;
        while (s.length() != n) {
            for (int i = 0; i < b && s.length() != n; i++) tmp = i + 97, s += tmp; 
        }

        cout << s << '\n';
    }
}