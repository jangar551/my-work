#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t;
    string s;
    cin >> t;
    while(t--) {
        for (int i = 0; i < 9; i++) {
            cin >> s;
            for (int j = 0; j < 9; j++) {
                if (s[j] == '1') cout << 2;
                else cout << s[j]; 
            }

            cout << '\n';
        }
    }
}