#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int findlongest(int l, int r, vector<int> nums) {
    int ans = 0;
    vector<int> count(27, 0);
    for (int i = l; i <= r; i++) count[nums[i]]++;
    for (int i = 1; i < 27; i++) ans = max(ans, count[i]);
    return ans;
}

int findmx(vector<int> count) {
    int answer = 0;
    for (int i = 0; i < 201; i++) answer = max(answer, count[i]);
    return answer;
}

int main(){
    int t, n, ans;
    cin >> t;
    while(t--) {
        cin >> n;
        vector<int> nums(n);
        vector<vector<int> > loc (201);
        vector<vector<int> > longest (n, vector<int> (n, 0));

        for (int i = 0; i < n; i++) cin >> nums[i], loc[nums[i]].pb(i);
        ans = findlongest(0, n - 1, nums);
        for (int i = 0; i < n - 1; i++) {
            vector<int> count(201, 0);
            for (int j = i; j < n; j++) {
                count[nums[j]]++;
                longest[i][j] = findmx(count);
                // cout << i << ' ' << j << ' ' << longest[i][j] << ' ';
                // cout << findlongest(i, j, nums) << '\n';
            }
        }

        for (int p = 1; p < 201; p++) {
            for (int i = 0; i < loc[p].size() / 2; i++) {
                ans = max(ans, (i + 1) * 2 + longest[loc[p][i] + 1][loc[p][loc[p].size() - i - 1] - 1] );
                // cout << longest(loc[p][i] + 1, loc[p][loc[p].size() - i - 1] - 1, nums) << '\n';
                // cout << loc[p][i] << ' ' << loc[p][loc[p].size() - i - 1] << '\n';
                // cout << p << ' ' << i << ' ' << ans << '\n';
            }
        }

        cout << ans << '\n';
    }
}