#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, m;
    cin >> t;
    while(t--) {
        cin >> n >> m;

        if (n * m % 2 == 1) {
            for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (i % 2 == 0) {
                    if (j % 2 == 0) cout << "B";
                    else cout << "W";
                } else {
                    if (j % 2 == 0) cout << "W";
                    else cout << "B";
                }
            }
            cout << '\n';
            }
            
        } else {
            if (n % 2 == 0) {
                for (int i = 0; i < n - 2; i++) {
                    for (int j = 0; j < m; j++) {
                        if (i % 2 == 0) cout << "B";
                        else cout << "W";
                    }
                    cout << '\n';
                }
                for (int i = 0; i < m; i++) if (i % 2 == 0) cout << 'W'; else cout << "B";
                cout << "WW";
                for (int i = 0; i < m - 2; i++) if (i % 2 == 0) cout << 'B'; else cout << "W";
            } else {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < m - 2; j++) {
                        if (i % 2 == 0) cout << "B";
                        else cout << "W";
                    }
                    cout << '\n';
                }
                for (int i = 0; i < m; i++) if (i % 2 == 0) cout << 'W'; else cout << "B";
                cout << "WW";
                for (int i = 0; i < m - 2; i++) if (i % 2 == 0) cout << 'B'; else cout << "W";
            }
        }
    }
}