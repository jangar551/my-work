#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll t, n, m, answer;
    cin >> t;
    while(t--) {
        cin >> n >> m;
        answer = 0;
        if (n % 2 == 1) {
            answer += (m / 2) * (n - 1 + 1);
            if (m % 2 == 1) answer += n / 2 + 1;
        } else {
            answer += (n / 2) * m;
        }

        cout << answer << '\n';
    }
}