#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n;
    cin >> t;
    while(t--) {
        cin >> n;
        vector<int> nums(n);
        for (int i = 0; i < n; i++) cin >> nums[i];

        sort(nums.begin(), nums.end());
        for (int i = n - 1; i >= 0; i--) {
            if (nums[i] <= i + 1) {
                cout << i + 2 << '\n';
                break;
            }
            if (i == 0) cout << 1 << '\n';
        }
    }

    return 0;
}