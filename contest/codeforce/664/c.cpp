#include <bits/stdc++.h>
#define ss second
#define pb push_back
#define ff first
#define pii pair<int, int>
#define FAST ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
#define INF INT_MAX

using namespace std;


int main() {
    int n, m, ans = 0, cnt;
    cin >> n >> m;
    vector<int> a(n), b(m);
    vector<vector<int> > nd(n, vector<int>(m));
    for (int i = 0; i < n; i++) 
        cin >> a[i];
    for (int i = 0; i < m; i++) 
        cin >> b[i];
    
    for (int i = 0; i < n; i++) 
    for (int j = 0; j < m; j++) 
        nd[i][j] = a[i] & b[j];

    for (int bit = 8; bit >= 0; bit--) {
        cnt = 0;
        vector<vector<int> > tmpnd(n);
        for (int j = 0; j < nd.size(); j++) 
        for (int k = 0; k < nd[j].size(); k++) 
            if ((nd[j][k] & (1 << bit)) == 0) 
                tmpnd[j].pb(nd[j][k]);
                
        for (int j = 0; j < n; j++) 
            if (tmpnd[j].size() != 0) 
                cnt++;
        
        if (cnt == n) nd = tmpnd;
        else ans += 1 << bit;
    }
    cout << ans << '\n';


    return 0;
}