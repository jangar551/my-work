#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

void go () {
    ll n, mood, ban;
    cin >> n >> ban >> mood;
    vector<ll> nums(n), suf(n + 1, 0);
    for (int i = 0; i < n; i++)
        cin >> nums[i];
    
    sort(nums.begin(), nums.end());

    for (auto el : nums) {
        if (el > mood) muzzle.pb(el);
        else banFree.pb(el);
    }
    sort(LLA(muzzle));
    if (banFree.size() > 0) preFree.pb(banFree[0]);
    REP1(i, banFree.size()) preFree.pb(preFree.back() + banFree[i]);
    if (muzzle.size() > 0) preMuzzle.pb(muzzle[0]);
    REP1(i, muzzle.size()) preMuzzle.pb(preMuzzle.back() + muzzle[i]);
    ll ans = 0;
    if (muzzle.size() <= 1) {
        for (auto el : nums) ans += el;
        cout << ans << '\n';
        return;
    }
    for (auto el : banFree) ans += el;
    ans += muzzle[0];
    int st = muzzle.size() / (ban + 1);
    // cout << ans << '\n';
    if (muzzle.size() % (ban + 1) != 0) st++;
    for (int i = st; i <= muzzle.size() && (i - 1) * (ban + 1) + 1 <= n; i++) {
        ll curAns = preMuzzle[i - 1];
        int need = (i - 1) * (ban + 1) - muzzle.size();
        if (need < 0) curAns += preFree.back();
        else {
            curAns += preFree.back() - preFree[need];
        }
        // cout << i << ' ' << curAns << '\n';
        ans = max(ans, curAns);
    }
    cout << ans << '\n';
}

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n;
    cin >> t;
    while (t--) {
        cin >> n;

    }
    return 0;
}