#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t;
    cin >> t;
    while (t--) {
        vector<int> nums(4);
        for (int i = 0; i < 4; i++)
            cin >> nums[i];
        
        int odd = 0, zero = 0;
        for (int i = 0; i < 3; i++) {
            if (nums[i] % 2 == 1)
                odd++;
            if (nums[i] == 0)
                zero++;
        }

        if (zero > 0) {
            if(odd + (nums[3] % 2) > 1)
                cout << "No\n";
            else
                cout << "Yes\n";
        } else {
            if (odd == 1 && nums[3] % 2 == 1)
                cout << "No\n";
            else if (odd == 2 && nums[3] % 2 == 0)
                cout << "No\n";
            else
                cout << "Yes\n";
        }   
        
    }
    return 0;
}