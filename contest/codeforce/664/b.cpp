#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int n, m, sx, sy, x, y;
    cin >> n >> m >> sx >> sy;
    vector<pair<int, int> > answer;
    vector<vector<bool> > passed(n + 1, vector<bool>(m + 1, false)); 
    x = sx;
    y = sy;
    for (int k = 0; k < m; k++) {
        answer.pb(mp(x, y));
        passed[x][y] = true;
        int tmpx;
        for (int i = 1; i <= n; i++) {
            if (passed[i][y])
                continue;
            answer.pb(mp(i, y));
            passed[i][y] = true;
            x = i;
        }
        for (int i = 1; i <= m; i++) {
            if (passed[x][i]) continue;
            y = i;
            break;
        }
    }

    for (int i = 0; i < answer.size(); i++)
        cout << answer[i].ff << ' ' << answer[i].ss << '\n';
    return 0;
}