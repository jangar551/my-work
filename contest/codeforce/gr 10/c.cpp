#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    ll t, n, tmp, ans, last;
    cin >> t;
    while (t--) {
        cin >> n;
        ans = 0;
        cin >> last;
        for (int i = 1; i < n; i++) {
            cin >> tmp;
            if (tmp < last)
                ans +=  last - tmp;
            last = tmp;
        }

        cout << ans << '\n';

    }
    return 0;
}