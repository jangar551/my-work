#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    ll t, n, k;
    int mx;
    cin >> t;
    while (t--) {
        cin >> n >> k;
        mx = INT_MIN;
        vector<int> nums(n);
        for (int i = 0; i < n; i++) {
            cin >> nums[i];
            mx = max(mx, nums[i]);
        }
        // cout << mx << '\n';

        // cout << tmp << '\n';
        int newmx = INT_MIN;
        for (int i = 0; i < n; i++) {
            nums[i] = mx - nums[i];
            newmx = max(newmx, nums[i]);
        }            
        mx = newmx;
        if (k % 2 == 1) {
            for (int i = 0; i < n; i++)
                cout << nums[i] << ' ';
            cout << '\n';
        } else {
            for (int i = 0; i < n; i++) 
                nums[i] = mx - nums[i];
            for (int i = 0; i < n; i++)
                cout << nums[i] << ' ';
            cout << '\n';
        }
    }
    return 0;
}
