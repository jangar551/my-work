#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, start, now, answer;
    cin >> t;
    string s;
    while (t--) {
        cin >> n >> s;
        start = 1;
        now = answer = 0;
        while (s[start] == s[start - 1] && start < n)
            start++;
        
        if (start == n) {
            cout << (n + 2) / 3 << '\n';
            continue;
        }

        for (int i = start; i < start + n + 1; i++) {
            if (s[i % n] != s[(i + n - 1) % n]) {
                if (now >= 3) 
                    answer += now / 3;
                now = 1;
            } else {
                now++;
            }
            // cout << now << '\n';
        }

        cout << answer << '\n';
    }
    return 0;
}