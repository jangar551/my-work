#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, first, tmp;
    cin >> t;
    while (t--) {
        cin >> n >> first;
        bool pos = false;
        for (int i = 0; i < n - 1; i++) {
            cin >> tmp;
            if (tmp != first) 
                pos = true;
        }

        if (pos)
            cout << 1 << '\n';
        else
            cout << n << '\n';
    }
    return 0;
}