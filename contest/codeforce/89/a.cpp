#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll t, a, b, answer;
    cin >> t;
    while (t--) {
        cin >> a >> b;
        if (b > a) swap(a, b);
        answer = 0;
        if (b >= a - b) {
            answer += a - b;
            b -= a - b;
            answer += b / 3 * 2;
            if (b % 3 == 2) answer += 1;
        } else {
            answer += min(b, a / 2);
        }
        cout << answer << '\n';
    }
    return 0;
}