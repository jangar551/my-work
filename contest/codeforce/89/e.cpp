#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define MOD 998244353

using namespace std;

int main(){
    ll n, m, indx, answer = 1, minn, count;
    cin >> n >> m;
    if (m > n){
        cout << 0 << '\n';
        return 0;
    }
    vector<int> nums(n), get(m), loc(m, 0), last(m, -1);
    for (int i = 0; i < n; i++) cin >> nums[i];
    for (int i = 0; i < m; i++) cin >> get[i];
    minn = nums[0];
    count = 0;
    indx = m - 1;
    for (int i = n - 1; i >= 0; i--) {
        if (indx == -1) {
            cout << 0 << '\n';
            return 0;
        }
        if (last[indx] == -1 && nums[i] == get[indx]) last[indx] = i;
        if (nums[i] < get[indx]) {
            loc[indx] = i;
            indx--;
            i++;
        }
    }

    for (int i = 0; i < m; i++) {
        if (last[i] == -1) {
            cout << 0 << '\n';
            return 0;
        }
    }

    for (int i = 1; i < m; i++) {
        answer *= last[i] - loc[i];
        answer %= MOD;
    }

    cout << answer << '\n';
  
    return 0;   
}