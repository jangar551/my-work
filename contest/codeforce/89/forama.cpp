#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define MOD 998244353

using namespace std;

int main(){
    ll n, m, now, res = 1;
    cin >> n >> m;
    if (m > n){
        cout << 0 << '\n';
        return 0;
    }
    vector<int> from(n), to(m), first(m, 0), last(m, -1);
    for (int i = 0; i < n; i++) cin >> from[i];
    for (int i = 0; i < m; i++) cin >> to[i];
    now = m - 1;
    
    int i;
    for (i = n - 1; i >= 0; i--) {
        if (now == -1) 
            break;
        
        if (from[i] == to[now] && last[now] == -1) last[now] = i;
        if (from[i] < to[now]) {
            first[now] = i;
            now--;
            i++;
        }
    }
    if (i != -1) {
        cout << 0 << '\n';
        return 0;
    }

    for (int i = 0; i < m; i++) {
        if (last[i] == -1) {
            cout << 0 << '\n';
            return 0;
        }
    }

    for (int i = 1; i < m; i++) {
        res *= last[i] - first[i];
        res %= MOD;
    }

    cout << res << '\n';
  
    return 0;   
}