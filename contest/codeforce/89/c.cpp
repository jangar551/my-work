#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll t, n, m, cell, answer;
    cin >> t;
    while (t--) {
        cin >> n >> m;
        answer = 0;
        vector<pair<int, int> > count(n + m + 10, mp(0, 0));
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                cin >> cell;
                if (cell == 1) count[i + j].ff++;
                else count[i + j].ss++;
            }
        }
        for (int i = 0; i < (m + n - 1) / 2; i++) 
            answer += min(count[i].ff + count[m + n - i - 2].ff, count[i].ss + count[m + n - i - 2].ss);
        
        cout << answer << '\n';
    }
    return 0;
}