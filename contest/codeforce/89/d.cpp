#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

vector<int> primes;

void SieveOfEratosthenes() { 
    int n = 10000000;
    vector<bool> prime(n+1, true); 
  
    for (int p=2; p*p<=n; p++){    
        if (prime[p] == true) { 
            for (int i=p*p; i<=n; i += p) 
                prime[i] = false; 
        } 
    } 
  
    for (int p=2; p<=n; p++) 
       if (prime[p]) 
            primes.pb(p);
} 

int main(){
    ll n, num, indx, cnt;
    cin >> n;
    SieveOfEratosthenes();
    cout << primes.size() << '\n';
    vector<pair<int, int> > answer;
    for (int i = 0; i < n; i++) {
        cin >> num;
        indx = 0;
        cnt = -1;
        while (primes[indx] <= num) {
            if (num % primes[indx] == 0) {
                if (cnt == -1)
                    cnt = primes[indx];
                else {
                    answer.pb(mp(cnt, primes[indx]));
                    break;
                }
            }
            indx++;
            if (primes[indx] > num) 
                answer.pb(mp(-1, -1));
            
        }
    }

    for (int i = 0; i < n; i++) cout << answer[i].ff << ' ';
    cout << '\n';
    for (int i = 0; i < n; i++) cout << answer[i].ss << ' ';
    cout << '\n';

    return 0;
}