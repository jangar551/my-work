#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, m, x, l, r;
    cin >> t;
    while (t--) {
        cin >> n >> x >> m;
        vector<pair<int, int> > limit(m);
        for (int i = 0; i < m; i++) cin >> limit[i].ff >> limit[i].ss;
        l = r = x;
        for (int i = 0; i < m; i++) {
            if ((limit[i].ff >= l && limit[i].ff <= r) || 
                (limit[i].ss >= l && limit[i].ss <= r) || 
                (limit[i].ff <= l && limit[i].ss >= r)) {
                l = min(l, limit[i].ff);
                r = max(r, limit[i].ss);
            }
        }

        cout << r - l + 1 << '\n';
    }
    return 0;
}