#include <iostream>
#include <vector>
#include <algorithm>
 
using namespace std;
 
void go() {
    
}
int main () {
    int t;
    cin >> t;
 
    for(int i = 0; i < t; i++){
        int sums = 0;
        vector <int> nums(3);
        for (int j = 0; j < 3; j++) {
            cin >> nums[j];
            sums += nums[j];
        }
 
        sort(nums.begin(), nums.end());
 
        if (nums[2] >= nums[0] + nums[1]) {
            cout << (nums[0] + nums[1]) << '\n';
            continue;
        }
        
        if (sums & 1) 
            cout << (sums - 1) / 2 << '\n';
        else 
            cout << sums / 2 << '\n';
    };
 
	return 0;
}