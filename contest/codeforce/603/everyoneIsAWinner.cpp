#include <iostream>
#include <set>
#include <cmath>
#include <algorithm>
 
using namespace std;
 
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    long long t, number, lim;
    cin >> t;
    for(int i = 0; i < t; i++){
        cin >> number;
        lim = sqrt(number);
        set <long long> answer;
 
        for (int i = 0; i <= lim; i++) {
            answer.insert(i);
            if (i != 0) 
                answer.insert(number / i);
        }
        
        cout << answer.size() << '\n';
        for (auto el : answer) 
            cout << el << ' ';
        
        cout << '\n';
    }
	return 0;
}