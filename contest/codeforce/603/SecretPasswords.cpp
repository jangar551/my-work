#include <iostream>
#include <vector>
 
using namespace std;
 
int Find(int i, vector<int>& par) {
    if (par[i] == i) 
        return i;
    return par[i] = Find(par[i], par);
}
 
void mrg(int a, int b, vector<int>& par) {
    int start = Find(b, par);
    int finish = Find(a, par);
    par[start] = par[finish];
}
 
int main(){
    int n, ans = 0;
    string input;
    cin >> n;
    vector <int> par;
    vector <bool> have(26, 0);
 
    for (int i = 0; i < 26; i++) 
        par.push_back(i);
 
    for (int i = 0; i < n; i++) {
        cin >> input;
        for (int j = 0; j < input.size(); j++) {
            if (!have[input[j] - 'a']) 
                have[input[j] - 'a'] = 1;
 
            for (int k = j + 1; k < input.size(); k++) 
                mrg(Find(input[j] - 'a', par), Find(input[k] - 'a', par), par);
 
        }
    }
 
    for (int i = 0; i < 26; i++) {
        if (!have[i])
            continue;
 
        if (Find(i, par) == i) 
            ans++;
    }
 
    cout << ans << '\n';
	return 0;
}