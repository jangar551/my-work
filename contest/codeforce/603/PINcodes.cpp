#include <iostream>
#include <vector>
#include <unordered_map>
#define ss string
#define v vector
using namespace std;
 
char tochar (int a) {
    return char(a);
}
int toint (char a) {
    return int(a);
}
void solve() {
    int n, resukt = 0;
    ss tmp;
    cin >> n;
 
    v<ss> ans;
    v<v<bool> > udig(4, v<bool> (10, 0));
    unordered_map<ss, int> usedpass;
    
    v<bool> indeces(n, 0);
    for (int i = 0; i < n; i++) {
        cin >> tmp;
        ans.push_back(tmp);
 
        if(usedpass.find(tmp) == usedpass.end()){
            indeces[i] = 1;
            usedpass[tmp] = 1;
            for (int j = 0; j < 4; j++)
                udig[j][toint(tmp[j] - '0')] = 1;
            
        }
    }
 
    for (int i = 0; i < ans.size(); i++) {
        if(indeces[i]) continue;
        resukt++;
        for (int j = 0; j < 4; j++) {
            for(int p = 0; p < 10; p++){
                if(!udig[j][p]) {
                    ans[i][j] = tochar(p + int('0'));
                    udig[j][p] = 1;
                    j = 3;
                    break;
                }
            }
        }
    }
    cout << resukt << "\n";
    for(int i = 0; i < n; i++) {
        cout << ans[i] << "\n";
    }
}
 
int main () {
    int t;
    cin >> t;
 
    for(int i = 0; i < t; i++) {
        solve();
    }
    return 0;
}