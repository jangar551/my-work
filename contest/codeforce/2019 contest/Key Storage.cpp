#include <iostream>
#include <vector>
#include <algorithm>
#define ll unsigned long long

using namespace std;

vector<int> tovector(ll n){
    vector<int> ans;
    int tmp = 2;
    while(n != 0){
        ans.push_back(n % tmp);
        n /= tmp;
        tmp++;
    }

    return ans;
}

ll factorial(ll n){
    if(n == 0)
        return 1;
    return n * factorial(n - 1);
}

void duplicate(vector<int>& nums, ll& n){
    vector<int> count(22, 0);//countsort, I know max num is 20, but to be sure 21
    for (int i = 0; i < nums.size(); i++)
        count[nums[i]]++;
    
    for (int i = 0; i < 22; i++)
        n /= factorial(count[i]);
}

ll solve (ll k) {
    vector<int> nums = tovector(k);
    sort(nums.begin(), nums.end());
    // for(int i = 0; i < nums.size(); i++)
        // cout << nums[i] << ' ';
    // cout << '\n';
    ll now = nums.size() + 1 - nums.back(), ans = now, zero, all;
    if (nums[0] == 0) {//lets count how many 0 ending comb is there
        now--;//since last digit is 0
        // cout << "zr now " << now << '\n';
        zero = now;//ans = now ==>> zero = now
        for (int i = nums.size() - 2; i > 0; i--) {// i > 0 since we already  counted first digit
            if (nums[i] != 0 && nums[i] != 1) now += nums[i + 1] - nums[i] - 1;
            else now = i;
        
            if (now == 0) { 
                zero = 0;
                break;
            }
            // cout << now << " zr " << zero << '\n'; //debuuug
            zero *= now;
        }
    } else zero = 0;

    now = nums.size() + 1 - nums.back();
    // cout << "now " << now << '\n';

    for (int i = nums.size() - 2; i >= 0; i--) {
        if (nums[i] != 0) now += nums[i + 1] - nums[i] - 1;
        else now = i + 1;
        if (now == 0) { 
            ans = 0;
            break;
        }

        // cout << now << '\n'; //debuuug
        
        ans *= now;
    }
    // cout << ans << ' ' << zero << '\n';
    duplicate(nums, ans);
    swap(nums[0], nums.back());
    nums.pop_back();
    duplicate(nums, zero);

    // cout << zrdivide << ' ' << divide << '\n'; 
    if(ans - zero > 0) return ans - zero - 1;
    return 0;
}

int main () {
    ll t, k;
    cin >> t;
    // cout << "working\n";
    vector<ll> result(t);
    for (ll i = 0; i < t; i++) {
        cin >> k;
        result[i] = solve(k);
    }

    for (int i = 0; i < t; i++)
        cout << result[i] << '\n';
    
    return 0;
}