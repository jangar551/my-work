#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
#define umap unordered_map
using namespace std;

vector<int> s(int a, int b) {
    vector<int> result;
    result.push_back(a / b + 1);
    if(a % b == 0)
        result.push_back(a / b);
    return result;
}

int success(int a, int b) {
    if(a % b == 0)
        return a / b;
    if( ((a / b) + 1) * (b - 1) <= a) 
        return a / b + 1;
    return 0;
}

int solve() {
    int n;
    cin >> n;
    vector<int> input(n);
    int sum = 0;
    umap<int, int> catagory;
    for(int i = 0; i < n; i++) {
        cin >> input[i];
        catagory[input[i]]++;
    }
    vector<int> cat;
    for(auto el : catagory) {
        cat.push_back(el.second);
    }
    sort(cat.begin(), cat.end());
    for(int i = 1; i <= cat[0]; i++) {
        vector<int> sizes = s(cat[0], i);
        for(auto el : sizes) {
            sum += i;
            for(int j = 1; j < cat.size(); j++) {
                int temp = success(cat[j], el);
                if(temp == 0) {
                    sum = 0;
                    break;
                }
                sum += temp;
            }
            if(sum != 0) {
                return sum;
            }
        }
    }
}

int main () {
    int t;
    cin >> t;
    vector<int> result(t);
    for(int i = 0; i < t; i++)
        result[i] = solve();
    for(int i = 0; i < t; i++) {
        cout << result[i] << "\n";
    }
    return 0;
}
