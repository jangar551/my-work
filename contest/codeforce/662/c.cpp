#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, num, mx, cnt;
    cin >> t;
    while (t--) {
        cin >> n;
        mx = 0;
        cnt = 0;
        vector<int> count(n + 2, 0);

        for (int i = 0; i < n; i++) {
            cin >> num;
            count[num]++;
            mx = max(mx, count[num]);
        }

        for (int i = 1; i <= n; i++) {
            if (count[i] == mx)
                cnt++;
        }
        // cout << n << ' ' << mx << ' ' << cnt << '\n';
        cout << (n - cnt * mx) / (mx - 1) + (cnt - 1) << '\n'; 
    }
    return 0;
}