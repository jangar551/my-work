#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define MAX 100002

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int n, q, num, two = 0, four = 0;
    cin >> n;
    char type;

    vector<int> nums(n), count(MAX, 0);
    
    for (int i = 0; i < n; i++) {
        cin >> nums[i];
        if (count[nums[i]] % 4 == 3)
            four++, two--;
        else if (count[nums[i]] % 2 == 1)
            two++;
        count[nums[i]]++;
    }
    cin >> q;

    for (int i = 0; i < q; i++) {
        cin >> type >> num;
        if (type == '+') {
            if (count[num] % 4 == 3)
                four++, two--;
            else if (count[num] % 2 == 1)
                two++;
            count[num]++;
        } else {
            if (count[num] % 4 == 0)
                four--, two++;
            else if (count[num] % 2 == 0)
                two--;
            count[num]--;
        }

        if (four >= 2 || (four == 1 && two >= 2))
            cout << "YES\n";
        else 
            cout << "NO\n";
    }
    return 0;
}