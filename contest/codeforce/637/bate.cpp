void go() {
    int n;
    cin >> n;
    VI nums = readVI(n), dp(n, 0);
    umap <int, int> pos;
    REP(i, n) pos[nums[i]] = i;

    REP1(i, n + 1) {
        // cout << '\n';
        // cout << '\n';
        int cur = pos[i];
        while(cur < n && dp[cur] == 0) {
            dp[cur++] = i++;
        }
        i--;
        // print(dp);
        // print(nums);
    }
    REP(i, n) {
        if (nums[i] != dp[i]) {
            cout << "No\n";
            return;
        }
    }
    cout << "Yes\n";
}