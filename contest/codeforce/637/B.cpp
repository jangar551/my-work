#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, k;
    cin >> t;
    while(t--) {
        cin >> n >> k;
        vector<int> mntn(n);
        for (int i = 0; i < n; i++) cin >> mntn[i];
        vector<bool> peak(n, false);

        for (int i = 1; i < n - 1; i++) if (mntn[i] > mntn[i - 1] && mntn[i] > mntn[i + 1]) peak[i] = true;

        int left = 1, answer = 0, cnt = 0;
        for (int i = 1; i < k - 1; i++) if (peak[i]) answer++;
        cnt = answer;

        for (int i = k; i < n; i++) {
            if (peak[i - k + 1]) cnt--;
            if (peak[i - 1]) cnt++;

            if (cnt > answer) left = i - k + 2, answer = cnt;
        }

        cout << answer + 1 << ' ' << left << '\n';
    }
}