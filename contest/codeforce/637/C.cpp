#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, tmp, back, indx;
    cin >> t;
    while(t--) {
        cin >> n;
        back = n;

        vector<int> loc(n + 1);
        for (int i = 1; i < n + 1; i++) cin >> tmp, loc[tmp] = i;

        int i;
        for (i = 1; i < n + 1; i++) {
            if (loc[i] == back) { 
                back--;
                continue;
            }
            indx = i;
            tmp = loc[i];
            while (tmp <= back && loc[i] == tmp) tmp++, i++;
            if (tmp != back + 1) break;
            back = loc[indx] - 1;
            i--;
        }

        if (i == n + 1) cout << "Yes\n";
        else cout << "No\n";

//      4
//      4 2 3 1
//      5
//      2 3 4 5 1
    }

    return 0;
}