#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

vector<string> digit {"1110111", "0010010", "1011101", "1011011", "0111010", "1101011", "1101111", "1010010", "1111111", "1111011"};

int find_min(string a) {
    int ans = INT_MAX, cnt;
    for (int i = 0; i < digit.size(); i++) {
        cnt = 0;
        int j;
        for (j = 0; j < 6; j++) {
            if (digit[i][j] == '0' && a[j] == '1') break;
            if (digit[i][j] == '1' && a[j] == '0') cnt++;
        }
        if (j == 7) ans = min(ans, cnt);
    }

    return ans;
}

int main(){
    int n, k, sum = 0;
    cin >> n >> k;
    vector<string> nums(n);
    vector<int> min_change(n);
    for (int i = 0; i < n; i++) cin >> nums[i];

    for (int i = 0; i < n; i++) min_change[i] = find_min(nums[i]), sum += min_change[i];

    for (int i = 0; i < n; i++) {
        
    }
}