#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, a, b, c, d;
    cin >> t;
    while (t--) {
        cin >> n >> a >> b >> c >> d;

        if ((a - b) * n > c + d || (a + b) * n < c - d) cout << "NO\n";
        else cout << "YES\n";
    }
}