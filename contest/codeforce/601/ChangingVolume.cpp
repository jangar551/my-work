#include <iostream>
 
using namespace std;
 
int main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
 
    int t, a, b, d, answer;
    
    cin >> t;
    
    for(int i = 0; i < t; i++){
        answer = 0;
        cin >> a >> b;
 
        d = abs(a - b);
 
        answer += d / 5;
        d %= 5;
 
        answer += d / 2;
        d %= 2;
 
        answer += d;
        
        cout << answer << '\n';
    }
	return 0;
}