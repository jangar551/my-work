#include <iostream>
#include <vector>
 
using namespace std;
 
char chicken[] = 
{'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9'};
 
int main () {
    int t, r, c, k, rice = 0, one_chicken, more, index = 0;
    cin >> t;
 
    for(int i = 0; i < t; i++) {
        rice = 0;
        index = 0;
        cin >> r >> c >> k;
 
        vector<string> field(r);
 
        for(int i = 0; i < r; i++) 
            cin >> field[i];
        
        for(int i = 0; i < r; i++) 
        for(int j = 0; j < c; j++) 
            if(field[i][j] == 'R')
                rice ++;
            
        
        vector<int> own(k);
 
        one_chicken = rice / k;
        more = rice % k;
 
        for(int i = 0; i < k; i++) {
            own[i] = one_chicken;
            if(i < more) 
                own[i]++;
        }
 
        for(int i = 0; i < r; i++) {
            if(i % 2 == 0){
                for(int j = 0; j < c; j++) {
                    
                    if(field[i][j] == 'R') {
                        if(own[index] == 0) {
                            index ++;
                        }
                        own[index]--;
                    }
                    field[i][j] = chicken[index];
                }
            }else{  
                for(int j = c - 1; j >= 0; j--) {
                    if(field[i][j] == 'R') {
                        if(own[index] == 0) {
                            index ++;
                        }
                        own[index]--;
                    }
                    field[i][j] = chicken[index];
                }
            }
        }
        for(int i = 0; i < r; i++) {
            for(int j = 0; j < c; j++) {
                cout << field[i][j];
            }
            cout << endl;
        }
    }
    return 0;
}