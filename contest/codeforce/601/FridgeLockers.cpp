#include <iostream>
#include <vector>
#include <algorithm>
 
using namespace std;
 
int main () {
    int t, n, m, ans;
    cin >> t;
 
    for(int i = 0; i < t; i++) {
        cin >> n >> m;
        ans = 0;
 
        vector<pair<int, int> > price(n);
        vector<pair<int, int> > connection;
 
        for(int i = 0; i < n - 1; i++) {
            cin >> price[i].first;
            price[i].second = i + 1;
 
            ans += price[i].first * 2;
            connection.push_back(make_pair(i + 1, (i + 2)));
        }
        cin >> price[n - 1].first;
        price[n - 1].second = n;
 
        ans += price[n - 1].first * 2;
        connection.push_back(make_pair(n , 1));
 
        if(n > m || n == 2) {
            cout << -1 << "\n";
            continue;
        }    
 
        m -= n;
 
        sort(price.begin(), price.end());
 
        for(int i = 0; i < m; i++) {
            ans += price[0].first + price[1].first;
            connection.push_back(make_pair(price[0].second, price[1].second));
        }
        cout << ans << "\n";
 
        for(int i = 0; i < connection.size(); i++) {
            cout << connection[i].first << " " << connection[i].second << "\n";
        }
 
    }
 
    return 0;
}