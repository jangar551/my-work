#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int r, g, b, num;
    cin >> r >> g >> b;

    vector<priority_queue <int> > pq(3);

    for (int i = 0; i < r; i++) {
        cin >> num;
        pq[0].push(num);
    }
    for (int i = 0; i < g; i++) {
        cin >> num;
        pq[1].push(num);
    }
    for (int i = 0; i < b; i++) {
        cin >> num;
        pq[2].push(num);
    }
    ll answer = 0, plus;
    while ((!pq[1].empty() && !pq[2].empty()) || (!pq[1].empty() && !pq[0].empty()) || (!pq[0].empty() && !pq[2].empty())) {
        if (pq[2].empty()) {
            answer += pq[1].top() * pq[0].top();
            pq[1].pop();
            pq[0].pop();
        } else if (pq[1].empty()) {
            answer += pq[2].top() * pq[0].top();
            pq[2].pop();
            pq[0].pop();
        } else if (pq[0].empty()) {
            answer += pq[1].top() * pq[2].top();
            pq[1].pop();
            pq[2].pop();
        } else {
            plus = 1;
            priority_queue<int> now;
            now.push(pq[1].top());
            now.push(pq[0].top());
            now.push(pq[2].top());

            for (int i = 0; i < 3; i++) {
                if (now.size() == 1)
                    break;
                if (pq[i].top() == now.top()) {
                    plus *= pq[i].top();
                    pq[i].pop();
                    now.pop();
                }
            }

            answer += plus;
        }
    }

    cout << answer << '\n';
    return 0;
}