#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, now, answer;
    string s;
    cin >> t;
    while (t--) {
        cin >> s;
        now = answer = 0;
        vector<int> len;
        for (int i = 0; i < s.length(); i++) {
            if (s[i] == '0') {
                if (now != 0) 
                    len.pb(now), now = 0;
            } else {
                now++;
            }
        }

        if (now != 0)
            len.pb(now);

        sort(len.begin(), len.end());
        for (int i = len.size() - 1; i >= 0; i-= 2) {
            answer += len[i];
        }

        cout << answer << '\n';
    }
    return 0;
}