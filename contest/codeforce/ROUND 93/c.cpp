#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n;
    string str;
    cin >> t;
    while (t--) {
        cin >> n >> str;
        ll pos = 0;
        unordered_map<int, int> passed;
        int pre = 0;
        passed[0] = 1;
        for (int i = 0; i < n; i++) {
            if (str[i] == '0')
                pre += 1;
            else
                pre += 1 - (str[i] - '0');
            pos += passed[pre];
            passed[pre]++;
        }
        cout << pos << '\n';
    }
    return 0;
}