#include <iostream>
 
using namespace std;
 
int main () {
    long long t;
    cin >> t;
 
    for(long long i = 0; i < t; i++) {
        long long x, y;
        cin >> x >> y;
        
        if(x >= y) {
            cout << "yes\n";
        }else if(x == 1) {
            cout << "no\n";
        }else if(x == 3 || (x == 2 && y != 3)) {
            cout << "No\n";
        }else
            cout << "Yes\n";
    }
 
    return 0;
}