#include <iostream>
 
using namespace std;
int main () {
    int t, n, a, b, m;
 
    cin >> t;
 
    for (int i = 0; i < t; i++) {
        cin >> n >> m >> a >> b;
 
        if (a > b) swap(a, b);
            int x = a - 1, y = n - b, answer = b - a + min(x + y, m);
 
        cout << answer << '\n';
    }   
    return 0;
}