#include <iostream>
#include <vector>
 
using namespace std;
 
int main() {
	int p;
	cin >> p;
 
	while (p > 0) {
		int n;
		cin >> n;
 
		vector<int> test(n);
		for(int i = 0; i < n; i++) 
			cin >> test[i];
 
		int result = -1;
		vector<pair<int, int> > el(n + 1, make_pair(-1, -1));
		for(int i = 0; i < n; i++) {
			if (el[test[i]].first == -1) {
				el[test[i]].first = i;
				continue;
			}
			if (el[test[i]].second == -1) {
				el[test[i]].second = i;
				int tmp = i - el[test[i]].first + 1;
				if (result == -1 || result > tmp)
					result = tmp;
				continue;
			}
			el[test[i]].first = el[test[i]].second;
			el[test[i]].second = i;
	
			int tmp = i - el[test[i]].first + 1;
			if (result > tmp)
				result = tmp;
		}
		cout << result << endl;
		p--;
	}
 
	return 0;
}