#include <iostream>
#include <unordered_map>
#include <vector>
#include <set>

using namespace std;

struct type1 {
    vector <long long> numbers;
    long long sums;
    type1() {
        sums = 0;
    }
};
long long n;
unordered_map <long long, type1> partition;
void adder(long long a, long long b) {
    partition[a].sums += b;
}
int main () {
    cin >> n;
    set <long long> values;
    vector <long long> numbers(n), cost(n);

    for (int i = 0; i < n; i++)  {
        cin >> numbers[i];
        values.insert(numbers[i]);
    }
    for (int i = 0; i < n; i++)  
        cin >> cost[i];
    for (int i = 0; i < n; i++) {
        partition[numbers[i]].sums += cost[i];
        partition[numbers[i]].numbers.push_back(cost[i]);
    }
    multiset <long long, greater <long long> > curnumbers;
    multiset <long long, greater <long long> > :: iterator it;
    
    long long sums = 0, answer = 0;

    for (auto element : values) {
        for (int i = 0; i < partition[element].numbers.size(); i++) 
            curnumbers.insert(partition[element].numbers[i]);
        sums = partition[element].sums;
        if (curnumbers.size() == 1) {
            curnumbers.clear();
            continue;
        }
        it = curnumbers.begin();
        long long tmp = *it;
        curnumbers.erase(it);
        sums -= tmp;
        answer += sums;
        values.insert(element + 1);
        adder(element + 1, sums);
    }
    cout << answer << '\n';
    return 0;
}