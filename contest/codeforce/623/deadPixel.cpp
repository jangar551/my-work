#include <iostream>
#include <algorithm>

#define INF INT_MAX
using namespace std;

int main () {
    int t;
    cin >> t;
    
    int a, b, x, y, answer;
    int tmp1, tmp2;
    
    while(t--) {
        answer = -INF;
        cin >> a >> b >> x >> y;
        x++, y++;
        
        tmp1 = a, tmp2 = y - 1;
        answer = max(answer, tmp1 * tmp2);

        tmp1 = a - x, tmp2 = b;
        answer = max(answer, tmp1 * tmp2);

        tmp1 = x - 1, tmp2 = b;
        answer = max(answer, tmp1 * tmp2);

        tmp1 = a, tmp2 = b - y;
        answer = max(answer, tmp1 * tmp2);

        cout << answer << '\n';
    };
    return 0;
}