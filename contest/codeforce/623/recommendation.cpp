#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>

using namespace std;

int main(){
    int n;
    cin >> n;

    vector <int> count(n), m_time(n, 0), s_time(n, 0), pub(n), time(n), cnt(n, 0);
    
    for (int i = 0; i < n; i++) cin >> pub[i];
    for (int i = 0; i < n; i++) cin >> time[i];
    
    for (int i = 0; i < n; i++) {
        count[pub[i]] += time[i];
        cnt[pub[i]]++;
        if (m_time[pub[i]] < time[i]) m_time[i] = time[i], s_time[pub[i]] = max(m_time[pub[i]], s_time[pub[i]]);
        else max(s_time[pub[i]], time[i]);
    }


    int answer = 0;

    for (int i = 0; i < n; i++) {
        cout << answer << '\n';
        if (cnt[i] > 1) {
            answer += count[i] - m_time[i];
            if (i != n - 1) {
                count[i + 1] += count[i] - m_time[i];
                cnt[i + 1] += 2;
                if (m_time[i + 1] < m_time[i]) s_time[i] = max(m_time[i + 1], s_time[i]), m_time[i + 1] = m_time[i];
                else s_time[i + 1] = max(m_time[i], s_time[i + 1]); 
            }
        }
    }

    cout << answer << '\n';
    return 0;
}