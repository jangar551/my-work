#include <iostream>
#include <vector>

using namespace std;


int main () {
    int t, n;
    cin >> t;

    vector<int> nums(1000), answer(1000);

    while(t--) {
        cin >> n;
        for (int i = 1; i <= n; i++) 
            cin >> nums[i];
       
        vector <bool> saw(1000, 0);

        for (int i = 1; i <= n; i++) {
            answer[i * 2 - 1] = nums[i];
            saw[nums[i]] = 1;
        }

        int now = 1;
        for (int i = 1; i <= n; i++) {
            now = answer[i * 2 - 1] + 1;
            while(saw[now]) 
                now++;
            answer[i * 2] = now;
            saw[now] = 1;
        }
        int j;
        for (j = 1; j <= 2 * n; j++) {
            if (!saw[j]) {
                cout << "-1\n";
                break;
            }
        }

        if (j != 2 * n + 1) continue;

        for (int i = 1; i <= 2 * n; i++) 
            cout << answer[i] << ' ';
        cout << '\n';
    }
    return 0;
}