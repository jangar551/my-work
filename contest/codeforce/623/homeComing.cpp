#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>
#define INF INT_MAX

using namespace std;

int main(){
    int t;
    cin >> t;

    int a, b, p; //bus pay, tram pay, balance
    string trans;//transport info
    int min_price; // min price to get to nth city
    char last; // last station

    while (t--) {
        cin >> a >> b >> p >> trans;
            
        if (trans[trans.length() - 2] == 'A') min_price = a;
        else min_price = b;
        last = trans[trans.length() - 2];

        if (min_price > p) {
            cout << trans.length() << '\n';
            continue;
        }
        if (trans.length() == 2) {
            cout << 1 << '\n';
            continue;
        }

        for (int i = trans.length() - 3; i >= 0; i--) {
            if (trans[i] != last) 
                if (trans[i] == 'A') min_price += a;
                else min_price += b;
            
            if (min_price > p) {
                cout << i + 2 << '\n';
                break;
            }

            if (i == 0)
                cout << 1 << '\n';
            last = trans[i];
        }
    }

    return 0;
}