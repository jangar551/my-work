#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

bool subseqsum(vector<int> len, int sum) {
    vector<bool> dp(5000, 0), newdp, clean(5000, 0);

    for (int i = 0; i < len.size(); i++) {
        newdp = clean;
        dp[0] = true;
        for (int j = 0; j < 5000 - len[i]; j++) {
            if (dp[j]) {
                newdp[j] = true;
                newdp[j + len[i]] = true;
            }
        }
        dp = newdp;
    }

    return dp[sum];
}

int main(){
    int t, n, now;
    cin >> t;
    bool first;
    while (t--) {
        cin >> n;
        first = true;
        vector<int> nums(2 * n), len;
        now = 1;
        for (int i = 0; i < 2 * n; i++)
            cin >> nums[i];

        for (int i = 1; i < 2 * n; i++) {
            if (nums[i] < nums[i - now]) 
                now++;
            else {
                len.pb(now);
                now = 1;
            }
        }
        len.pb(now);


        if (subseqsum(len, n))
            cout << "YES\n";
        else
            cout << "NO\n";
    }
    return 0;
}