#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, m, num, answer;
    cin >> t;
    while (t--) {
        cin >> n >> m;
        answer = -1;
        vector<bool> have(2000, 0);

        for (int i = 0; i < n; i++) {
            cin >> num;
            have[num] = true;
        }
        
        for (int i = 0; i < m; i++) {
            cin >> num;
            if (have[num]) 
                answer = num;
        }

        if (answer == -1)
            cout << "NO\n";
        else 
            cout << "YES\n" << 1 << ' ' << answer << '\n';
    }
    return 0;
}