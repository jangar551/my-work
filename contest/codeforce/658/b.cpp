#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n;
    bool win;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<int> nums(n);
        for (int i = 0; i < n; i++)
            cin >> nums[i];

        win = true;
        for (int i = n - 2; i >= 0; i--) {
            if (nums[i] == 1) 
                win = !win;
            else if (!win)
                win = true;
        }

        if (win)
            cout << "First\n";
        else 
            cout << "Second\n";
    }
    return 0;
}