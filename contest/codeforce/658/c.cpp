#include <bits/stdc++.h>
#define ff first 
#define ss second
#define ll long long 
#define pb push_back
#define mp make_pair
#define FAST ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
using namespace std;
void solve() {
    int n;
    string a, b;
    cin >> n >> a >> b;
    int r = n - 1, l = 0, i = n - 1;
    vector<int> p;
    bool ind = 0;
    while(i >= 0) {
        if(!ind) {
            while(i >= 0 && a[r] == b[i])
                i--, r--;
            if(i < 0)
                break;
            if(a[l] == b[i]) 
                p.pb(0);
            p.pb(i);
            l++, i--;
        } else {
            // cout << "here\n";
            while(i >= 0 && a[l] != b[i]) 
                l++, i--;
            if(i < 0)
                break;
            if(a[r] != b[i]) 
                p.pb(0);
            p.pb(i);
            r--, i--;
        }
        ind ^= 1;
    }

    cout << p.size() << " ";
    for(int x : p) 
        cout << x + 1<< " ";
    cout << "\n";
}
int main () {
    int t;
    cin >> t;
    while(t--) {
        solve();
    }
    return 0;
}