#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, x, y, over, tmpx, uniq, dup, indx;
    cin >> t;
    while (t--) {
        cin >> n >> x >> y;
        dup = y - x;
        tmpx = x;
        vector<int> count(n + 2, 0);
        vector<pair<int, int> > nums(n), ans(n);
        over = 0;

        for (int i = 0; i < n; i++) {
            cin >> nums[i].ff;
            count[nums[i].ff]++;
            nums[i].ss = i;
        }
        
        for (int i =  1; i < n + 2; i++) {
            over += max(count[i] - ((y - x) / 2), 0);
            if (count[i] == 0)
                uniq = i;
        }
        
        if (over - x - (n - y) > 0) {
            cout << "NO\n";
            continue;
        }
        
        sort(nums.begin(), nums.end());
        // cout << (y - x) / 2 << '\n';
        // for (int i = 1; i < n + 2; i++) {
        //     cout << count[i] << ' ';
        // }
        // cout << '\n';
        for (int i = 0; i < n; i++) {
            if (count[nums[i].ff] > ((y - x) / 2)) {
                if (tmpx > 0) {
                    tmpx--;
                    ans[i].ff = nums[i].ff;
                } else {
                    ans[i].ff = uniq;
                }

                count[nums[i].ff]--;
            } else {
                ans[i].ff = -1;
            }
            ans[i].ss = nums[i].ss;
        }

        // for (int i = 0; i < n; i++) {
        //     cout << ans[i].ff << ' ' << ans[i].ss << " - ";
        // }
        // cout << '\n';
        indx = n - 1;

        for (int i = 0; i < n; i++) {
            if (ans[i].ff != -1)
                continue;
            if (tmpx > 0) {
                ans[i].ff = nums[i].ff;
                tmpx--;
                continue;
            }
            if (dup == 0) {
                ans[i].ff = uniq;
                continue;
            }

            // cout << indx << '\n';
            if (nums[i].ff == nums[indx].ff)
                indx = 0;
            while (ans[indx].ff != -1) {
                if (indx >= i)
                    indx--;
                else
                    indx++;
            }
            // cout << i << ' ' << indx << '\n';
            
            ans[i].ff = nums[indx].ff;
            
            if (indx > i)
                indx--;
            else
                indx++;
            
            dup--;
        }

        for (int i = 0; i < n; i++) 
            swap(ans[i].ff, ans[i].ss);
        
        sort (ans.begin(), ans.end());

        cout << "YES\n";
        for (int i = 0; i < n; i++) 
            cout << ans[i].ss << ' ';
        cout << '\n';
    }
    return 0;
}