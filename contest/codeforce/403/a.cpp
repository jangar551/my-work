#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, answer = 0, tmp, now;
    cin >> n;
    now = 0;
    vector<bool> out(n + 1, false);
    for (int i = 0; i < 2*n; i++) {
        cin >> tmp;
        if (out[tmp]) now--;
        else {
            out[tmp] = true;
            now++;
        }
        answer = max(answer, now);
    }

    cout << answer << '\n';
    return 0; 
}