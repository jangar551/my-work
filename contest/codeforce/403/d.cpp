#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, c;
    cin >> n;
    string tmp;
    vector<pair<string, string> > names(n);
    for (int i = 0; i < n; i++) cin >> names[i].ff >> names[i].ss;
    vector<string> answer;
    vector<bool> convert(n);
    unordered_map <string, bool> dont, moredup;

    for (int i = 0; i < n - 1; i++) {
        for (int j = i + 1; j < n; j++) {
            for (c = 0; c < 3; c++) if (names[i].ff[c] != names[j].ff[c]) break;
            if (c == 3) {
                tmp = names[i].ff[0];
                tmp += names[i].ff[1];
                tmp += names[i].ff[2];
                dont[tmp] = true;
            } 
        }
    } 

    for (int i = 0; i < n; i++) {
        tmp = names[i].ff[0];
        tmp += names[i].ff[1];
        tmp += names[i].ff[2];
        if (dont.find(tmp) != dont.end() || moredup.find(tmp) != moredup.end()) {
            tmp = names[i].ff[0];
            tmp += names[i].ff[1];
            tmp += names[i].ss[0];
        }
        answer.pb(tmp);
        moredup[tmp] = true;
    }
   
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (answer[i] == answer[j]) {
                cout << "NO\n";
                return 0;
            }
        }
    }
    cout << "YES\n";
    for (int i = 0; i < n; i++) cout << answer[i] << '\n';

    return 0;
}