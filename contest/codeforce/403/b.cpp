#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n;
    cin >> n;
    vector<pair<double, double> > f(n);
    for (int i = 0; i < n; i++) cin >> f[i].ff;
    for (int i = 0; i < n; i++) cin >> f[i].ss;
    double time, meeting, answer;
    bool quit = false;
    sort (f.begin(), f.end());
    int l = 1, r = n - 2, nl = 0, nr = n - 1;
    while (true) {
        if (r - l <= 1) quit = true;
        // cout << l << ' ' << r << '\n';
        if (f[l].ff < f[nl].ff + f[nl].ss * ((f[nr].ff - f[nl].ff) / (f[nl].ss + f[nr].ss))) {
            if ((f[nl].ff + f[nl].ss * ((f[nr].ff - f[nl].ff) / (f[nl].ss + f[nr].ss)) - f[l].ff) / f[l].ss > (f[nl].ff + f[nl].ss * ((f[nr].ff - f[nl].ff) / (f[nl].ss + f[nr].ss)) - f[nl].ff) / f[nl].ss) nl = l;
            l++;
        }
        if (f[r].ff > f[nl].ff + f[nl].ss * ((f[nr].ff - f[nl].ff) / (f[nl].ss + f[nr].ss))) {
            if ((f[r].ff - f[nl].ff + f[nl].ss * ((f[nr].ff - f[nl].ff) / (f[nl].ss + f[nr].ss))) / f[r].ss > (f[nr].ff - f[nl].ff + f[nl].ss * ((f[nr].ff - f[nl].ff) / (f[nl].ss + f[nr].ss))) / f[nr].ss) nr = r;
            r--;
        }
        if (quit) break;
    }
    // cout << "=============\n";
    // for (int i = 0; i < n; i++) cout << f[i].ff << ' ';
    // cout << '\n';
    // for (int i = 0; i < n; i++) cout << f[i].ss << ' ';
    // cout << '\n';
    // cout << "=============\n";
    // cout << nl << ' ' << nr << ' ' << meeting << ' ' << '\n';
    answer = (f[nr].ff - f[nl].ff) / (f[nl].ss + f[nr].ss);
    cout << fixed << setprecision(7) << answer << '\n';
    return 0;
}