#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>

using namespace std;

int main(){
    int t, x;
    cin >> t;
    while (t--) {
        cin >> x;
        cout << x - 1 << ' ' << 1 << '\n'; 
    }
}