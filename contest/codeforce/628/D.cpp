#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>
#define ll long long

using namespace std;

int main(){
    ll u, v, tmp;
    cin >> u >> v;
    
    if (u == 0 && v == 0) cout << 0 << '\n';
    else if (u > v || v - u == -1) cout << -1 << '\n';
    else if (u == v) cout << 1 << '\n' << u << '\n';
    else {
        tmp = v - u;
        while (tmp % 2 != 1 && tmp != 0) tmp /= 2;
        if (tmp == 1) {
            tmp = u ^ ((v - u) / 2);
    
            if (tmp > u) {
                cout << 2 << '\n';
                cout << tmp << ' ' << (v - u) / 2 << '\n';
                return 0;
            }
        }
        if ((v - u) % 2 == 0) {
            cout << 3 << '\n';
            cout << u << ' ' << (v - u) / 2 << ' ' << (v - u) / 2 << '\n'; 
        } else {
            cout << -1 << '\n';
        }
    }
}