#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>
#define INF 1000000002

using namespace std;

int main(){
    int t, n, tmp, duplicate;
    cin >> t;
    while (t--) {
        duplicate = 0;
        cin >> n;

        vector<bool> used(INF);
        for (int i = 0; i < n; i++) {
            cin >> tmp;
            if (used[tmp] != NULL) duplicate++;
            else used[tmp] = true;
        }

        cout << n - duplicate << '\n';
    }
}