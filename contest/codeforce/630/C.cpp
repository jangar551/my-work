#include <bits/stdc++.h>
using namespace std;
int check(vector<int>&a, string&b) {
    vector<int> count(26, 0);
    unordered_map<int, bool> haha;
    int ez = 0;
    for(int i = 0; i < a.size(); i++) {
        if(haha.find(a[i]) != haha.end()) continue;
        haha[a[i]] = 1;
        ez++;
        count[b[a[i]] - 'a']++;
    }
    int maxi = 0;
    for(int i = 0; i < 26; i++) maxi = max(maxi, count[i]);
    return ez - maxi;
}
void go() {
    int n, m, ans = 0;
    string a;
    cin >> n >> m >> a;
    vector<int> col(n, -1);
    for(int i = 0; i < n; i++) {
        if (col[i] != -1) continue;
        vector<int> cur;
        int tmp = i;
        while(tmp < n) {
            cur.push_back(tmp);
            tmp += m;
        }
        tmp = cur.size();
        for(int j, tmp) 
            cur.pb(n - 1 - cur[j]);
        for(int j, cur.size()) 
            col[cur[j]] = 1;
        ans += check(cur, a);
    }
    cout << ans << '\n';
}
int main () {
    int T;
    cin >> T;
    while(T--) go();
    return 0;
}
// 