#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t;
    cin >> t;
    while (t--) {
        int l, r, d, u;
        int x, y, x1, x2, y1, y2;
        cin >> l >> r >> d >> u >> x >> y >> x1 >> y1 >> x2 >> y2;
        
        if ((x >= x1 && x <= x2) && (y >= y1 && y <= y2)) {
            int tmp;

            if (x1 == x2 && (l > 0 || r > 0)) {
                cout << "NO\n";
                continue;
            } else if (y1 == y2 && (u > 0 || d > 0)) {
                cout << "NO\n";
                continue;
            } else {
            
            tmp = x - l + r;
            if (tmp < x1 || tmp > x2) {
                cout << "NO\n";
                continue;
            }

            tmp = y - d + u;
            if (tmp < y1 || tmp > y2) {
                cout << "NO\n";
                continue;
            }

            cout << "YES\n";
            }
        } else {
            cout << "NO\n";
        }
    }
}