#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std; 

int main(){
    int t, n, color;
    vector<int> div{ 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31 };
    cin >> t;

    while(t--) {
        cin >> n;
        color = 1;
        vector<int> nums(n), answer(n, -1);
        for (int i = 0; i < n; i++) cin >> nums[i];
        
        vector<vector<int> > box(11);

        for (int i = 0; i < n; i++) 
        for (int j = 0; j < 11; j++) 
        if (nums[i] % div[j] == 0) {
            box[j].pb(i);
            break;
        }

        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < box[i].size(); j++) 
                answer[box[i][j]] = color;
            if (box[i].size() > 0) color++;
        }

        cout << color - 1 << '\n';
        for (int i = 0; i < n; i++) cout << answer[i] << ' ';
        cout << '\n';
    }

    return 0;
}