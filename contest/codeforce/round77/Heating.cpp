#include <iostream>
 
using namespace std;
 
int main () {
    int t, m, n, t1, t2, t3, answer;
    cin >> t;
    while(t--){
        cin >> n >> m;
 
        t1 = m / n;
        t2 = t1 * t1;
        t3 = m % n;
        answer = t2 * (n - t3);
        answer += (t1 + 1) * (t1 + 1) * t3;
 
        cout << answer << '\n';
    };
	return 0;
}