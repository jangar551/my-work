#include <iostream>
#define int long long
 
using namespace std;
 
signed main(){
    int k, a, b;
 
    cin >> k;
 
    for(int i = 0; i < k; i++){
        cin >> a >> b;
 
        if(a < b)
            swap(a, b);
        if(a > 2 * b){
            cout << "no\n";
            continue;
        }
        
        if((a + b) % 3 == 0)
            cout << "yes\n";
        else
            cout << "no\n";
    }
}
