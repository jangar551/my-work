#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n;
    string s;
    cin >> t;
    while (t--) {
        cin >> n >> s;
        string ans = "";
        for (int i = 0; i < n; i++) 
            if (s[i] == 'b')
                ans += 'b';
        
        for (int i = 0; i < n; i++) 
            if (s[i] != 'b')
                ans += s[i];
        cout << ans << '\n';
    }
    return 0;
}