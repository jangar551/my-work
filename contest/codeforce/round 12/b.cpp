#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, k;
    cin >> t;
    while (t--) {
        cin >> n >> k;
        vector<pair<int, int> > nums(n);
        for (int i = 0; i < n; i++)
            cin >> nums[i].ff >> nums[i].ss;
        for (int i = 0; i < n; i++) {
            int j;
            for (j = 0; j < n; j++) 
            if (abs(nums[i].ff - nums[j].ff) + abs(nums[i].ss - nums[j].ss) > k)
                break;
            
            if (j == n) {
                cout << 1 << '\n';
                break;
            }
            if (i == n - 1)
                cout << -1 << '\n';
        }
    }
    return 0;
}