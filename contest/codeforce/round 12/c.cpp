#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, ans;
    string s;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<string> ar;
        vector<vector<int> > nums(2, vector<int> (3));
        for (int i = 0; i < n; i++) {
            cin >> s;
            for (int j = 0; j < n; j++) {
                if (s[j] == 'O')
                    nums[0][(i + j) % 3]++;
                else if (s[j] == 'X')
                    nums[1][(i + j) % 3]++;
            }
            ar.pb(s);
        }
        ans = INT_MAX;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (i == j)
                    continue;
                ans = min(ans, nums[0][i] + nums[1][j]);
            }
        }

        /*
            5
            XXXOO
            XXOOO
            XOOOO
            OOO..
            OO..O

            5 5 5
            1 2 3
            
            6
        */

        bool quit = false;
        for (int x = 0; x < 3; x++) {
            if (quit)
                break;
            for (int o = 0; o < 3; o++) {
                if (x == o || quit)
                    continue;
                if (ans == nums[0][x] + nums[1][o]) {
                    for (int i = 0; i < n; i++) {
                        for (int j = 0; j < n; j++) {
                            if (ar[i][j] == 'O' && (i + j) % 3 == x)
                                ar[i][j] = 'X';
                            else if (ar[i][j] == 'X' && (i + j) % 3 == o)
                                ar[i][j] = 'O'; 
                        }
                    }
                    quit = true;
                }
            }
        }

        for (int i = 0; i < n; i++) 
            cout << ar[i] << '\n';
        
    }
    return 0;
}