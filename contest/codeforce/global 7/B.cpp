#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>
#define ll long long

using namespace std;

int main(){
    int n;
    cin >> n;
    vector<int> b(n), answer(n);

    for (int i = 0; i < n; i++) cin >> b[i];

    answer[0] = b[0];

    int mx = b[0];

    for (int i = 1; i < n; i++) {
        answer[i] = b[i] + mx;
        mx = max(mx, answer[i]);
    }

    for (int i = 0; i < n; i++) cout << answer[i] << ' ';
    cout << '\n';

    return 0;
}