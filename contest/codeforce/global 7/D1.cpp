#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>
#include <string>
#define ll long long

using namespace std;



bool ifpal (string s) {
    for (int i = 0; i < s.length() / 2; i++) 
        if (s[i] != s[s.length() - i - 1]) return false;
    return true;
}

void run() {
    int n;
    string s, tmp;

    cin >> s;
    n = s.length();

    if (ifpal(s)) {
        cout << s << '\n';
        return;
    }
    for (int i = 1; i < n; i++) {
        for (int j = 0; j <= n - i; j++) {
            tmp = s.substr(0, j);
            // cout << tmp << '\n';
            tmp += s.substr(j + i, n - j - i);

            // cout << tmp << '\n';

            // cout << "====================\n";

            if (ifpal(tmp)) {
                cout << tmp << '\n';
                return;
            }
        }
    }
}

int main(){
    int t;
    cin >> t;


    while (t--) 
        run();


    return 0;
}