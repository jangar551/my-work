#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>
#define int long long

using namespace std;

signed main(){
    int t;
    cin >> t;
    int n;
    string answer;

    while (t--) {
        cin >> n;
        answer = "";
        
        if (n == 1) { 
            cout << -1 << '\n';
            continue;
        }
        if (n == 2) {
            cout << 23 << '\n';
            continue;
        }
        if ((n - 1) % 3 == 0) {

            for (int i = 0; i < n - 2; i++) 
                answer += '2';
            answer += "33";
            cout << answer << '\n';
            continue;    
        }

        for (int i = 0; i < n - 1; i++) 
                answer += '2';
        answer += '3';
        
        cout << answer << '\n';
    }
}