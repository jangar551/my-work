#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std;

int main(){
    int n, firstdivisor = -1;

    cin >> n;

    for(int i = 2; i <= sqrt(n); i++){
        if(n % i == 0){
            firstdivisor = i;
            break;
        }
    }

    if(firstdivisor == -1)
        cout << n << '\n';
    else{
        while(n % firstdivisor == 0)
            n /= firstdivisor;
        
        if(n == 1)
            cout << firstdivisor << '\n';
        else
            cout << 1 << '\n';
        
    }
    return 0;
}