#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
 
using namespace std;
 
bool isitpossible(string& s, string& t, int& length){
    int differentchar = 0;
    vector<pair<char, char> > different;
    for(int i = 0; i < length; i++)
    if(s[i] != t[i]){
        differentchar++;
        different.push_back(make_pair(s[i], t[i]));
    }
    
    if(differentchar != 2)
        return 0;
    
    if(different[0].first == different[1].first && different[0].second == different[1].second)
        return 1;
    else
        return 0;
    
}
 
int main(){
    int k, length;
    string s, t;
 
    cin >> k;
 
    vector<bool> answer(k);
    
    for(int i = 0; i < k; i++){
        cin >> length >> s >> t;
        answer[i] = isitpossible(s, t, length);
    }
 
    for(int i = 0; i < k; i++)
        if(answer[i])
            cout << "YES\n";
        else
            cout << "NO\n";
 
    return 0;
}