#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;

int main(){
    srand(12);
    int t;
    cin >> t;
    for (int p = 1; p <= t; p++) {
        ofstream out("input" + to_string(p) + ".txt");
        int N = rand() % 999 + 1;
        out << N << '\n';
        int cnt = 0;
        while (N--) {
            int mod = (cnt == 0) ? 3 : 5;
            int op = rand() % mod; 
            if (op == 0)
                out << "push " << rand() % 999 + 1 << '\n', cnt++;
            if (op == 1)
                out << "empty\n";
            if (op == 2)
                out << "size\n";
            if (op == 3)
                out << "front\n";
            if (op == 4)
                out << "pop\n", cnt--;
        }
        
        out.close();
    }

    return 0;
}

/*
17
empty
size
push 1
push 2
push 3
empty
size
top
pop
top
pop
top
pop
push 4
size
pop
empty



YES
0
NO
3
1
3
2
1
YES



stack: {}

1. empty: true тул YES
2. size: 0
3. push 1 -> stack: {1}
4. push 2 -> stack: {1, 2}
5. push 3 -> stack: {1, 3, 2}
6. empty: false тул NO
7. size: 3
8. top: 1 // {**1**, 3, 2}
9. pop -> stack: {3, 2} 
10. top: 3
11. pop -> stack: {2}
12. top: 2
13. pop -> stack: {}
14. push 4 -> stack: {4}
15. size: 1
16. pop -> stack: {}
17. empty: true тул YES
*/