#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;

int main(){
    int t;
    cin >> t;
    for (int p = 1; p <= t; p++) {
        ifstream in("input" + to_string(p) + ".txt");
        ofstream out("output" + to_string(p) + ".txt");
        queue<int> a, b;
        int N;
        in >> N;
        while (N--) {
            string op;
            in >> op;
            if (op == "push") {
                int c;
                in >> c;
                if (c % 2 == 0)
                    a.push(c);
                else
                    b.push(c);
            }
            if (op == "empty")
                out << ((a.empty() && b.empty()) ? "YES\n" : "NO\n");
            if (op == "size")
                out << a.size() + b.size() << '\n';
            if (op == "front") {
                if (!a.empty())
                    out << a.front() << '\n';
                else 
                    out << b.front() << '\n';
            }
            if (op == "pop") {
                if (!a.empty())
                    a.pop();
                else 
                    b.pop();
            }
        }
        out.close();
        in.close();
    }

    return 0;
}