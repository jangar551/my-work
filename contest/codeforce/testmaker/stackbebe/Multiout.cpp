#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;

int main(){
    string* a = nullptr;
    int t;
    cin >> t;
    for (int p = 1; p <= t; p++) {
        ifstream in("input" + to_string(p) + ".txt");
        ofstream out("output" + to_string(p) + ".txt");
        vector<int> a;
        int N;
        string cmd;
        int e;
        stack<int> s;
        in >> N;
        while (N--) {
            in >> cmd;
            if (cmd == "push") {
                in >> e;
                if (s.empty()) s.push(e);
                else {
                    int temp = s.top();
                    s.pop();
                    s.push(e);
                    s.push(temp);
                }
            } else if (cmd == "pop") {
                s.pop();
            } else if (cmd == "top") {
                out << s.top() << endl;
            } else if (cmd == "empty") {
                if (s.empty()) out << "YES" << endl;
                else out << "NO" << endl;
            } else if (cmd == "size") {
                out << s.size() << endl;
            }
        }
        out.close();
        in.close();
    }

    return 0;
}