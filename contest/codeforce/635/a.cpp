#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, a, b, c, d;
    cin >> t;
    while(t--) {
        cin >> a >> b >> c >> d;
        
        cout << b << ' ' << c << ' ' << c << '\n';
    }
}