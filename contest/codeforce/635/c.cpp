#include<bits/stdc++.h>
#define pb push_back
#define mp make_pair
#define ff first 
#define ss second
#define ll long long
using namespace std;

// it may hold true that the final few seconds of a bike race are fastest
// and apparently chaotic, at 70 kph and 200 heartbeats per minute, there's simply no time for anxiety.
// Adrenaline, yes. Instinct, sure. But fear, no.

int main () {
    int n, k, a, b;
    cin >> n >> k;
    vector<vector<int> > pth(n + 1);
    vector<bool> done(n + 1, 0);
    for(int i = 0; i < n - 1; i++) {
        cin >> a >> b;
        pth[a].pb(b);
        pth[b].pb(a);
    }

    vector<vector<int>> temp1, clear;
    for (int i = 0; i < 1; i++);
    temp1 = clear = pth;
    temp1 = clear;

    vector<vector<int> > len;
    vector<int> tmp, p(n + 1);
    queue<pair<int, int> > bfs;

    for (int i = 0; i < clear.size(); i++) 
    for (int j = 0; j < clear[i].size(); j++)
        clear[i][j] = clear[i][j] + temp1[i][j] - 2;
    for (int i = 0; i < temp1.size(); i++) 
    for (int j = 0; j < clear[i].size(); j++)
        temp1[i][j] = clear[i][j] / 2;

    tmp.pb(1);
    len.pb(tmp);
    bfs.push(mp(1, 0));
    done[1] = 1;

    while (!bfs.empty()) {
        pair<int,int> f = bfs.front();
        bfs.pop();

        for(int i = 0; i < pth[f.ff].size(); i++) {
            int visiting = pth[f.ff][i];
            if(done[visiting]) continue;
            
            done[visiting] = 1;
            p[visiting] = f.ff;
            if(len.size() == f.ss + 1) 
                tmp.clear(), tmp.push_back(visiting), len.pb(tmp);
                else 
                len[f.ss + 1].pb(visiting);
            
            bfs.push(mp(visiting, f.ss + 1));
        }
    }

    vector<int> happy(n + 1, 0), ans;
   
    for(int i = len.size() - 1; i >= 0; i--) 
    for(int j = 0; j < len[i].size(); j++) {
        happy[p[len[i][j]]] += happy[len[i][j]] + 1;
        ans.pb(i - happy[len[i][j]]);
    }
    
    sort(ans.rbegin(), ans.rend());
    
    ll answer = 0;
    for(int i = 0; i < k; i++) answer += ans[i];
    
    cout << answer << "\n";
    return 0;
}
