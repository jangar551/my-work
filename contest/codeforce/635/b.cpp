#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, h, n, m;
    cin >> t;

    while(t--) {
        cin >> h >> n >> m;

        while (n > 0 && h >= 21) h = h / 2 + 10, n--;
        while (m--) h -= 10;
        if (h <= 0) cout << "YES\n";
        else cout << "NO\n"; 
    }
}