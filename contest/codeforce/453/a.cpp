#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, m, now;
    cin >> n >> m;
    vector<pair<int, int> > nums(n);
    for (int i = 0; i < n; i++) cin >> nums[i].ff >> nums[i].ss;
   
    now = 0;
    int i;
    for (i = 0; i < n; i++) {
        if (nums[i].ff <= now && nums[i].ss >= now) 
            now = nums[i].ss;
        
        if (now >= m) break;
    }
    if (i != n) cout << "YES\n";
    else cout << "NO\n";

    return 0;
}