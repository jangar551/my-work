#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int h, now = 1;
    cin >> h;
    vector <int> level(h + 1);
    for (int i = 0; i < h + 1; i++) cin >> level[i];
    int indx;
    for (indx = 1; indx < h + 1; indx++) {
        if (now > 1 && level[indx] > 1) break;
        now = level[indx];
    }
    if (indx == h + 1) cout << "perfect\n";
    else {
        cout << "ambiguous\n";
        int nodes = 1;
        now = 1;
        cout << 0 << ' ';
        for (int i = 1; i < h + 1; i++) {
            if (now > 1 && level[i] > 1) {
                for (int l = 0; l < level[i] - 1; l++) 
                    cout << nodes << ' ';
                cout << nodes - 1 << ' ';
            }else{
                for (int l = 0; l < level[i]; l++) 
                    cout << nodes << ' ';
            }
            nodes += level[i];
            now = level[i];
        }
        cout << '\n';
        cout << 0 << ' ';
        nodes = 1;
        for (int i = 1; i < h + 1; i++) {
            for (int l = 0; l < level[i]; l++) 
                cout << nodes << ' ';
            nodes += level[i];
            now = level[i];
        }
        cout << '\n';
    }
}