#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int paint(vector<vector<int> >& path, vector<int>& color, int now, int from) {
    int answer = 0;
    if (color[from] != color[now]) answer ++; 
    for (int i = 0; i < path[now].size(); i++) {
        if (path[now][i] == from) continue;
        answer += paint(path, color, path[now][i], now);
    }
    return answer;
}

int main(){
    int n, link;
    cin >> n;
    vector<int> color(n + 1);
    vector<vector<int> > path(n + 1);
    for (int i = 1; i < n; i++) {
        cin >> link;
        path[link].pb(i + 1);
        path[i + 1].pb(link);
    }
    for (int i = 0; i < n; i++) cin >> color[i + 1];
    color[0] = 0;

    cout << paint(path, color, 1, 0) << '\n';
}