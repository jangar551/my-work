    #include <bits/stdc++.h>
    #define ll long long
    #define pb push_back
    #define pp pop_back
    #define mp make_pair
    #define bb back
    #define ff first
    #define ss second

    using namespace std;

    int main(){
        int t, n, x, odd, even, num, tmp;
        cin >> t;
        while (t--) {
            cin >> n >> x;
            odd = even = 0;
            for (int i = 0; i < n; i++) {
                cin >> num;
                if (num % 2 == 0) even++;
                else odd++;
            }
            if (odd == 0) cout << "NO\n";
            else {
                if (odd - 1 + (odd % 2) > x) tmp = x - 1 + (x % 2);
                else tmp = odd - 1 + (odd % 2);
                if (tmp + even < x) cout << "NO\n";
                else cout << "YES\n";
            } 
        }

        return 0;
    }