#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, z1, o1, z2, o2, ans;
    string s;
    cin >> t;
    while (t--) {
        cin >> s; 
        z1 = o1 = z2 = o2 = 0;
        ans = INT_MAX;
        for (int i = 0; i < s.length(); i++) 
            if (s[i] == '0') o1++;
            else z1++;
        
        for (int i = 0; i < s.length(); i++) {
            ans = min(ans, o2 + z1);
            ans = min(ans, z2 + o1);
            if (s[i] == '0') {
                o1--;
                o2++;
            } else {
                z1--;
                z2++;
            }
        }

        cout << ans << '\n';
    }
}