#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define int long long

using namespace std;

int findways(vector<vector<int> >& routes, int now, int from) {
    if (routes[now].size() == 1) return 1;
    int ans = 0;
    for (int i = 0; i < routes[now].size(); i++) {
        if (routes[now][i] == from) continue;
        ans += findways(routes, routes[now][i], now);
    }
    return ans + 1;
}

signed main(){
    int t, n, x, s, e, f;
    cin >> t;
    while (t--) {
        cin >> n >> x;
        vector<vector<int> > routes(n + 1);
        for (int i = 0; i < n - 1; i++) {
            cin >> s >> e;
            routes[s].pb(e);
            routes[e].pb(s);
        }
        f = -1;
        if (routes[x].size() <= 1) cout << "Ayush\n";
        else if ((findways(routes, x, f) - 1 - 2) % 2 == 1) cout << "Ayush\n";
        else cout << "Ashish\n";
    }
}