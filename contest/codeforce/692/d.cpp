#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define int long long

using namespace std;

signed main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int x, y;
    string s;
    cin >> s >> x >> y;

    if (y < x) {
        swap(x, y);
        for (int i = 0; i < s.size(); i++) {
            if (s[i] == '0')
                s[i] = '1';
            else if (s[i] == '1')
                s[i] = '0';
        }
    }
    int suf1 = 0, suf0 = 0, pre1 = 0, pre0 = 0, ans, now = 0;
    for (int i = 0; i < s.size(); i++) {
        if (s[i] == '0') {
            now += suf1 * y;
            suf0++;
        }else{
            now += suf0 * x;
            suf1++;
        }
    }

    ans = now;
    // cout << now << '\n';
    for (int i = 0; i < s.size(); i++) {
        if (s[i] != '?') {
            if (s[i] == '0') {
                suf0--;
                pre0++;
            } else {
                suf1--;
                pre1++;
            }
            continue;
        }
        suf1--;
        now -= pre0 * x + suf0 * y;
        now += pre1 * y + suf1 * x;
        // cout << now << '\n';
        ans = min(ans, now);
        pre0++;
        /*
        
        */
    }

    cout << ans << '\n';
    
    return 0;
}


/*
x < y (01 cheaper than 10)
? ? 
1 0

? ? ? ? ? ? 
0 0 0 0 .. 1 1 1 1
0 count
1 count

0 count
1 count


01?01??0?

011011101
ans
010011101
ans
010110101
ans

1011
7 239
*/