#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, m, a, b;
    cin >> t;
    while (t--) {
        cin >> n >> m;
        vector<pair<int, int> > nums(m);
        unordered_map<int, vector<int> > loc; 
        int wrong = 0;
        for (int i = 0; i < m; i++) {
            cin >> nums[i].ff >> nums[i].ss;
            if (nums[i].ff != nums[i].ss) {
                loc[nums[i].ss].pb(i);
                wrong++;
                loc[nums[i].ff].pb(i);
            }
        }

        vector<int> used(m + 1, 0);
        int loop = 0;
        for (int i = 0; i < m; i++) {
            if (used[i] || nums[i].ss == nums[i].ff)
                continue;
            // cout << "ello " << i << '\n';
            if (loc[nums[i].ff].size() != 1) {
                int indx = nums[i].ff, me = i;
                while (loc[indx].size() != 1 && !used[me]) { //need end when same
                    // cout << me << ' ' << indx << '\n';
                    // cout << "GG ";
                    // for (int k = 0; k < loc[indx].size(); k++)
                    //     // cout << loc[indx][k] << ' ';
                    // cout << '\n';
                    used[me] = true;
                    if (loc[indx][0] == me) {
                        // cout << "f\n";
                        me = loc[indx][1];
                        if (nums[loc[indx][1]].ss == indx)
                            indx = nums[loc[indx][1]].ff;
                        else
                            indx = nums[loc[indx][1]].ss;
                    } else {
                        // cout << "s\n";
                        me = loc[indx][0];
                        if (nums[loc[indx][0]].ss == indx)
                            indx = nums[loc[indx][0]].ff;
                        else
                            indx = nums[loc[indx][0]].ss;
                    }
                    // cout << "-> " << indx << ' ' << me << '\n';
                }
                if (loc[indx].size() != 1 && me == i) {
                    // cout << "LOPOP " << me << ' ' << indx << '\n';
                    loop++; 
                }
            }     
        }
        // cout << wrong << ' ' << loop << '\n';
        cout << wrong + loop << '\n';
    }
    return 0;
}

/* 
* * * * # * *
* * * * * * *
* * * * * * *
* * # * * * *
* * * * * # *
* * * # * * *
* * * * * * #

*/