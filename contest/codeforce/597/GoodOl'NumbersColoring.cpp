#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
 
using namespace std;
 
int find_gcd(int a, int b){
    if(b == 0)
        return a;
    return find_gcd(b, a % b);
}
 
int main(){
    int n, a, b, gcd;
 
    cin >> n;
 
    vector<bool> isinfinite(n, 0);
 
    for(int i = 0; i < n; i++){
        cin >> a >> b;
        if(a >= b)
            gcd = find_gcd(a, b);
        else
            gcd = find_gcd(b, a);       
        
        if(gcd != 1)
            isinfinite[i] = 1;
            
    }
 
    for(int i = 0; i < n; i++){
        if(isinfinite[i])
            cout << "Infinite\n";
        else 
            cout << "Finite\n";
    }
 
    return 0;
}