#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <string>
 
using namespace std;
 
vector<int> bobcounter(string rps){
    vector<int> bobrps(3, 0);
    for(unsigned int i = 0; i < rps.length(); i++){
        if(rps[i] == 'R')
            bobrps[0]++;
        if(rps[i] == 'P')
            bobrps[1]++;
        if(rps[i] == 'S')
            bobrps[2]++;
    }
 
    return bobrps;
}
 
string solution(vector<int>& bobrps, string& rps, int& r, int& p, int& s){
    int a = 0, b = 0, c = 0;//overflow
    if(bobrps[0] < p) 
        b = p - bobrps[0];
 
    if(bobrps[1] < s)
        c = s - bobrps[1];
 
    if(bobrps[2] < r)
        a = r - bobrps[2];
 
 
    for(unsigned int i = 0; i < rps.length(); i++){
        if(rps[i] == 'R'){
            if(p > 0){
            rps[i] = 'P';
            p--;
            }else{
                if(a > 0){
                    rps[i] = 'R';
                    a--;
                }else{
                    rps[i] = 'S';
                    c--;
                }
            }
        }else{
            if(rps[i] == 'P'){
                if(s > 0){
                    rps[i] = 'S';
                    s--;
                }else{
                    if(a > 0){
                        rps[i] = 'R';
                        a--;
                    }else{
                        rps[i] = 'P';
                        b--;
                    }
                }
            }else{
                if(r > 0){
                    rps[i] = 'R';
                    r--;
                }else{
                    if(b > 0){
                        rps[i] = 'P';
                        b--;
                    }else{
                        rps[i] = 'S';
                        c--;
                    }
                }
            }
        }
    }
 
    return rps;
}
 
void ifwinning(vector<int>& bobrps, int& r, int& p, int& s, string& rps, vector<string>& answer, int i){
    int alicewin = 0;
 
    if(bobrps[0] > p)
        alicewin += p;
    else   
        alicewin += bobrps[0];
 
    if(bobrps[1] > s)
        alicewin += s;
    else   
        alicewin += bobrps[1];
 
    if(bobrps[2] > r)
        alicewin += r;
    else   
        alicewin += bobrps[2];
 
 
 
    if(2 * alicewin < r + p + s)
        answer[i] = "NO";
    else{
        answer[i] = solution(bobrps, rps, r, p, s);
    }
}
 
int main(){
    int t, n, r, p, s;
    string rps;
    vector<int> bobrps;
    cin >> t;
 
    vector<string> answer(t);
 
    for(int i = 0; i < t; i++){
        cin >> n >> r >> p >> s >> rps;
        bobrps = bobcounter(rps);
        ifwinning(bobrps, r, p, s, rps, answer, i);
    }
 
    for(int i = 0; i < t; i++){
        if(answer[i] == "NO")
            cout << "NO\n";
        else{
            cout << "YES\n" << answer[i] << '\n';
        }
    }
 
    return 0;
}