#include <bits/stdc++.h>
using namespace std;

int gcd(int a, int b) { 
    if (a == 0 || b == 0) 
       return b; 

    if (a == b) 
        return a; 
 
    if (a > b) 
        return gcd(a-b, b); 
    return gcd(a, b-a); 
} 

int main () {
    int t;
    cin >> t;
    while(t--) {
        int num1, num2;
        cin >> num1 >> num2;
        
        if (num1 < num2) swap(num1, num2);

        if (num1 % num2 != 0) cout << "NO\n";
        else cout << "YES\n";
        
    };
    return 0;
}