#include <bits/stdc++.h>
using namespace std;

int main () {
    int t, n;
    cin >> t;

    while (t--) {
        cin >> n;
        vector<int> array(n);

        for(int i = 0; i < n; i++) cin >> array[i];
        sort(array.rbegin(), array.rend());

        for(int i = 0; i < n; i++) cout << array[i] << " ";
        cout << "\n";
    }
    return 0;
}