#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, tmp, rank = 1, thomas = 0, student;
    cin >> n;
    for (int i = 0; i < 4; i++) cin >> tmp, thomas += tmp;

    for (int i = 0; i < n - 1; i++) {
        student = 0;
        for (int j = 0; j < 4; j++) cin >> tmp, student += tmp;

        if (student > thomas) rank++;
    }

    cout << rank << '\n';

    return 0;
}