#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, m, answer;
    string s;
    cin >> t;
    while (t--) {
        cin >> n >> m;
        answer = 0;
        for (int i = 0; i < n; i++) {
            cin >> s;
            if (i == n - 1) {
                for (int j = 0; j < m - 1; j++)
                    if (s[j] != 'R')
                        answer++;
            } else if (s[m - 1] != 'D')
                answer++;
        }

        cout << answer << '\n';
    }
    return 0;
}