#include <bits/stdc++.h>
#define int long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define MOD 1000000007

using namespace std;

signed main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int n, ans = 1, minus = 1;
    cin >> n;
    for (int i = 0; i < n; i++) {
        if (i != n - 1) {
            minus *= 2;
            minus %= MOD;
        }
        ans *= i + 1;
        ans %= MOD;
    }
    ans = (ans - minus + MOD) % MOD;
    cout << ans << '\n';

    return 0;
}