#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    // ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int n, m, a1 = 0, a2 = 0;
    cin >> n >> m;
    vector<string> tmp(n), grid;
    for (int i = 0; i < n; i++) 
        cin >> tmp[i];
    if (n > 3 && m > 3) {
        cout << -1 << '\n';
        return 0;
    } 
    if (n == 1 || m == 1) {
        cout << 0 << '\n';
        return 0;
    }

    if (m <= 3) {
        grid.resize(m, "");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                grid[j] = grid[j] + tmp[i][j];
            }
        }
        swap(n, m);
    } else grid = tmp;
    for (int i = 0; i < n; i++) {
        int ev = 0, od = 0, ans;
        for (int j = 0; j < m; j++) {
            if (grid[i][j] == '1') {
                if (j % 2 == 0) 
                    ev++;
                else
                    od++;
            }
        }   
        ans = min(n / 2 + n % 2 - ev + od, n / 2 - od + ev);
        if (i % 2 == 0) {
            a1 += ans; 
            a2 += ev + od;           
        } else {
            a2 += ans;
            a1 += ev + od;
        }
    }

    cout << min(a1, a2) << '\n';
    return 0;
}