#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define int long long

using namespace std;

signed main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n;
    cin >> t;
    while (t--) {
        cin >> n;
        if (n % 2 != 0) {
            cout << -1 << '\n';
            continue;
        }

        vector<int> ans;
        int bit = 2, cnt = 0;
        
        while (bit <= n) 
            bit = (bit + 1) * 2, cnt++;
        bit = bit / 2 - 1;
        cnt--;

        while (n > 0) {
            if (n >= bit) {
                ans.pb(1);
                for (int i = 0; i < cnt; i++)
                    ans.pb(0);
                n -= bit;
            }
            if (bit > n) {
                cnt--;
                bit = bit / 2 - 1;
            }
        }

        cout << ans.size() << '\n';
        for (int i = 0; i < ans.size(); i++)
            cout << ans[i] << ' ';
        cout << '\n';
    }
    return 0;
}