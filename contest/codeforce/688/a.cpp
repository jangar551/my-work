#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, m, indx, answer;
    cin >> t;
    while (t--) {
        cin >> n >> m;
        answer = 0;
        vector<bool> nums(101, 0);
        for (int i = 0; i < n; i++) {
            cin >> indx;
            nums[indx] = true;
        }

        for (int i = 0; i < m; i++) {
            cin >> indx;
            if (nums[indx])
                answer++;
        }
        cout << answer << '\n';
    }
    return 0;
}