#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define int long long

using namespace std;

signed main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, ans, red;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<int> nums(n);
        for (int i = 0; i < n; i++)
            cin >> nums[i];
        red = 0;
        for (int i = 0; i < n; i++) {
            bool dif = true;
            if (i != 0 && nums[i - 1] == nums[i])
                dif = false;
            if (i != n - 1 && nums[i + 1] == nums[i])
                dif = false;

            if (dif) {
                int now;
                if (i == 0)
                    now = abs(nums[i + 1] - nums[i]);
                else if (i == n - 1)
                    now = abs(nums[i - 1] - nums[i]);
                else 
                    now = (abs(nums[i + 1] - nums[i]) + abs(nums[i] - nums[i - 1])) - abs(nums[i + 1] - nums[i - 1]);
                
                red = max(red, now);
            }
        }
        ans = 0;
        for (int i = n - 1; i > 0; i--)
            ans += abs(nums[i] - nums[i - 1]);
        ans -= red;
        cout << ans << '\n';
    }
    return 0;
}