#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, m, num, nghbr;
    bool pos;
    cin >> t;
    while (t--) {
        cin >> n >> m;
        pos = true;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                cin >> num;

                if ((i == 0 || i == n - 1) && (j == 0 || j == m - 1)) 
                    nghbr = 2;                        
                else if ((i == 0 || i == n - 1) || (j == 0 || j == m - 1)) 
                    nghbr = 3;
                else 
                    nghbr = 4;
                
                if (num > nghbr)
                    pos = false;
            }
        }

        if (!pos) {
            cout << "NO\n";
        } else {
            cout << "YES\n";
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    if ((i == 0 || i == n - 1) && (j == 0 || j == m - 1)) 
                        nghbr = 2;                        
                    else if ((i == 0 || i == n - 1) || (j == 0 || j == m - 1)) 
                        nghbr = 3;
                    else 
                        nghbr = 4;
                    
                    cout << nghbr << ' ';
                }
                cout << '\n';
            }
        }
    }   
    return 0;
}