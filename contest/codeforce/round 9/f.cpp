#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    vector<ll> nums(3);
    ll indx, out;
    cin >> nums[0] >> nums[1] >> nums[2];
    out = max(nums[0], max(nums[1], nums[2]));
    cout << "First\n";
    cout.flush();
    cout << out << '\n';
    cout.flush();
    cin >> indx;
    if (indx == 0 || indx == -1) 
        return 0;
    nums[indx - 1] += out;
    out = 0;
    for (int i = 0; i < 3; i++) {
        if (nums[i] != max(nums[0], max(nums[1], nums[2])) && nums[i] != min(nums[0], max(nums[1], nums[2])))
            out += max(nums[0], max(nums[1], nums[2])) - nums[i];
    }
    cout << out << '\n';
    cout.flush();
    cin >> indx;
    if (indx == 0 || indx == -1) 
        return 0;
    nums[indx - 1] += out;
    out = 0;
    sort(nums.begin(), nums.end());
    for (int i = 0; i < 3; i++) {
        if (nums[i] != max(nums[0], max(nums[1], nums[2]))) {
            out += max(nums[0], max(nums[1], nums[2])) - nums[i];
            break;
        }
    }
    cout << out << '\n';
    cout.flush();
    cin >> indx;
    return 0;
}