#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<int> nums(n), count(n + 1);

        for (int i = 0; i < n; i++) {
            cin >> nums[i];
            count[nums[i]]++;
        }
        cout << 2 * n << '\n';
        for (int tilldone = 0; tilldone < 2 * n; tilldone++) {
            for (int c = 0; c <= n; c++) {
                if (count[c] == 0) {
                    if (c == n) {
                        for (int i = 0; i < n; i++) {
                            if (nums[i] != i || i == n - 1) {
                                count[nums[i]]--;
                                count[c]++;
                                nums[i] = c;
                                cout << i + 1 << ' ';
                                break;
                            }
                        }
                    } else {
                        count[nums[c]]--;
                        count[c]++;
                        nums[c] = c;
                        cout << c + 1 << ' ';
                    }
                    break;
                }
            }
        }
        cout << '\n';
    }

    return 0;
}