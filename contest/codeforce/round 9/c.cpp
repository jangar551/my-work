#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, mx;
    bool pos;
    cin >> t;
    while (t--) {
        cin >> n;
        vector<int> nums(n);

        for (int i = 0; i < n; i++)
            cin >> nums[i];

        pos = false;
        mx = 0;
        for (int i = 1; i < n; i++) {
            if (nums[i] <= nums[0]) {
                mx = max(mx, nums[i]);
                pos = false;
            }

            if (nums[i] > mx) 
                pos = true;
        }

        if (pos)
            cout << "YES\n";
        else
            cout << "NO\n";
    }
    return 0;
}