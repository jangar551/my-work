#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, tmp, a, b;
    bool positive;
    cin >> t;
    while (t--) {
        cin >> n;
        positive = true;
        for (int i = 0; i < n; i++) {
            cin >> tmp;
            a = 1;
            b = -1;
            if (positive) {
                a = -1;
                b = 1;
                positive = false;
            } else 
                positive = true;
            
            if (tmp > 0) 
                cout << a * tmp << ' ';
            else 
                cout << b * tmp << ' ';
        }
        cout << '\n';
    }
    return 0;
}