#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int first_bit(int n) {
    if (n < 2) return 1;
    return 1 + first_bit(n / 2);
}

int main(){
    int t, n, mx, answer;
    cin >> t;
    while (t--) {
        cin >> n;
        mx = INT_MIN;
        answer = 0;
        vector<int> nums(n);
        for (int i = 0; i < n; i++) {
            cin >> nums[i];
            if (mx > nums[i]) answer = max(answer, first_bit(mx - nums[i]));
            mx = max(mx, nums[i]);
        }

        cout << answer << '\n';
    }
}