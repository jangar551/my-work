#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

void go(){
    int n;
    cin >> n;
    vector<int> nums(n);
    for(int i = 0; i < n; i++) cin >> nums[i];
    sort(nums.begin(), nums.end());

    vector<int> answer;
    for(int i = 0; i < n; i++) 
    if(i % 2 == 0) answer.pb(nums[i / 2]);
    else answer.pb(nums[n - 1 - i / 2]);
    
    reverse(answer.begin(), answer.end());
    for(int i = 0; i < n; i++) 
        cout << answer[i] << " ";
    cout << "\n";
}

int main () {
    int t, n;
    cin >> t;
    while(t--) go();
    
    
    return 0;
}