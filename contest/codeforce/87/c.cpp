#include <bits/stdc++.h>
using namespace std;
#define int long double
const double PI = acos(-1.0);

signed main () {
    int T, n;
    cin >> T;
    while(T--) {
        cin >> n;
        n = 2 * n;
        n = 1 / (2*sin((180/n) * PI / 180));
        n *= 2;
        cout << fixed << setprecision(9) << n << '\n'; 
    }
    return 0;
}