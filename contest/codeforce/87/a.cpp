#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;

int main(){
    long long    t, a, b, c, d;
    cin >> t;
    while (t--) {
        cin >> a >> b >> c >> d;
        if (a <= b) {
            cout << b << '\n';
        } else {
            if (d >= c) cout << -1 << '\n';
            else {
                if ((a - b) % (c - d) != 0)
                    cout << b + c + (a - b) / (c - d) * c << '\n';
                else
                    cout << b + (a - b) / (c - d) * c << '\n';
            } 
        }
    }

    return 0;
}