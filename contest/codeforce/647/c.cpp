#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll t, n, answer, now;
    cin >> t;
    while (t--) {
        cin >> n;
        n++;
        answer = 0;
        now = 1;
        while (now < n + 1) {
            answer += n / now - 1;
            if (n % now != 0) answer++;
            now *= 2;
        }
        cout << answer << '\n';
    }
    return 0;
}