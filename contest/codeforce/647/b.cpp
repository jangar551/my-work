#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, mx, xr, now, tmp;
    cin >> t;
    while (t--) {
        cin >> n;
        mx = 1025;
        vector<int> nums(n), have(2500, 0), used(2500, 0), clear(2500, 0);
        for (int i = 0; i < n; i++) {
            cin >> nums[i];
            mx = max(mx, nums[i]);
            have[nums[i]]++;
        }
        xr = 1;
        while (xr <= mx * 2) {
            int i;
            for (i = 0; i < n; i++) {
                tmp = nums[i]^xr;
                if (have[tmp] <= used[tmp]) break;
                used[tmp]++;
            }
            if (i == n) {
                cout << xr << '\n';
                break;
            }
            xr++;
            used = clear;
        }
        if (xr > mx) cout << -1 << '\n'; 
    }
    return 0;
}