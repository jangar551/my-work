#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll t, a, b, cnt, answer;
    cin >> t;
    while (t--) {
        cin >> a >> b;
        cnt = 1;
        answer = 0;
        if (a > b) swap(a, b);
        while (a < b) {
            a = a * 2;
            cnt*=2;
        }
        if (a != b) cout << -1 << '\n';
        else {
            while (cnt >= 8) {
                answer++;
                cnt /= 8;
            }
            while (cnt >= 4) {
                answer++;
                cnt /= 4;
            }
            while (cnt >= 2) {
                answer++;
                cnt /= 2;
            }
            cout << answer << '\n';
        }  
    }
    return 0;
}