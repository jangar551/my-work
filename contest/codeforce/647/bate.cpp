void go () {
    int n, ans = INF;
    cin >> n;
    VI nums = readVI(n);
    sort(ALL(nums));
    if (n == 1) {
        cout << nums[0] << '\n';
        return;
    }
    REP1(i, n) {
        VI used(2000, -1);
        REP(j, n) used[nums[j]] = 0;
        int dif = nums[i]^nums[0];
        bool yes = 1;
        REP(j, n) {
            if (used[nums[j]]) continue;
            int cur = nums[j];
            int next = nums[j]^dif;
            if (next > 1024 || used[cur] == -1 || used[next] == -1) {
                yes = 0;
                break;
            }
            if (used[cur] == 1) {
                yes = 0;
                break;
            }
            if (used[next] == 1) {
                yes = 0;
                break;
            }
            used[cur] = used[next] = 1;
        }
        if (yes) ans = min(ans, dif);
    }
    if (ans == INF) ans = -1;
    cout << ans << '\n'