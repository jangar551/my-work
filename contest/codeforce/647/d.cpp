#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, m, a, b;
    cin >> n >> m;
    vector<vector<int> > routes(n + 1);
    vector<int> assign(n + 1, 1);
    vector<pair<int, int> > nums(n);
    for (int i = 0; i < m; i++) {   
        cin >> a >> b;
        routes[a].pb(b);
        routes[b].pb(a);
    }
    for (int i = 0; i < n; i++) {
        cin >> nums[i].ff;
        nums[i].ss = i + 1;
    }
    sort(nums.begin(), nums.end());
    int i;
    vector<bool> had(n + 1, false);
    for (i = 0; i < n; i++) {
        if (assign[nums[i].ss] != nums[i].ff) break;
        int j;
        for (j = 0; j < routes[nums[i].ss].size(); j++) {
            if (had[routes[nums[i].ss][j]]) continue;
            if (assign[routes[nums[i].ss][j]] < nums[i].ff) break;
            assign[routes[nums[i].ss][j]] = nums[i].ff + 1;
        }
        had[nums[i].ss] = true;
        if (j != routes[nums[i].ss].size()) break;
    }
    if (i != n) {
        cout << -1 << '\n';
    } else {
        for (int indx = 0; indx < n; indx++) {
            cout << nums[indx].ss << ' ';
        }
        cout << '\n';
    }
    
}