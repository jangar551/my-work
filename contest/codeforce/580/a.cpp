#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, m, nmax = -10000, mmax = -10000, tmp;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> tmp;
        nmax = max(nmax, tmp);
    }
    cin >> m;
    for (int i = 0; i < m; i++) {
        cin >> tmp;
        mmax = max(mmax, tmp);
    }
    cout << nmax << ' ' << mmax << '\n';
}