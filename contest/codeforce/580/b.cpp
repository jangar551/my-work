#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll n, tmp, cnt = 0, answer = 0;
    cin >> n;
    bool zero = false;
    for (int i = 0; i < n; i++) {
        cin >> tmp;
        if (tmp > 0) answer+= tmp - 1;
        else if(tmp == 0) {
            if(zero) answer += 1;
            else zero = true;
        } else {
            answer += -1 - tmp;
            cnt++;
        }
    }
    if (zero) answer += 1;
    else if (cnt % 2 == 1) answer += 2;

    cout << answer << '\n';
    return 0;
}