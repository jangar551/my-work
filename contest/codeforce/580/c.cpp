#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, now = 1;
    cin >> n;
    bool high = false;
    vector<int> answer (2 * n);
    for (int i = 0; i < n; i++) {
        if (high) answer[i] = now + 1;
        else answer[i] = now;
        now += 2;
        high = !high;
    }
    now = 2;
    high = false;
    for (int i = n; i < 2 * n; i++) {
        if (high) answer[i] = now - 1;
        else answer[i] = now;
        now += 2;
        high = !high;
    }
    if (!high) cout << "NO\n";
    else {
        cout << "YES\n";
        for (int i = 0; i < 2 * n; i++) cout << answer[i] << ' ';
        cout << '\n';
    }
}