#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, k;
    cin >> t;
    while (t--) {
        cin >> n >> k;
        if (k > n) {
            cout << k - n << '\n';
        } else {
            if (n % 2 != k % 2)
                cout << 1 << '\n';
            else 
                cout << 0 << '\n';
        }
    }
    return 0;
}