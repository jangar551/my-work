#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n;
    long long pile1, pile2;
    cin >> t;
    while (t--) {
        cin >> n;
        pile1 = pile2 = 0;

        for (int i = 1; i <= n / 2 - 1; i++) pile1 += pow(2, i);
        for (int i = n / 2; i < n; i++) pile2 += pow(2, i);
        pile1 += pow(2, n);
        cout << abs(pile1 - pile2) << '\n';
    }

    return 0;
}