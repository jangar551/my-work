#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, k, count;
    cin >> t;
    while (t--) {
        cin >> n >> k;
        count = 0;
        vector<int> nums(n);
        vector<bool> have(n + 1);
        for (int i = 0; i < n; i++) {
            cin >> nums[i];
            have[nums[i]] = true;
        }

        for (int i = 1; i <= n; i++) if (have[i]) count++;

        if (count > k) cout << -1 << '\n';
        else {
            vector<int> answer, loop;
            for (int i = 1; i <= n; i++) if (have[i]) loop.pb(i);
            for (int i = 0; i < k - count; i++) loop.pb(1);

            int indx = 0;

            for (int i = 0; i < n; i++) {
                while (nums[i] != loop[indx]) {
                    answer.pb(loop[indx]);
                    indx = (indx + 1) % k;
                }
                answer.pb(nums[i]);
                indx = (indx + 1) % k;
            }

            cout << answer.size() << '\n';
            for (int i = 0; i < answer.size(); i++) cout << answer[i] << ' ';
            cout << '\n';
        } 
    }   

    return 0;
}