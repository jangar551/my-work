#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, k, len;
    string s, answer;
    char tmp;
    cin >> t;
    while(t--) {
        cin >> n >> k >> s;
        sort(s.begin(), s.end());
        if (k == 1) {
            cout << s << '\n';
        }else if (s[0] != s[k - 1]) {
            cout << s[k - 1] << '\n';
        }else if (s[k] != s.back()) {
            cout << s[0] + s.substr(k) << '\n';
        } else {
            answer = "";
            for (int i = 0; i < n; i += k) answer += s[i];
            cout << answer << '\n'; 
        }
    }
}