#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll t, n, tmp, cnt, plus;
    cin >> t;
    while (t--) {
        cin >> n;
        tmp = 1;
        plus = 2;
        cnt = 0;

        while (n > tmp + plus) {
            tmp += plus;
            cnt++; 
            plus *= 2;
        }
        if (tmp == n) {
            cout << cnt << '\n';
            tmp = 1;
            for (int i = 0; i < cnt; i++) {
                cout << tmp << ' ';
                tmp *= 2;
            }    
        } else {
            tmp -= plus;
            plus /= 2;
            cout << cnt << '\n';
            for (int i = 0; i < cnt - 2; i++) {
                cout << tmp << ' ';
                tmp *= 2;
            }
        }
    }
}