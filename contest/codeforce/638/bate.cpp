int n, m;
    cin >> n >> m;
    string s;
    cin >> s;
    sort(ALL(s));
    if (m == 1) {
        cout << s << '\n';
        return;
    }
    vector <string> ans(m);
    REP(i, m) {
        ans[i % m] += s[i];
    }
    if (ans[0] != ans.back()) {
        cout << s[m - 1] << '\n';
        return;
    }
    if (s[m] != s.back()) {
        cout << ans[0] + s.substr(m) << '\n';
        return;
    }
    FOR(i, m, n) ans[i % m] += s[i];
    cout << ans[0] << '\n';
    return;