#include <iostream>
#include <vector>
#include <unordered_map>
#include <map>
using namespace std;
#define mp make_pair
void ans(){
    int n;
    cin >> n;
    unordered_map<int, int> pos;
    vector <int> nums(n);
    for(int i = 0; i < n; i++) {
        cin >> nums[i];
        pos[nums[i]] = i;
    }
    int l = pos[1], r = pos[1];
    vector <pair <int, int> > answers(n + 1);
    for (int i = 2; i <= n; i++) {
        answers[i - 1] = mp(l, r);
        // in
        int len = r - l + 1;
        if (pos[i] <= r && pos[i] >= l) continue;
        int cur = pos[i];
        if (cur < l) l = cur;
        else r = cur;
        // for(int )
    }
    answers[n] = mp(l, r);
    
    for (int i = 1; i <= n; i++) {
        int a = answers[i].first, b = answers[i].second;
        // cout << i << ' ' << a << ' ' << b << '\n';
        if (b - a + 1 <= i) cout << 1;
        else cout << 0;
    } 
    cout << '\n';
}

int main(){
    int t;
    cin >> t;
    while (t--)
        ans();
    return 0;

}