#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void work() {
    string input;
    cin >> input;
    int n = input.size();
    char abc[] = {'a', 'b', 'c'};
    string word = "abc?";
    for (int i = 0; i < n - 1; i++) 
    for (int j = 0; j < 3; j++) 
        if (input[i] == abc[j] && input[i + 1] == abc[j]) {
            cout << -1 << '\n';
            return;
        }
        
    
    string answer = "";
    for (int i = 0; i < n; i++) {
        if (input[i] != '?') {
            answer += input[i];
            continue;
        }
        vector <bool> position(4, true);
        if (i > 0) position[word.find(input[i - 1])] = false;
        if (i > 0) position[word.find(answer.back())] = false;
        if (i + 1 < input.size()) position[word.find(input[i + 1])] = false;
        for (int j = 0; j < 4; j++) {
            if (position[j]) {
                answer += abc[j];
                break;
            }
        }
    }   
    cout << answer << '\n';
}
int main () {
    int t;
    cin >> t;
    while(t--) work();
    return 0;
}
 
 