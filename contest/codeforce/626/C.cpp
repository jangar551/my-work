#include <bits/stdc++.h>
using namespace std;

int adder(string a) {
    int count = 0;

    for (int i = 0; i < a.size(); i++) {
        if (a[i] == '(') count++;
        else count--;
        if (count < 0) return a.size();
    }

    return 0;
}
int main () {
    int n;
    string a;
    cin >> n >> a;

    int c1 = 0, answer = 0, lengt = 0;
    for (int i = 0; i < n; i++) {
        lengt++;

        if (a[i] == '(') c1++;
        else c1--;
        if (c1 == 0) {
            answer += adder(a.substr(i - lengt + 1, lengt));
            lengt = 0;
        }
    }

    if (lengt & 1 || c1 != 0) 
        cout << -1 << '\n';
    else 
        cout << answer << '\n';
    return 0;
}