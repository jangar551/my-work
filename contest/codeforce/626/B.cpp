#include <bits/stdc++.h>
using namespace std;

long long n, m, k;

unordered_map <int, long long> line_count, clum_count;
vector<long, long> divisor;

long long FUNC(int a, int b) {
    long long answer = 0;

    for(int i = 0; i < divisor.size(); i++) {
        if (divisor[i] <= a) {
            int tmp = k / divisor[i];
            if (b < tmp) continue;
            answer += (a - divisor[i] + 1) * (b - tmp + 1);
        } else break;
    }
    return answer;
}
signed main () {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    cin >> n >> m >> k;

    vector<int> tmp1(n + 1, 0), tmp2(m + 1, 0);
    set <int> l, c;
    for (int i = 1; i < sqrt(k); i++) {
        if (k % i == 0) {
            divisor.push_back(i);
            if (k / i != i)
                divisor.push_back(k / i);
        }
    }

    sort(divisor.begin(), divisor.end());

    for (int i = 0; i < n; i++) cin >> tmp1[i];
    for (int i = 0; i < m; i++) cin >> tmp2[i];

    if (true) {
        int count = 0;
        for(int i = 0; i < n + 1; i++) 
        if (tmp1[i] == 0) {
            if (count != 0) {
                l.insert(count);
                line_count[count]++;
            }
            count = 0;
        } else 
            count++;
            
        
        for (int i = 0; i < m + 1; i++) 
        if (tmp2[i] == 0) {
            if (count != 0) {
                c.insert(count);
                clum_count[count]++;
            }
            count = 0;
        } else
            count++;  
    }
    
    long long answer = 0;
    for (auto num : l) 
    for (auto num1 : c) 
    if (num * num1 >= k) 
        answer += FUNC(num, num1) * line_count[num] * clum_count[num1];
        
    cout << answer << '\n';
    return 0;
}