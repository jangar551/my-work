#include <iostream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <map>
#include <set>

using namespace std;

int main(){
    int t, n;
    cin >> t;

    while(t--) {
        cin >> n;
        vector<int> num(n);

        int even = -1;

        for (int i = 0; i < n; i++) {
            cin >> num[i];
            if(num[i] % 2 == 0) even = i;

        }

        if (even != -1) {
            cout << 1 << '\n';
            cout << even + 1 << '\n';
            continue;
        }

        int count = 0;
        vector<int> odd (2, 0);
        int i;

        for (i = 0; i < n; i++) {
            if (num[i] % 2 == 1) odd[count] = i, count++;
            if (count == 2) {
                cout << 2 << '\n';
                cout << odd[0] + 1 << ' ' << odd[1] + 1 << '\n';
                break;
            }
        }
        if (i == n) cout << -1 << '\n';
    }
}   