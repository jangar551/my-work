#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;

vector<int> answer(10, 0);

void counter (string n) {
    if (n.length() == 1) {
        for (int i = 1; i < n[0] - '0'; i++) answer[i] += 1;
        return;
    }

    ll tmp, last = 1;

    for (int i = n.length() - 1; i > 0; i--) {
        tmp = n[0] - '0';
    
        for (int j = 1; j < i; j++) {
            tmp *= 10;
        }
        
        for (int j = 1; j < 10; j++) answer[j] += tmp * last;
        answer[0] += tmp;
        last *= tmp;
        cout << tmp << ' ' << last << '\n';

        if (i == 1) {
            tmp = 0;
            for (int j = 1; j < n.length(); j++) tmp = tmp * 10 + (n[j] - '0');
            for (int j = 1; j <= n[0] - '0'; j++) answer[j] += tmp + 1;
        }

        if (i == 0) counter(n.substr(1, n.length() - 1));
    }

}

int main(){
    string n;
    cin >> n;
    counter(n);
    for (int i = 0; i < 10; i++) cout << answer[i] << ' ';
    cout << '\n';
}