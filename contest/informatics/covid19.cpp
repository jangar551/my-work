#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define int ll

using namespace std;

signed main(){
    int n, q, answer = 0, free = 0;
    cin >> n >> q;
    vector<int> virus(q);

    for (int i = 0; i < q; i++) cin >> virus[i];


    int l = 0, r = n - 1, indxl = 0, indxr = q - 1;
    sort(virus.begin(), virus.end());

    vector<pair<int, int> > importance, tmp;
    vector<bool> out(q, false);

    importance.push_back(make_pair(virus[0] + (virus[1] - virus[0]) / 2, 0));
    for (int i = 1; i < q - 1; i++) {
        importance.push_back(make_pair((virus[i] - virus[i - 1] + virus[i + 1] - virus[i]) / 2, i));
    }
    importance.push_back(make_pair(n - virus[q - 1] + (virus[q - 1] - virus[q - 2]) / 2, q - 1));

    sort(importance.begin(), importance.end());

    for (int i = 0; i < q; i++) cout << importance[i].first << ' ' << importance[i].second << '\n'; 
    
    tmp = importance;

    while (importance.size() != 0) {
        answer += n - free - 1;
        int indx = importance.size() - 1;
        int outindx = importance.back().second;
        
        if (importance[indx].second == 0) {
            free += virus[0];
            cout << "a  "   << importance[indx].first << ' ' << free << " --> new free "; 
            if (out[1]) free += virus[1] - virus[0] - 1;  
            else {
                int i;
                for (i = 0; i < importance.size(); i++) 
                if (importance[i].second == 1) {
                    importance[i].first += (virus[1] - virus[0]) / 2;
                    break;
                } 
            }

            cout << free << '\n';

        } else if(importance[indx].second == q - 1) {
            free += n - virus[q - 1] + 1;
            cout << "b  "  << importance[indx].first << ' ' << free << " --> new free "; 
            if (out[q - 2]) free += virus[q - 1] - virus[q - 2] - 1;
            else {
                int i;
                for (i = 0; i < importance.size(); i++) 
                if (importance[i].second == q - 2) {
                    importance[i].first += (virus[1] - virus[0]) / 2;
                    break;
                } 
            }
            cout << free << '\n';

        } else {
            cout << "c  "  << importance[indx].first << ' '  << free << " --> new free "; 

            if (out[outindx + 1]) free += virus[outindx + 1] - virus[outindx] - 1;
            else {
                int i;
                for (i = 0; i < importance.size(); i++) 
                if (importance[i].second == outindx + 1) {
                    importance[i].first += (virus[outindx + 1] - virus[outindx]) / 2;
                    break;
                } 
            }
            if (out[outindx - 1]) free += virus[outindx] - virus[outindx - 1] - 1;
            else {
                int i;
                for (i = 0; i < importance.size(); i++) 
                if (importance[i].second == outindx - 1) {
                    importance[i].first += (virus[outindx] - virus[outindx - 1]) / 2;
                    break;
                } 
            }
            cout << free << '\n';

        }

        importance.pop_back();

        sort(importance.begin(), importance.end());
        
        cout << free << '\n';
    }

    cout << answer << '\n';
    return 0;
}