#include <bits/stdc++.h>
#define ll long long
#define pb push_back

using namespace std;

int main(){
    int n, q;
    cin >> n >> q;

    vector<int> nums(q);
    for (int i = 0; i < q; i++) cin >> nums[i];

    vector<pair<int, int> > imp;
    imp.push_back(make_pair(nums[0] - 1 + (nums[1] - nums[0]) / 2, 0));
    for (int i = 1; i < q - 1; i++) imp.push_back(make_pair((nums[i] - nums[i - 1]) / 2 + (nums[i + 1] - nums[i]) / 2, i));
    imp.push_back(make_pair(n - nums[q - 1] - 1 + (nums[q - 1] - nums[q - 2]) / 2, q - 1));

    sort(imp.begin(), imp.end());

    int answer = 0;
    vector<bool> out(q, false);

    while(imp.size() != 0) {
        // for (int i = 0; i < imp.size(); i++) {
        //     cout << imp[i].first << ' ' << imp[i].second << '\n';
        // }
        // cout << "----------------\n";

        int indx = imp.back().second, left, right, tl, tr;
        left = indx - 1;
        while ((indx != 0 && left >= 0) && !out[left]) left--;
        if (left == -1 || indx == 0) left = 0;
        else left = nums[left];

        right = indx + 1;
        while ((indx != q - 1 && right < q) && !out[right]) right++;
        if (right == q || indx == q - 1) right = n - 1;
        else right = nums[right] - 2;

        // cout << left << ' ' << right << '\n';

        answer += right - left;

        if (indx != 0) {
            tl = indx - 1;
            while (tl >= 0 && out[tl]) tl--;
            if (tl != -1)
            for (int i = 0; i < imp.size(); i++) 
            if (imp[i].second == tl) {
                imp[i].first += (nums[indx] - nums[left]) / 2;
                break;
            }
        } 

        if (indx != q - 1) {
            tr = indx + 1;
            while (tr < q && out[tr]) tr++;
            if (tr != q)
            for (int i = 0; i < imp.size(); i++) 
            if (imp[i].second == tr) {
                imp[i].first += (nums[right] - nums[indx]) / 2;
            }
        } 

        imp.pop_back();
        out[indx] = true;
        sort(imp.begin(), imp.end());
    }

    cout << answer << '\n';
}