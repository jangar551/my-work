#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define INF 100000000

using namespace std;

void sale(double& money, int cnt) {
    if (cnt >= 50) money = money / 8 * 10;
    else if (cnt >= 30) money = money / 85 * 100;
    else if (cnt >= 10) money = money / 9 * 10;
}

int main(){
    int n, cnt;
    double money, ans = INF;
    cin >> n;
    while (n--) {
        cin >> money >> cnt;
        money -= 2000;
        sale(money, cnt);
        money /= cnt;
        ans = min(ans, money);
    }

    cout << fixed << setprecision(2) << ans;

    return 0;
}