#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int t, n, minspeed, sum, tmp;
    cin >> t;
    while(t--) {
        cin >> n;
        sum = 0;
        minspeed = INT_MAX;
        for (int i = 0; i < n; i++) {
            cin >> tmp;
            sum += tmp;
            minspeed = min(minspeed, tmp);
        }
        
        cout << sum - minspeed << '\n';
    }
}