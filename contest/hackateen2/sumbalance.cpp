#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll n, back = 0, front = 0;
    cin >> n;
    vector<ll> nums(n);
    for (ll i = 0; i < n; i++) cin >> nums[i], back += nums[i];

    ll i;
    for (i = 0; i < n; i++) {
        back -= nums[i];
        if (front == back) {
            cout << i << '\n';
            break;
        }
        front += nums[i];
    } 

    if (i == n) cout << "invalid\n";

    return 0;
}