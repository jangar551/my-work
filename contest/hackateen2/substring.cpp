#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    string s, match;
    int n, now, tmp;
    cin >> s >> n;
    vector<vector<int> > near(s.length(), vector<int>(53, -1)), loc(53);
    for (int i = 0; i < s.length(); i++) 
    if (s[i] > 'z') loc[s[i] - 'A' + 26].pb(i);
    else loc[s[i] - 'a'].pb(i);
    
    vector<int> indx (53, 0);

    for (int i = 0; i < s.length(); i++) 
    for (int j = 0; j < 52; j++) {
        // cout << i << ' ' << j << " gege\n";
        // for (int k = 0; k < 52; k++) cout << indx[i] << ' ';
        // cout << '\n';
        if (indx[j] < loc[j].size()) {
            near[i][j] = loc[j][indx[j]];
            if (loc[j][indx[j]] == i) indx[j]++;
        }
    }
    for (int i = 0; i < s.length(); i++) { 
    for (int j = 0; j < 52; j++) 
        cout << near[i][j] << ' ';
    cout << '\n';
    }

    while (n--) {
        cin >> match;
        now = 0;

        for (int i = 0; i < match.length(); i++) {
            if (match[i] > 'z') tmp = match[i] - 'A' + 26;
            else tmp = match[i] - 'a';  
            // cout << i << ' ' << tmp << ' ' << now << '\n';          
            if (near[now][tmp] != -1) now = near[now][tmp] + 1;
            else {
                cout << "No\n";
                break;
            }

            if (i == match.length() - 1) cout << "Yes\n"; 
            else if (now > s.length() - 1) {
                cout << "No\n";
                break;
            }
        }
    }
}