#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int n, from = 0, last = 0, ans = 0;
    cin >> n;
    vector<int> nums(n);

    for (int i = 0; i < n; i++) 
        cin >> nums[i];
    
    for (int i = 1; i < n; i++) {
        int tmp = (last == from ? 0 : 1);
        ans = max(ans, i - from - tmp);
        if (nums[i] <= nums[i - 1]) {
            if ((i != 1 && nums[i - 2] < nums[i]) || (i != n - 1 && nums[i + 1] > nums[i - 1])) {
                from = last;
            } else {
                from = i;
            }
            last = i;
        }
        // cout << ans << '\n';
    }
    int tmp = (last == from ? 0 : 1);
    ans = max(ans, n - from - tmp);

    cout << ans << '\n';
    
    return 0;
}