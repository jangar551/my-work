#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int n, h = 0, now  = 0, l = 0, r, answer;
    string s;
    cin >> n >> s;
    for (int i = 0; i < n; i++)
        if (s[i] == 'H')
            h++;
    r = h - 1;
    answer = h;
    for (int i = 0; i < h; i++)
        if (s[i] == 'T')
            now++;
    
    for (int i = 0; i < n - 1; i++) {
        if (s[l] == 'T')
            now--;
        if (s[(r + 1) % n] == 'T')
            now++;
        answer = min(answer, now);
        l = (l + 1) % n;
        r = (r + 1) % n;
    }

    cout << answer << '\n';
    return 0;
}