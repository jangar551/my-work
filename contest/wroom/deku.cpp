#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int n, last = 0, start = 0, answer = 1;
    cin >> n;

    vector<int> nums(n);

    for (int i = 0; i < n; i++)
        cin >> nums[i];

    for (int i = 1; i < n; i++) {
        if (nums[i] > nums[i - 1]) 
            continue;
        
        answer = max(answer, i - start);
        if ((i != 1 && nums[i] > nums[i - 2])||(i != n - 1 && nums[i + 1] > nums[i - 1])) {
            start = last;
            last = i - 1;
        }
    }


    return 0;
}