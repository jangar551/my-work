#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

struct pair_hash {
    template <class T1, class T2>
    std::size_t operator () (const std::pair<T1,T2> &p) const {
        auto h1 = std::hash<T1>{}(p.first);
        auto h2 = std::hash<T2>{}(p.second);
        return h1 ^ h2;  
    }
};

void move (pair<int, int>& indx, char dir) {
    if (dir == 'R')
        indx.ff++;
    else if (dir == 'L')
        indx.ff--;
    else if (dir == 'U')
        indx.ss++;
    else
        indx.ss--;
}

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n;
    string s;
    cin >> t;
    while (t--) {
        cin >> n >> s;
        unordered_map<pair<int, int>, int, pair_hash> um;
        int answer = INT_MAX;
        pair<int, int> indx = mp(0, 0), lr = mp(-1, -1);
        um[indx] = 1;
        for (int i = 0; i < n; i++) {
            move(indx, s[i]);
            if (um.find(indx) != um.end()) {
                if (i + 1 - um[indx] + 1 < answer) {
                    answer = i + 1 - um[indx] + 1;
                    lr.ff = um[indx];
                    lr.ss = i + 1;
                }
            } 
            um[indx] = i + 2;   
        }

        if (lr.ff == -1)
            cout << -1 << '\n';
        else
            cout << lr.ff << ' ' << lr.ss << '\n'; 
    }   
    return 0;
}