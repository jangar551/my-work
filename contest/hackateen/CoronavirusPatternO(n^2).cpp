#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    string s;
    cin >> s;

    long long answer = 0;
    for (int i = 0; i < s.length() - 2; i++) {
        vector<int> count (10, 0);
        for (int j = i + 1; j < s.length(); j += 2) {
            count[s[j - 1] - '0']++;
            count[s[j] - '0']++;

            for (int m = 0; m < 10; m++) {
                if (count[m] % 2 != 0) break; 
                if (m == 9) answer++;
            }
        }
    }

    cout << answer << '\n';

    return 0;
}