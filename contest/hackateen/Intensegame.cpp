#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define INF 100000005

using namespace std;

int main(){
    int t, n;
    cin >> t;

    while(t--) {
        cin >> n;

        vector<bool> dp(n, true);

        dp[2] = false;
        dp[3] = false;
        dp[4] = true;
        int last = 3, tmp;

        if (n == 1) cout << "FIRST\n";
        else if (n == 2) cout << "SECOND\n";
        else if (n == 3) cout << "SECOND\n";
        else if (n == 4) cout << "FIRST\n";
        else {
            for (int i = 5; i < n; i++) {
                tmp = i / 3;
                if (i % 3 == 0) tmp--;
            
                if (tmp < i - last) {
                    if (!dp[i - last]) dp[i] = false, last = i, i += tmp - 1;
                }
            }
            tmp = n / 3;
            if (n % 3 == 0) tmp--;

            if (tmp >= n - last) cout << "FIRST\n";
            else {
                if (dp[n - last]) cout << "FIRST\n";
                else cout << "SECOND\n";
            }
        }

    }

    return 0;
}