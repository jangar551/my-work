#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n;
    cin >> n;
    vector<int> chain(n);
    for (int i = 0; i < n; i++) cin >> chain[i];
    
    int sum = 0, back = 0;
    for (int i = 0; i < n / 2; i++) sum += chain[i];
    for (int i = n - 1; i >= n / 2; i--) back += chain[i];
    if (sum == n / 4 || back == n / 4) cout << 1 << '\n' << n / 2 << ' ' << n / 2 + 1 << '\n';
    else {
        for (int i = 1; i <= n / 2; i++) {
            sum -= chain[i - 1];
            sum += chain[i + n / 2 - 1];

            if (sum == n / 4) {
                cout << 2 << '\n';
                cout << i << ' ' << i + 1 << '\n';
                cout << i + n / 2 << ' ' << i + n / 2 + 1 << '\n';

                break;
            } 
        }
    }
    return 0;
}