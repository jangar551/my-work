#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    vector<double> nums(4), answer;
    for (int i = 0; i < 4; i++) cin >> nums[i];
    double highest = INT64_MIN;
    for (int i = 0; i < 4; i++) highest = max(highest, nums[i]);
    for (int i = 0; i < 4; i++) if (nums[i] == highest) answer.pb(nums[i]);
    for (int i = 0; i < answer.size() - 1; i++) cout << answer[i] << ' ';
    cout << answer.back() << '\n';   
}