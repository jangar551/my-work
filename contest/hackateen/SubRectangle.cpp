#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int findmax(vector<int> sum) {
    int answer = INT_MIN, tmp = 0;

    for (int i = 0; i < sum.size(); i++) {
        if (tmp <= 0) tmp = sum[i];
        else tmp += sum[i];

        answer = max(answer, tmp);
    }

    return answer;
}

int main(){
    int n;
    cin >> n;
    vector<vector<int> > grid( n, vector<int>(n));
    for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
        cin >> grid[i][j];
    
    vector<int> sum(n);
    
    int answer = INT_MIN;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) sum[j] = grid[j][i];
        for (int j = i; j < n; j++) {
            if (j != i) for (int m = 0; m < n; m++) sum[m] += grid[m][j];
            // cout << i << ' ' << j << '\n';
            // for (int m = 0; m < n; m++) cout << sum[m] << ' ';
            // cout << findmax(sum);
            // cout << "\n ---------------------------- \n";
            answer = max(answer, findmax(sum));
        }
    }

    cout << answer << '\n';
}