#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    int n, k;
    cin >> n >> k;
    vector<int> nums(n); 
    for (int i = 0; i < n; i++) cin >> nums[i];

    unordered_map<int, int> map;

    int tmp;
    for (int i = 0; i < n; i++) {
        if (map.find(nums[i]) == map.end()) map.insert(mp(nums[i], 0));
        tmp = map[nums[i]];
        map.erase(nums[i]);
        map.insert(mp(nums[i], tmp + 1));
    }

    int answer = 0;
    for (auto i : map) {
        // cout << i.first << ' ' << i.second << '\n';
        if (map.find(k - i.first) != map.end()) answer += i.second * map[k - i.first];

        if (i.first + i.first == k) answer -= i.second * map[k - i.first];
        // cout << answer << '\n';
    }

    if (k % 2 == 0) {
        if (map.find(k / 2) != map.end() && map[k / 2] >= 2) answer += map[k / 2] * (map[k / 2] - 1);
    }

    cout << answer / 2 << '\n';
    return 0;
}