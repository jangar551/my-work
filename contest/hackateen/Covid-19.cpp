#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    long long d, k;
    double r, p;
    cin >> p >> d >> k >> r;

    p *= pow(2, k);
    d -= 6 * k;
    double m = 2 - r;

    while (d >= 6 && m > 1) {
        d -= 6;
        p *= m;
        m -= r;
    }

    if (d > 0 && m > 1) {
        p += (p * m - p) * d / 6;
    }

    cout << int (p) << '\n';

    return 0;
}