#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, n, mile, juice, call;
    cin >> t;
    while (t--) {
        cin >> n;
        mile = juice = 0;
        for (int i = 0; i < n; i++) {
            cin >> call;
            mile += ((call + 29) / 30) * 10;
            juice += ((call + 59) / 60) * 15;
        }

        if (mile < juice) 
            cout << "Mile " << mile << '\n';
        else if (juice < mile)
            cout << "Juice " << juice << '\n';
        else
            cout << "Mile Juice " << mile << '\n';
    }
    return 0;
}