#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, now, answer = 0;
    string s;
    cin >> t;
    while (t--) {
        cin >> s;
        now = answer = 0;
        for (int i = 0; i < s.size(); i++) {
            if (s[i] == 'O') {
                now++;
                answer += now;
            } else 
                now = 0;
        }
        cout << answer << '\n';
    }
    return 0;
}