#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    string s;
    int T = 0;
    string hello[6] = {"HELLO", "HOLA", "HALLO", "BONJOUR", "CIAO", "ZDRAVSTVUJTE"};
    string lang[6] = {"ENGLISH", "SPANISH", "GERMAN", "FRENCH", "ITALIAN", "RUSSIAN"};

    while (cin >> s) {
        T++;
        if(s == "#")
            break;
        
        cout << "Case " << T << ": ";
        for (int i = 0; i < 6; i++) {
            if (s == hello[i]) {
                cout << lang[i] << '\n'; 
                break;
            }

            if (i == 5)
                cout << "UNKNOWN\n";
        }
    }
    return 0;
}