#include <bits/stdc++.h>
using namespace std;
#define o vector<vector<int> >

int p (o& s, int i, int j) {
    if ((i >= s.size() || i < 0) || (j >= s[0].size() || j < 0) || s[i][j] == 0)
        return 0;
    s[i][j] = 0;
    return 1 + p(s, i + 1, j) + p(s, i - 1, j) + p(s, i, j + 1) + p(s, i, j - 1); 
}

int main() {
    int t, n, m, k, q;
    cin >> t;
    while (t--) {
        cin >> n >> m >> k;
        q = 0;
        o s(n, vector<int>(m));
        for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) cin >> s[i][j];
        for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) if (p(s, i, j) >= k) q++;
        cout << q << '\n';
    }
    return 0;
}
