#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define int long long

using namespace std;

int n;
vector<vector<int> > edge;
vector<int> nums, segtree,  cost, newindx, child;

void init(int now) {
    // cout << now << ' ';
    nums.pb(cost[now]);
    newindx[now] = nums.size() - 1;
    
    for (auto el : edge[now]) {
        init(el);
        child[now] += child[el] + 1;
    }
}

void build() { 
	for (int i=0; i<n; i++)	 {
        // cout << nums[i] << ' ';
		segtree[n+i] = nums[i]; 
    }
    // cout << '\n';

	for (int i = n - 1; i > 0; --i)	 
		segtree[i] = max(segtree[i<<1], segtree[i<<1 | 1]);	 
} 

void update(int p, int value) { 
    // cout << "oi-> " << p << '\n';
	segtree[p+n] = value; 
	p = p+n; 

	for (int i=p; i > 1; i >>= 1) 
		segtree[i>>1] = max(segtree[i], segtree[i^1]); 
} 

int query(int l, int r) { 
    r++;
	int res = 0; 
	
	for (l += n, r += n; l < r; l >>= 1, r >>= 1) { 
		if (l&1) 
			res = max(res, segtree[l++]); 
	
		if (r&1) 
			res = max(res, segtree[--r]); 
	} 
	
	return res; 
} 

signed main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int t, q, u, v, x;
    cin >> t;
    while (t--) {
        cin >> n >> q;
        unordered_map<int, int> key;
        cost.resize(n + 1);
        edge.resize(n + 1);
        newindx.resize(n + 1);
        child.resize(n + 1, 0);
        segtree.resize(n * 2 + 1);
        for (int i = 0; i < n; i++) {
            cin >> cost[i];
            key[cost[i]] = i;
        }
        vector<int> head(n + 1, -1);
        for (int i = 0; i < n - 1; i++) {
            cin >> u >> v;
            edge[u].pb(v);
            head[v] = u;
        }

        for (int i = 0; i < n; i++) {
            // cout << head[i] << ' ';
            if (head[i] == -1) init(i);
        }
        // cout << '\n';
        // cout << '\n';
        build();
        // for (int i = 0; i < n; i++)
        //     cout << child[i] << ' ';
        // cout << '\n';
        // for (int i = 0; i < n; i++)
        //     cout << newindx[i] << ' ';
        // cout << '\n';
        while (q--) {
            cin >> x;
            int ans = query(newindx[x], newindx[x] + child[x]);
            cout << ans << '\n';
            // cout << key[ans] << '\n';
            if (ans != 0)
                update(newindx[key[ans]], 0);
        }
        edge.clear();
        cost.clear();
        newindx.clear();
        child.clear();
        segtree.clear();
        nums.clear();
    }
    return 0;
}