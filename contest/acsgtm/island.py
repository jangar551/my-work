b = [[]];

def p(s, i, j):
    if ((i >= len(s) or i < 0) or (j >= len(s[0]) or j < 0) or s[i][j] == 0 or b[i][j]):
        return 0;
    b[i][j] = True;
    return 1 + p(s, i + 1, j) + p(s, i - 1, j) + p(s, i, j + 1) + p(s, i, j - 1); 

t = int(input());
while t>=0:
    n = int(input());
    m = int(input());
    k = int(input());
    s = [[]];
    ans = 0;
    for i in range(n):
        for j in range(m):
            s[i][j] = int(input());
            b[i][j] = False;
    for i in range(n):
        for j in range(m):
            if (p(s, i, j) >= k)
                ans+=1;
    print(ans);
    t-=1;
