#include <iostream>
#include <vector>

using namespace std;

int main(){
    int n, m, q, move, a, b, book;
    cin >> n >> m >> q;
    
    vector<vector<bool> > shelf(n, vector<bool>(m, 0));

    for(int i = 0; i < q; i++){
        cin >> move;
        book = 0;

        if(move == 1){
            cin >> a >> b;
            a--;
            b--;
            if(!shelf[a][b]){
                shelf[a][b] = 1;
                book++;
            }
            cout << book << '\n';
  
            continue;
        }

        if(move == 2){
            cin >> a >> b;
            a--;
            b--;
            if(shelf[a][b]){
                shelf[a][b] = 0;
                book--;
            }
            cout << book << '\n';
            continue;
        }
        
        if(move == 3){
            cin >> a;
            a--;
            for(int l = 0; l < m; l++){
                if(shelf[a][l]){
                    shelf[a][l] = 0;
                    book--;
                }
                if(!shelf[a][l]){
                    shelf[a][l] = 1;
                    book++;
                }
            }

            cout << book << '\n';
            continue;
        }
        
        if(move == 4){
            cin >> a;
            a--;
            cout << book << '\n';   
            continue;
        }
    }
}