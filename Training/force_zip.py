import zipfile
from zipfile import ZipFile

myfile = ZipFile("bruteforce.zip")

data = ''
for x in range(1000, 10000):
    password = str(x)
    try:
        data = myfile.extractall(pwd=password)
    except:
        pass
    if data != '':
        break
print(password)
