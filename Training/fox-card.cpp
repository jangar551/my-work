#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>

using namespace std;

bool customtolow(int a, int b){
    return a > b;
}


int main(){
    int ciel = 0, jiro = 0, p, n, c;
    vector<int> needsort;

    cin >> n;

    for(int i = 0; i < n; i++){
        cin >> p;

        for(int k = 0 ;k < p / 2; k++){
            cin >> c;
            ciel += c;
        }

        if(p % 2 == 1){
            cin >> c;
            needsort.push_back(c);  
        }

        for(int k = 0 ;k < p / 2; k++){
            cin >> c;
            jiro += c;
        }
            
    }

    sort(needsort.begin(), needsort.end(), customtolow);
    
    for(int i = 0; i < needsort.size(); i++)
        if(i % 2 == 0)
            ciel += needsort[i];
        else
            jiro += needsort[i];

    cout << ciel << ' ' << jiro;

    return 0;
}