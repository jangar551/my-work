#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>

using namespace std;

int main(){
    int t, l;
    string s1, s2;

    cin >> t;

    for(int i = 0; i < t; i++){
        cin >> l >> s1 >> s2;
        vector<int> count1(26, 0), count2(26, 0);
        vector<char> first, second;
        for(int k = 0; k < l; k++){
            count1[s1[k] - 97]++;
            count2[s2[k] - 97]++;
             
        }
        for(int k = 0; k < 27; k++)
        if(count1[k] != count2[k]){
            cout << "NO\n";
            return 0;
        }

    }
}
