#include<iostream>
#include<vector>
#include<cmath>

using namespace std;

#define max_n  100000

int binarysearch(vector<int>& primes, int& n, int p1, int p2){
    if(p2 - p1 == 1){
        if(n >= primes[p2])
            return p2;
        else
            return p1;
    }

    int mid = p1 + (p2 - p1) / 2; 

    if(n > primes[mid])
        return binarysearch(primes, n, mid, p2);
    else
        return binarysearch(primes, n, p1, mid);
}

int main(){

    vector<int> primes;

    for (int i = 2; i <= max_n; i++){
        bool is_prime = true;
        for (int j = 2; j <= sqrt(i) && is_prime; j++)
            if (i % j == 0)
                is_prime = false;
        if (is_prime)
            primes.push_back(i);
        
    }
    cout << primes.size() << endl;
    
    int n, g;
    cin >> g;
    while(g--){
        cin >> n;
        if(binarysearch(primes, n, 0, 9592) % 2 == 0)
            cout << "Alice\n";
        else
            cout << "Bob\n";
    }

    return 0;
}

