#include <iostream>
#include <vector>

using namespace std;

int medofmed(vector<int> nums, int l, int r){
    vector<int> meds;

    int i;

    for (i = l + 5; i <= r; i += 5) {
        sort(nums.begin() + i - 5, nums.begin() + i);
        meds.push_back(nums[i + 2]);
    }

    if ((r - l) % 5 > 0) {
        sort(nums.begin() + i - 5, nums.begin() + r);
        meds.push_back(nums[(r + i - 5) / 2]);
    }

    sort(meds.begin(), meds.end());

    return meds[meds.size() / 2];
}

int partition(vector<int>& nums, int l, int r, int med) {
    for (int i = l; i <= r; i++)
        if (nums[i] == med) {
            swap(nums[i], nums[r]);
            break;
        }
    int now = l;

    for (int i = l; i < r; i++)
        if (nums[i] < med) swap(nums[i], nums[now]), now++;
    
    swap(nums[now], nums[r]);
    
    return now;
}
int findkthsmall(vector<int>& nums, int l, int r, int k){
    if(l == r)
        return nums[l];
    int med = medofmed(nums, l, r);
    int partit = partition(nums, l, r, med);

    if (partit == k)
        return nums[partit];
    
    if (partit > k)
        return findkthsmall(nums, l, partit - 1, k);
    
    return findkthsmall(nums, partit + 1, r, k);
}

int main(){
    int n, k;
    cin >> n;    
    vector<int> nums(n);

    for (int i = 0; i < n; i++)
        cin >> nums[i];
    
    cin >> k;
    k--;

    cout << findkthsmall(nums, 0, n - 1, k) << '\n';
    // cout << medofmed(nums, 0, n - 1);


    return 0;
}