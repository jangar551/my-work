#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <unordered_map>
#define ll long long

using namespace std;

int main(){
    ll t, n, p;
    
    cin >> t;

    vector<bool> answer(t);

    for(ll i = 0; i < t; i++){
        cin >> n;
        p = 0;
        vector<bool> findprime(n + 1, 1);
        for(ll k = 2; k <= n; k++){
            if(findprime[k] == 1){
                for(ll j = k * k; j < n; j += k)
                    findprime[j] = 0;
            }
        }

        for(int k = 2; k <= n; k++)
            if(findprime[k])
                p++;

        cout << p << '\n';
        
        answer[i] = p % 2;
    }

    for(ll i = 0; i < t; i++)
        if(answer[i])
            cout << "Alice\n";
        else
            cout << "Bob\n";

    return 0;
}