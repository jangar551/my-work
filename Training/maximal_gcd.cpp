#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#define ll long long

using namespace std;

int main(){
    ll n, k, m = 0;// m = max

    cin >> n >> k;

    ll sum = k * (k + 1) / 2;

    vector<ll> answers;

    if(k > n){
        cout << -1 << '\n';
        return 0;
    }

    for(int i = 1; i <= sqrt(n); i++){
        if(n % i == 0 && n / i >= sum)
            answers.push_back(i);
    
        if(n % i == 0 && i >= sum)
            answers.push_back(n / i);
    }

    if(answers.size() != 0){
        for(int i = 0; i < answers.size(); i++){
            m = max(m, answers[i]);
        }

        for(int i = 1; i < k; i++)
            cout << i * m << ' ';

        cout << (n / m - sum + k) * m;

    }else cout << -1 << '\n';

    return 0;
}