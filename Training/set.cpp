#include<iostream>
#include<set>
#include<cmath>

using namespace std;

#define max_n  100000

int main(){

    set<int> primes;

    for (int i = 2; i <= max_n; i++){
        bool is_prime = true;
        for (int j = 2; j <= sqrt(i) && is_prime; j++)
            if (i % j == 0)
                is_prime = false;
        if (is_prime)
            primes.insert(i);
        
    }
    
    int n, g;
    cin >> g;
    while(g--){
        cin >> n;
        set<int>::iterator high = primes.upper_bound(n), low = primes.lower_bound(n);
        // for (it = primes.begin(); it != primes.end(); it++)
        //     cout << it - primes.begin() << " ";
        
        if(high == low)
            cout << distance(primes.begin(), low) - 1<< endl;
        else 
            cout << distance(primes.begin(), low) << endl;
    }

    return 0;
}

