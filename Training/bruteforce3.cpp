#include <iostream>
#include <vector>
#define int long long

using namespace std;

vector<int> ans;

void comb(vector<int> use, int now, int number){
    if(now == 10){
        if(number % 11111 == 0){
            ans.push_back(number);
        }
    }

    vector<int> tmp;
    int answer = 0, tmpnum;

    if(number == 0){
        for(int i = 1;i < use.size(); i++){
            tmp = use; 
            tmpnum = number * 10 + use[i];

            swap(tmp[i], tmp[tmp.size() - 1]);
            tmp.pop_back();

            comb(tmp, now + 1, tmpnum);
        }
    }else{
        for(int i = 0;i < use.size(); i++){
            tmp = use; 
            tmpnum = number * 10 + use[i];

            swap(tmp[i], tmp[tmp.size() - 1]);
            tmp.pop_back();

            comb(tmp, now + 1, tmpnum);
        }
    }

   

}

signed main(){
    vector<int> use;

    for(int i = 0; i <= 9; i++){
        use.push_back(i);
    }

    comb(use, 0, 0);

    cout << ans.size() << '\n';
}