#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>

using namespace std;

bool newscheck(string s){
    string secret = "heidi";
    int p = 0; //pointer

    for(int i = 0; i < s.length(); i++){
        if(s[i] == secret[p]){
            p++;
            if(p == 5)
                return 1;
        }
    }

    return 0;
}

int main(){
    string s;

    cin >> s;

    if(newscheck(s))
        cout << "YES\n";
    else
        cout << "NO\n";
    return 0;
}