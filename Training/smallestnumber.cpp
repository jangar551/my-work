#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>
#define int long long

using namespace std;

int ans = LLONG_MAX;

void rotatekick(vector<char>& operation, int n){
    if(n == operation.size() - 1)
        return;
    
    swap(operation[n], operation[n + 1]);
    rotatekick(operation, n + 1);
}

void brute(vector<int> num, vector<char> operation){
    if(num.size() == 1){
        ans = min(ans, num[0]);
        return;
    }
    
    vector<int> tmp, tmp2;
    vector<char> tmpchar;
    int a, b;

    for (int i = 0; i < num.size(); i++) {
        for (int k = i + 1; k < num.size(); k++) {
            tmp = num;
            a = num[i];
            b = num[k];
            if(k == num.size() - 1)
                swap(tmp[i], tmp[num.size() - 2]);
            else
                swap(tmp[i], tmp.back());
            swap(tmp[k], tmp[tmp.size() - 2]);
    
            tmp.pop_back();        
            tmp.pop_back();        

            if(operation[0] == '*')
                tmp.push_back(a * b);
            else 
                tmp.push_back(a + b);
            
            tmpchar = operation;
            rotatekick(tmpchar, 0);
            tmpchar.pop_back();
            brute(tmp, tmpchar);
        }
    }
}

signed main(){
    vector<int> num(4);
    for(int i = 0; i < 4; i++)
        cin >> num[i];
    vector<char> operation(3, 0);
    for(int i = 0; i < 3; i++)
        cin >> operation[i];
    
    brute(num, operation);
    cout << ans << '\n';
    return 0;
}