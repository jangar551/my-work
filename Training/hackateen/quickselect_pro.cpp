#include <bits/stdc++.h>

using namespace std;

// int partition(vector<int>& nums, int l, int r) {
//     int indx = l;

//     for (int i = l; i < r; i++) 
//         if (nums[i] < nums[r]) {
//             swap(nums[i], nums[indx]);
//             indx++;
//         }

//     swap(nums[indx], nums[r]);

//     return indx;
// }

int medofmed (vector<int> nums, int l, int r) {
    
}

int partition (vector<int>& nums, int l, int r, int med) {

}

int quickselect (vector<int>& nums, int k, int l, int r) {
    int med = medofmed(nums, l, r);
    int mid = partition(nums, l, r, med);

    // for (int i = 0; i < nums.size(); i++) 
    //     cout << nums[i] << ' ';
    // cout << '\n';
    // cout << mid << '\n';

    if (mid == k) return nums[mid];
    if (mid > k) return quickselect(nums, k, l, mid - 1);
    return quickselect(nums, k, mid + 1, r);
}

int main(){
    int n;
    cin >> n;
    vector<int> nums(n);

    for (int i = 0; i < n; i++) cin >> nums[i];

    int k;
    cin >> k;
    k--;

    cout << quickselect(nums, k, 0, n - 1) << '\n';
}
