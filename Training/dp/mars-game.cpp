#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
#define int ll

using namespace std;

int dp_function(vector<vector<int> >& mult, int deg) {
    vector<pair<bool, vector<int> > > dp(1000, mp(0, vector<int>(0)) ), newdp;
    dp[0].ff = true;
    dp[0].ss.pb(0);
    int zero = 0;

    for (int i = 0; i < mult[deg - 1].size(); i++) {
        // cout << "=======\n" << i << '\n';
        // cout << mult[deg - 1][0] << '\n';
        // cout << "-------\n";
        newdp = dp;
        for (int j = 0; j < 50; j++) {
            if (dp[j].ff) {
                // cout << j << '\n';
                newdp[j + mult[deg-1][i]].ff = true;
                for (int l = 0; l < dp[j].ss.size(); l++)
                    newdp[j + mult[deg-1][i]].ss.pb(dp[j].ss[l] + 1);
            }
        }
        dp = newdp;
    }

    for (int i = deg -1; i >= 0; i--) 
        if (dp[i].ff) {
            for (int j = 0; j < dp[i].ss.size(); j++)
                if (dp[i].ss[j] == deg)
                    return i;
        }
    
    return zero;
}

int findmx(vector<vector<int> >& mult) {
    int n = mult.size(), answer, k = 1, now;
    answer =  0;
    for (int i = 2; i <= n; i++) {
        now = dp_function(mult, i);
        // cout << now << '\n';
        if (now > answer) {
            answer = now;
            k = i;
        }
    }

    return k;
}

signed main(){
    ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
    int n, push;
    vector<int> nums(n);
    cin >> n;
    for (int i = 0; i < n; i++) 
        cin >> nums[i];

    vector<vector<int> > mult(n, vector<int>(n));
    // cout << nums[0] << '\n';
    for (int i = 0; i < n; i++) {
        // cout << i << " <-\n";
        for (int j = 1; j <= n; j++) {
            push = 1;
            for (int k = 0; k < j; k++) {
                push *= nums[i];
                // cout << push << "\n" <<  "--\n";
                push %= j;
            }  
            mult[j-1][i] = push;          
            // cout << push << '\n' << "=-=-=\n";
        }
    }

    cout << findmx(mult) << '\n';
    return 0;
}