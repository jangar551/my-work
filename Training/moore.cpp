#include <iostream>
#include <vector>

using namespace std;

int main(){
    string a, b;
    cin >> a >> b;
    int now = 0;
    vector<int> starts;

    for (int i = 0; i < a.length(); i++) 
        if (a[i] == b[0]) starts.push_back(i);
    
    for (int i = 0; i < starts.size(); i++) {
        int j;
        for (j = 0; j < b.length(); j++) 
            if(a[starts[i] + j] != b[j]) break;
        if(j == b.length()) {
            cout << starts[i] + 1 << '\n';
            return 0;
        }
    }
    cout << "not found\n";

    return 0;
}