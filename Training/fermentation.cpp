#include <iostream>
#include <vector>

using namespace std;

void ferment(int s, vector<int> num){
    if(s == num.size()){
        for(int i = 0; i < num.size(); i++)
            cout << num[i] << ' ';
        cout << '\n';
        return;
    }

    for(int i = s; i < num.size(); i++){
        swap(num[i], num[s]);
        ferment(s + 1, num);
        swap(num[i], num[s]);
    }
}

int main(){
    int n;
    cin >> n;
    vector<int> num(n);
    for(int i = 0; i < n; i++)
        cin >> num[i];
    
    ferment(0, num);

    return 0;
}