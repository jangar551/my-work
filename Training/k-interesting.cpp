#include<iostream>
#include<vector>

using namespace std;

int countbit(int n) { 
    int count = 0; 
    while (n > 0) { 
        n &= (n - 1); 
        count++; 
    } 
    return count; 
}

int main(){
    int n, k, answer = 0, tmp;

    cin >> n >> k;

    vector<int> number(n);

    for(int i = 0; i < n; i++)
        cin >> number[i];

    for(int i = 0; i < n - 1; i++)
        for(int j = i + 1; j < n; j++){

            if(countbit(number[i] ^ number[j]) == k)
                answer++;
        }

    cout << answer << '\n';

}

