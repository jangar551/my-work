#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int binsearch(int l, int r, int in, vector<ll>& nums) {
    int middle = (l + r) / 2;
    if (middle == l || middle == r) return nums[l] + in > 0 ? nums.size() - l : nums.size() - r;
    if (nums[middle] + in > 0) return binsearch(l, middle, in, nums);
    else return binsearch(middle, r, in, nums); 
}

int main(){
    ll n, tmp;
    cin >> n;
    vector<ll> nums(n);
    for (int i = 0; i < n; i++) cin >> nums[i]; 
    for (int i = 0; i < n; i++) cin >> tmp, nums[i] = nums[i] - tmp;

    sort(nums.begin(), nums.end());

    for (int i = 0; i < nums.size(); i++) cout << nums[i] << ' ';
    cout << '\n';
    ll ans = 0;
    for (int i = 0; i < n; i++) {
        ans += binsearch(0, n - 1, nums[i], nums);
        if (nums[i] > 0) ans--;
        cout << ans << '\n';
    }

    cout << ans / 2 << '\n';
    return 0;
}