#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    ll n, m, a;
    cin >> n >> m >> a;
    if (n % a == 0) n /= a;
    else n = (n + a) / a;
    
    if (m % a == 0) m /= a;
    else m = (m + a) / a;
    
    cout << m * n << '\n';

    return 0;
}