#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int change(string num) {
    int ans = 0, mult = 1;
    for (int i = num.length() - 1; i >= 0; i--) {
        ans += (num[i] - 'A' + 1) * mult;
        mult *= 26;
    }

    return ans;
}

string change(int num) {
    int div = 1, plus = 1;
    string ans = "";
    char tmp;
    while (num > plus) div *= 26, plus += plus * div;
    plus /= div + 1;
    div /= 26;
    
    while (num != plus) {
        tmp = (num - plus) / div + 'A';
        ans += tmp;
        plus += ((num - plus) / div) * div;
        div /= 26;
    }
}

int main(){
    int t;
    string s;
    cin >> t;
    while (t--) {
        cin >> s;
        if (s[0] == 'R' && s[1] <= 57) {
            int row = 0, column = 0;
            int i;
            for (i = 1; i < s.length(); i++) {
                if (s[i] == 'C') break;
                row = row * 10 + (s[i] - '0');
            }
            i++;
            for (; i < s.length(); i++) 
                column = column * 10 + (s[i] - '0');
            
            cout << change(column) << row << '\n';
        } else {
            int row = 0, i;
            string column = "";
            for (i = 0; i < s.length(); i++) {
                if (s[i] <= 57) break;
                column += s[i];
            }
            for (; i < s.length(); i++) row = row * 10 + (s[i] - '0');

            cout << 'R' << row << 'C' << change(column) << '\n';
        }
    }
}