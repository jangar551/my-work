#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    //11101101
    //111100101
    //n * m ? subrectange

    int n, m, k, tmp;
    cin >> n >> m >> k;
    vector<int> s1(n), s2(m);
    for (int i = 0; i < n; i++) cin >> s1[i];
    for (int i = 0; i < m; i++) cin >> s2[i];

    vector <int> one1, one2;
    vector <pair<int, int> > comp1, comp2;

    for (int i = 0; i < n; i++) {
        if (s1[i] != 1) continue;
        tmp = 0;
        while (i < n && s1[i] == 1) i++, tmp++;
        one1.pb(tmp);
        i--;
    }
    for (int i = 0; i < m; i++) {
        if (s2[i] != 1) continue;
        tmp = 0;
        while (i < m && s2[i] == 1) i++, tmp++;
        one2.pb(tmp);
        i--;
    }
    for (int i = 0; i < one1.size(); i++) {
        tmp = 1;
        while (i < n - 1 && one1[i + 1] == one1[i]) i++, tmp++;
        comp1.pb(mp(one1[i - 1], tmp));
    }
    for (int i = 1; i < one2.size(); i++) {
        tmp = 1;
        while (one2[i - 1] == one2[i]) i++, tmp++;
        comp2.pb(mp(one2[i - 1], tmp));
    }

    int i1, i2;
    long long answer = 0;
    for (int i = 0; i < comp1.size(); i++) {
        for (int j = 0; j < comp2.size(); j++) {
            i1 = comp1[i].ff, i2 = comp2[j].ff;
            cout << i1 << ' ' << i2 << '\n'; 
            for (int l = 1; l <= i1; l++) {
                if (k % l == 0 && i2 >= k / l) {
                    answer += (i1 - l + 1) * (i2 - k / l + 1) * comp1[i].ss * comp2[j].ss;
                }
            }
        }
    }

    cout << answer << '\n';

    return 0;
}