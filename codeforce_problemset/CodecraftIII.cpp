#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;

int main(){
    vector<string> months {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    string month;
    int count;
    cin >> month >> count;

    for (int i = 0; i < 12; i++) 
    if (months[i] == month) {
        cout << months[(i + count) % 12];
        break;
    }
    
    return 0;
}