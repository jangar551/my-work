#include <iostream>
#include <time.h>

using namespace std;

double fRand(double fMin, double fMax){
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int main(){
    double fmin = 1.0, fmax = 1000000.0;
    srand(time(0));
    for(int i = 0; i < 8; i++)
        cout << fRand(fmin, fmax) << ' ' << fRand(fmin, fmax) << ' ' << fRand(fmin, fmax) << '\n';
    return 0;
}