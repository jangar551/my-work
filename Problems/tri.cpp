#include <iostream>
#include <vector>
#include <cmath>
#define ll long long
#define pb push_back

using namespace std;

vector<bool> used(10, 0);

bool check(ll n){
    while(n){
        if(!used[n % 10])
            return 0;
        n /= 10;
    }

    return 1;
}

int main(){
    ll n, tmp;

    cin >> n;

    while(tmp){
        used[tmp % 10] = 1;
        tmp /= 10;
    }

    vector<int> diviser;
    
    for(int i = 1; i < sqrt(n); i++)
        if(n % i == 0)
        diviser.pb(i);
    
    
}