#include <iostream>
#include <vector>

using namespace std;

vector<vector<int> > answer;

void check(vector<int> numbers){
    int sum = numbers[0] + numbers[1] + numbers[2];
    // cout << sum << '\n';
    for(int i = 0; i < 9; i += 2){
        // cout << numbers[i] + numbers[i + 1] + numbers[(i + 3) % 10] << '\n';
        if(numbers[i] + numbers[i + 1] + numbers[(i + 3) % 10] != sum)
            return;
    }

    cout << "HEEEY\n";

    answer.push_back(numbers);
}

void createpenta(vector<int> numbers, int pointer, vector<int> use){
    // cout << pointer << '\n';
    if(pointer == 9){
        numbers[9] = use[0];
        // for(int i = 0; i < 10; i++)
        //     cout << numbers[i] << ' ';
        // cout << '\n';
        check(numbers);
        return;
    }

    vector<int> tmp;
    for(int i = 0; i < use.size(); i++){
        numbers[pointer] = use[i];
        tmp = use;
        swap(tmp[i], tmp[tmp.size() - 1]);
        tmp.pop_back();
        createpenta(numbers, pointer + 1, tmp);
    }
}

int main(){
    vector<int> numbers(10);    
    vector<int> use;
    for(int i = 1; i <= 10; i++)
        use.push_back(i);

    createpenta(numbers, 0, use);

    for(int i = 0; i < answer.size(); i++){
        for(int k = 0; k < 9; k += 2){
            cout << answer[i][k] << ' ' << answer[i][k + 1] << ' ' << answer[i][(k + 2) % 9] << '    ';
        }

        cout << '\n';
    }

    return 0;
}