#include <iostream>

using namespace std;

int main(){
    string seat;
    cin >> seat;
    char lane = seat[seat.length() - 1];
    int number = 0;
    for(int i = 0; i < seat.length() - 1; i++)
        number = number * 10 + (seat[i] - '0');
    
    if(number <= 2){
        if(lane == 'B' || lane == 'C') 
            cout << "edge\n";
        else 
            cout << "window\n";
    }else{
        if(number <= 20){
            if(lane == 'A' || lane == 'F')
                cout << "window\n";
            else
                cout << "edge\n";
        }else{
            if(lane == 'A' || lane == 'K')
                cout << "window\n";
            else if((lane == 'C' || lane == 'D') || (lane == 'G' || lane == 'H'))
                cout << "edge\n";
            else
                cout << "middle\n";
        }
    }

    return 0;
}