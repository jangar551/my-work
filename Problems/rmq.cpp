#include<iostream>
#include<vector>

using namespace std;

struct SegmentTree {

    int len;
    vector<int> input, tree;

    int left(int i){
        return i * 2;
    }

    int right(int i){
        return i * 2 + 1;
    }
    int rmq(int cur, int l, int r, int i, int j){
        if (i > r || j < l)
            return -1;
        if (l >= i && r <= j)
            return tree[cur];
        int li = rmq(left(cur),l,(l+r)/2,i,j);
		int ri = rmq(right(cur),((l+r)/2)+1,r,i,j);
        if (li == -1)
            return ri;
        if (ri == -1) 
            return li;
        if (input[li] > input[ri])
            return li;
        return ri;   
    }
    int rmq(int l, int r){
        return rmq(1, 0, len - 1, l, r);
    }
    void build(int cur, int l, int r){
        if (l == r)
            tree[cur] = l;
        else{
            build(left(cur), l, (l + r) / 2);
            build(right(cur), (l + r) / 2 + 1, r);
            int lmax_ind = tree[left(cur)];
            int rmax_ind = tree[right(cur)];

            if (input[lmax_ind] > input[rmax_ind])
                tree[cur] = lmax_ind;
            else
                tree[cur] = rmax_ind;
        }
    }
    
    SegmentTree(vector<int> &data){
        input = data;
        len = data.size();
        tree.assign(3 * len, 0);
        build(1, 0, len - 1);
	}		
};

int main(){
    int n;
    cin >> n;
    vector<int> nums(n, 0);
    // for (int i = 0; i < n; i++)
    //     cin >> nums[i];
        
    SegmentTree st(nums);
    cout << st.rmq(0, 7) << endl;
    cout << nums[st.rmq(0, 7)] << endl;
    
    return 0;
}