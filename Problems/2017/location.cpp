#include <iostream>
#define MAX 200000

using namespace std;

int main(){
    string n, allnums = "";
    int now = 1;
    while(now <= MAX) allnums += to_string(now), now++;

    cin >> n;

    for (int i = 0; i < allnums.length() - n.length(); i++){
        int j;
        for (j = 0; j < n.length(); j++) if (n[j] != allnums[i + j])  break; 
        if (j == n.length()) {
            cout << i + 1 << '\n';
            return 0;
        }
    }

    return 0;
}