#include <iostream>
#include <iomanip>
#include <cmath>
#define pi 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679

using namespace std;

double distance(int x1, int y1, int x2, int y2){
    return sqrt(abs(x1 - x2) * abs(x1 - x2) + abs(y1 - y2) * abs(y1 - y2));
}

int main(){
    double x1, x2, y1, y2, r1, r2;
    cin >> x1 >> y1 >> r1 >> x2 >> y2 >> r2;
    double dist = distance(x1, y1, x2, y2);
    if(dist > r1 + r2){
        cout << fixed << setprecision(3) << 0 << '\n';
        return 0;
    }

    if(dist + min(r1, r2) < max(r1, r2)){
        cout << min(r1, r2) * min(r1, r2) * pi << '\n'; 
        return 0;
    }

    // double x = (r1 * r1 - r2 * r2 + dist * dist) / 2 * dist, y = sqrt(r1 * r1 - x * x);

    // cout << fixed << setprecision(3) << r1 * r1 *  asin(y / r1) + r2 * r2 * asin(y / r2) - y * (x + sqrt(x + sqrt(r2 * r2 - r1 * r1 + x * x))) << '\n';

    return 0; 
}