#include <iostream>
#include <vector>
#define ll long long

using namespace std;

vector<int> todigit(ll n){
    vector<int> digit, answer;

    while(n != 0){
        digit.push_back(n % 10);
        n /= 10;
    }
    for(int i = digit.size() - 1; i >= 0; i--)
        answer.push_back(digit[i]);
    
    return answer;
}

ll nextdan(ll& n){
    vector<int> digit = todigit(n);
    vector<bool> used(11, 0);
    used[0] = 1;
    ll tmp, ans = 0;

    for(int i = 0; i < digit.size(); i++){
        if(used[digit[i]]){
            for(int k = i + 1; k < digit.size(); k++)
                digit[k] = 1;
            
            // cout << i << " used " << digit[i] << '\n';
            while(used[digit[i]] && digit[i] != 10)
                digit[i]++;
            if(digit[i] != 10){ 
                used[digit[i]] = 1;
                continue;
            }
            // cout << digit[i] << '\n';
            tmp = i;
            while(tmp != 0 && digit[tmp] == 10){
                digit[tmp] = 1;
                tmp--;
                used[digit[tmp]] = 0;
                digit[tmp]++;
                while(used[digit[tmp]])
                    digit[tmp]++;
            }
            // cout << tmp << " test " << digit[tmp] << '\n';
            // for(int k = 0; k < digit.size(); k++){
            //         cout << digit[k] << ' ';
            // }
            // cout << '\n';
            // used[digit[tmp]] = 1;
            if(tmp == 0 && digit[tmp] == 10){
                digit[tmp] = 1;
                digit.push_back(1);    
            }
            
            i = tmp - 1;
            continue;
        }
        // cout << i << " ok\n";
        used[digit[i]] = 1;
    }

    for(int i = 0; i < digit.size(); i++)
        ans = ans * 10 + digit[i];
    
    return ans;
}

int main(){
    ll t, n;
    cin >> t;

    for(ll i = 0; i < t; i++){
        cin >> n;

        cout << nextdan(n) << '\n';
    }

    return 0;
}