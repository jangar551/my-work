#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#define ll long long

using namespace std;

// ll pow(ll n, ll power){
//     cout << power << " Carrot\n";
//     if(power == 2)
//         return n * n;
//     if(power == 1)
//         return n;
//     if(power == 0)
//         return 1;
//     if(power % 2 == 0)
//         return pow(pow(n, power / 2), 2);
//     return n * pow(pow(n, (power - 1) / 2), 2);
// }

vector<int> todigit(ll n){
    vector<int> digit, answer;

    while(n != 0){
        digit.push_back(n % 10);
        n /= 10;
    }
    for(int i = digit.size() - 1; i >= 0; i--)
        answer.push_back(digit[i]);
    
    return answer;
}
ll toBaseEight(ll n){
    if(n == 0)
        return 0;

    return toBaseEight(n / 8) * 10 + n % 8;
}

ll toDecimal(ll n, ll power){
    if(n == 0)
        return 0;
    return (n % 10) * pow(8, power) + toDecimal(n / 10, power + 1);
}

ll closestBaseEightPalindrome(ll n){
    vector<int> digit = todigit(n);
    ll ans = 0, half = 0, full = 8 * pow(10, digit.size() / 2 - 1), now = 0, tmp;

    for(int i = digit.size() / 2 + digit.size() % 2; i < digit.size(); i++)
        now = now * 10 + digit[i];

    for(int i = 0; i < digit.size() / 2; i++)
        digit[digit.size() - i - 1] = digit[i];
    
 
    for(int i = digit.size() / 2 ; i <= digit.size(); i--)
        half = half * 10 + digit[i];dfm

    cout << half << ' ' << full <<  ' ' << now << '\n';
    if(digit.size() % 2 == 1 && full - now + half < abs(now - half)){

        if(digit[digit.size() / 2] == 7){
            tmp = digit.size() / 2;
            while(tmp != -1 && digit[tmp] == 7){
                digit[tmp] = 0;
                tmp--;
            }
            if(tmp == -1){
                digit.push_back(0);
                swap(digit[digit.size() / 2], digit[digit.size() - 1]);
            }else{
                digit[tmp]++;
            }
            for(int i = 0; i < digit.size(); i++)
                ans = ans * 10 + digit[i];
            return closestBaseEightPalindrome(ans); 
        }
        digit[digit.size() / 2]++; 
    }
    

    for(int i = 0; i < digit.size(); i++)
        ans = ans * 10 + digit[i];

    return toDecimal(ans, 0);
}

int main(){
    ll t, n, base;
    cin >> t;

    while(t--){
        cin >> n;
        base = toBaseEight(n);
        cout << closestBaseEightPalindrome(base) << ' ';
    }
    cout << '\n';
}