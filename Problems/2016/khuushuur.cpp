#include <iostream>

using namespace std;

int main(){
    int n, m, k, answer = 0;

    cin >> k >> m >> n;
    //k - size of pan, m = per one side, n = count

    answer += (n / k) * m * 2;
    if (n % k >= k / 2 + k % 2) answer += m * 2;
    if (n % k > 0 && n % k < k / 2 + k % 2) answer += m;
    cout << answer << '\n';
}