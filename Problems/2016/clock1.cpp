#include <iostream>

using namespace std;

int main(){
    int n, hour, min, answer = 0;
    string time;
    cin >> n;
    while(n--){
        cin >> time;
        hour = int(time[0] - '0');
        hour = hour * 10 + int(time[1] - '0');
        hour %= 12;
        hour *= 5;
        min = int(time[3] - '0');
        min = min * 10 + int(time[4] - '0');
        hour += min / 12;
        if (abs(hour - min) == 15) answer++;
    }
    cout << answer << '\n';
}     