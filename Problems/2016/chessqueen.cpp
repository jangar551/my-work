#include <iostream>

using namespace std;

bool eatable(int x1, int y1, int x2, int y2){
    if(x1 == x2 || y1 == y2)
        return 1;
    
    if(abs(x1 - x2) == abs(y1 - y2))
        return 1;
    
    // x - -
    // - - - 
    // - - x 
 
    return 0;
}

int main(){
    int x1, y1, x2, y2;
    cin >> x1 >> y1 >> x2 >> y2;
    if (eatable(x1, y1, x2, y2)) cout << "YES\n";
    else cout << "NO\n";

    return 0;
}