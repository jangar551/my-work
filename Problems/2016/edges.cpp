#include <iostream>

using namespace std;

int main(){
    int n, start, end, mx = -1, indx;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> start >> end;
        if (end - start >= mx) mx = end - start, indx = i + 1;
    }

    cout << indx << '\n';

    return 0;
}