#include <iostream>
#include <vector>

using namespace std;

int main(){
    string firstname, lastname;
    vector<int> agecount(121, 0), answer;
    int n, age, mx = -1;
    cin >> n;

    while(n--) cin >> lastname >> firstname >> age, agecount[age]++;
    
    for(int i = 0; i < 121; i++) mx = max(mx, agecount[i]);
    cout << mx << '\n';
    for(int i = 0; i < 121; i++) if(agecount[i] == mx) cout << i << ' ';
    cout << '\n';

    return 0;
}