#include <iostream>

using namespace std;

int main(){
    int degree, hour, minute;
    cin >> degree;
    
    hour = degree / 30;//every 30 degree = 1 hour
    minute = (degree % 30) * 2;

    cout << hour << ' ' << minute << '\n';
}