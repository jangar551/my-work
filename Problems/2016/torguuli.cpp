#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
    int n, dugaar, torguuli, mx = 0;
    cin >> n;
    vector<int> numbers(10000, 0);
    vector<int> sorting;
    
    for(int i = 0; i < n; i++){
        cin >> dugaar >> torguuli;
        numbers[dugaar] += torguuli;
    }

    for(int i = 0; i < 10000; i++)
        mx = max(mx, numbers[i]);

    for(int i = 0; i < 10000; i++)
        if(numbers[i] == mx) sorting.push_back(i);

    sort(sorting.begin(), sorting.end());

    for(int i = sorting.size() - 1; i >= 0; i--)
        cout << sorting[i] << ' ';
    cout << '\n';

    return 0;
}