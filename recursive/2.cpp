#include <iostream>
#include <queue>

using namespace std;

void out(queue<int>& a){
    if(a.empty())
        return;

    cout << a.front() << ' ';

    a.pop();

    out(a);
}

int main(){
    queue<int> a;

    int n, tmp;

    cin >> n;

    for(int i = 0; i < n; i++){
        cin >> tmp;
        
        a.push(tmp);
    }

    out(a);

    return 0;
}