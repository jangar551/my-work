#include <iostream>
#include <vector>

using namespace std;

vector <int> ins_sort(vector <int>& a, int in){
    if(a.size() == 0 || a[a.size() - 1] < in){
        a.push_back(in);
        return a;
    }

    int tmp = a.back();
    a.pop_back();
    a = ins_sort(a, in);
    a.push_back(tmp);

    return a;
}

int main(){
    int n, tmp;

    vector <int> a;

    cin >> n;

    for(int i = 0; i < n; i++){
        cin >> tmp;

        a = ins_sort(a, tmp);
    }

    for(int i = 0; i < n; i++){
        cout << a[i] << ' ';
    }

    return 0;
}