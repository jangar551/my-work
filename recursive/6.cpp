#include <iostream>
#include <vector>

using namespace std;

int fibonacci_n(vector <int>& a, int n){
    if(n == 0)
        return a[a.size() - 1];

    a.push_back(a[a.size() - 1] + a[a.size() - 2]);

    return fibonacci_n(a, n - 1);
}

int main(){
    int n;

    vector <int> a;

    cin >> n;

    a.push_back(0);

    a.push_back(1);

    cout << fibonacci_n(a, n) << '\n';

    return 0;
}