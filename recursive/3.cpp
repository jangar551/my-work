#include <iostream>
#include <stack>

using namespace std;

void out(stack<int>& a){
    if(a.empty())
        return;

    cout << a.top() << ' ';

    a.pop();

    out(a);
}

int main(){
    int n, tmp;

    stack <int> a;
    
    cin >> n;

    for(int i = 0; i < n; i++){
        cin >> tmp;
        a.push(tmp);
    }

    out(a);

    return 0;

}