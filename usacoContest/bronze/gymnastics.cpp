#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

bool consistent(vector<vector<int> >& rank, int a, int b) {
    if (rank[b][0] > rank[a][0])
        swap(a, b);

    for (int i = 0; i < rank[0].size(); i++) 
        if (rank[a][i] <= rank[b][i]) return false;
    
    return true;
}

int main(){
    ifstream in ("gymnastics.in");
    ofstream out ("gymnastics.out");

    int k, n;

    in >> k >> n;

    vector<vector<int> > rank(n, vector<int> (k));

    int tmp;
    for (int i = 0; i < k; i++) 
    for (int j = 0; j < n; j++) 
        in >> tmp, rank[tmp - 1][i] = j;
    
    int answer = 0;

    for (int i = 0; i < n - 1; i++)
    for (int j = i + 1; j < n; j++) 
        if (consistent(rank, i, j)) answer++;

    out << answer << '\n';
}   