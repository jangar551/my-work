#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int search(string now, string color){
    int ans = 0;

    for (int i = 0; i < color.length() - now.length() + 1; i++) {
        int j = 0;
        while (j < now.length() && now[j] == color[i + j]) j++;

        if (j == now.length()) ans++;
    }

    return ans;
}

int main(){
    ifstream in ("whereami.in");
    ofstream out ("whereami.out");
    int len;
    in >> len;


    string color;
    in >> color;

    for (int i = 1; i <= len; i++) {
        int j;
        for (j = 0; j < len - i + 1; j++) {
            string now = "";
            for (int k = 0; k < i; k++) 
                now += color[j + k];
            if (search(now, color) != 1)
                break;
        }
        if (j == len - i + 1) {
            out << i << '\n';
            return 0;
        }
    }
}