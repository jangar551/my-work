#include <iostream>
#include <vector>

using namespace std;

int main(){
    int n;
    cin >> n;

    vector<string> lexo(n);

    for (int i = 0; i < n; i++) 
        cin >> lexo[i];

    sort(lexo.begin(), lexo.end());

    for (int i = 0; i < n; i++) {
        cout << lexo[i] << ", ";
    }    
    cout << '\n';
    return 0;
}