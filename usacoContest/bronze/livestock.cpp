#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;

unordered_map<string, vector<string> > map;
vector<vector<string> > possible;

bool check(vector<string>& name) {
    if (map.find(name[0]) != map.end()){
        if (map[name[0]].size() != 1 || map[name[0]][0] != name[1]) 
            return 0;
    }

    for (int i = 1; i < name.size() - 1; i++) {
        if (map.find(name[i]) != map.end()){
            if (map[name[i]].size() == 1 && (map[name[i]][0] != name[i - 1] && map[name[i]][0] != name[i + 1]))
                return 0;
            if (map[name[i]].size() == 2 && ((map[name[i]][0] != name[i - 1] && map[name[i]][0] != name[i + 1]) || (map[name[i]][1] != name[i - 1] && map[name[i]][1] != name[i + 1])))
                return 0;//ene zavaan hudarsan
        }
    }

    return 1;
}

void ferment(int s, vector<string> name){
    if(s == name.size()){
        if (check(name)) 
            possible.push_back(name);
        
        return;
    }

    for(int i = s; i < name.size(); i++){
        swap(name[i], name[s]);
        ferment(s + 1, name);
        swap(name[i], name[s]);
    }
}


string ordered[] = {"Beatrice", "Belinda", "Bella", "Bessie", "Betsy", "Blue", "Buttercup", "Sue"}; 

int main(){
    ifstream in ("lineup.in");
    ofstream out ("lineup.out");

    int n;

    in >> n;

    vector<string> name(8);
    for (int i = 0; i < 8; i++)
        name[i] = ordered[i];

    string a, b;
    for (int i = 0; i < n; i++) {
        in >> a >> b >> b >> b >> b >> b;
        map[a].push_back(b);
        map[b].push_back(a);
    }

    ferment(0, name);

    sort(possible.begin(), possible.end());

    for (int i = 0; i < 8; i++)
        out << possible[0][i] << '\n';
    
    return 0;
}