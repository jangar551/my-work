#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

vector<bool> condition(5);

bool ifpass(int num) {
    if (condition[0] && num % 2 != 1) return false;
    if (condition[1] && num % 2 != 0) return false;
    if (condition[2] && num % 3 != 0) return false;
    if (condition[3] && num % 5 != 0) return false;
    if (condition[4] && num % 10 != 0) return false;
    return true;
}

int main() {
    int n, m, tmp, answer = 0;
    string test;
    cin >> n;
    vector<int> nums(n);
    for (int i = 0; i < n; i++) cin >> nums[i];
    cin >> m;
    for (int i = 0; i < m; i++){
        cin >> test;
        if (test == "odd") tmp = 0;
        if (test == "even") tmp = 1;
        if (test == "multiple_of_three") tmp = 2;
        if (test == "multiple_of_five") tmp = 3;
        if (test == "ends_with_zero") tmp = 4;
        condition[tmp] = true;
    }  

    for (int i = 0; i < n; i++) if(ifpass(nums[i])) answer++;
    cout << answer << '\n';
    return 0;
}
