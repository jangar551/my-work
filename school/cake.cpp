#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
using namespace std;


int main() {
    vector<ll> side(3);
    ll answer, tmp = 0, tmp1 = 0, d;
    cin >> side[0] >> side[1] >> side[2] >> d;
    sort(side.begin(), side.end());
    if (d <= side[2] - side[1]) {
        cout << side[0] * side[1] * (side[2] - d) << '\n';
        return 0;
    } 
    d -= side[2] - side[1];
    side[2] = side[1];     
    if (d <= (side[1] - side[0]) * 2) {
        cout << side[0] * (side[1] - d / 2) * (side[2] - d / 2 - d % 2) << '\n';
        return 0;
    } 
    d -= (side[1] - side[0]) * 2;
    if (d % 3 == 1) tmp = 1;
    if (d % 3 == 2) tmp1 = 1;
    cout << (side[0] - d / 3) * (side[0] - d / 3 - tmp) * (side[0] - d / 3 - tmp1) << '\n';  
    return 0;
}

/* 
    tilt tilt
*/