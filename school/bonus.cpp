#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second
using namespace std;


int main() {
    vector<pair<int, int>> points(4);
    for (int i = 0; i < 4; i++) cin >> points[i].ff >> points[i].ss;
    int length, width;
    for (int i = 0; i < 4; i++) 
    for (int j = 0; j < 4; j++) {
        if (i == j) continue;
        if (points[i].ff == points[j].ff) length = abs(points[i].ss - points[j].ss);
        if (points[i].ss == points[j].ss) width = abs(points[i].ff - points[j].ff);
    }
    cout << length * width << '\n';
    return 0;
}
