/*
ID: jangar51
LANG: C++
TASK: prefix
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>
#define ll long long

using namespace std;
ifstream in ("prefix.in");
ofstream out ("prefix.out");

ll longest(vector<int>& dp, string& bigstr, vector<string>& primitive, ll now, ll& length){
    if(now == length)
        return now - 1;
    if(dp[now] != -1){
        out << now << " we have dp " << dp[now] << '\n';
        return dp[now];
    }
    ll fartest = now - 1;

    for (ll i = 0; i < primitive.size(); i++) {
        int j = 0;
        while ((now + j < length) && (j < primitive[i].length() && primitive[i][j] == bigstr[now + j])) j++;
        out << now + 1 << " <now> " << i + 1 << '\n';
        // cout << primitive[i][j] << ' ' << bigstr[now + j] << '\n';
        if(j == primitive[i].length())
            fartest = max(fartest, longest(dp, bigstr, primitive, now + j, length));
        out << "FAR " << fartest << '\n';
    }
    dp[now] = fartest;
    return fartest;
}

int main(){

    vector<string> primitive;
    string tmp, bigstr = "";

    while(in >> tmp){
        if(tmp == ".")
            break;
        primitive.push_back(tmp);
    }

    while(in >> tmp)
        bigstr += tmp;

    // cout << bigstr << '\n';

    for(int i = 0; i < )

    ll length = bigstr.length();
    vector<int> dp(length, -1);
    out << longest(dp, bigstr, primitive, 0, length) + 1 << '\n';
    // for(int i = 0; i < length; i++)
    //     cout << dp[i] << ' ';
    // cout << '\n';
}