#include <iostream>
#include <fstream>
#include <vector>   

using namespace std;

struct student {
        string name;
        int age;
        int point;
    };

pair <string, int> findlong(vector <student> students){
    int longest = 0;
    string name;
    for(int i = 0; i < students.size(); i++){
        if(students[i].name.length() > longest){
            longest = students[i].name.length();
            name = students[i].name;
        }
    }

    return make_pair(name, longest);
}

pair <string, int> findyoung(vector <student> students){
    int age = students[0].age;
    string name = students[0].name;
    for(int i = 0; i < students.size(); i++){
        if(students[i].age < age){
            age = students[i].age;
            name = students[i].name;
        }
    }

    return make_pair(name, age);
}

pair <string, int> findhighestpoint(vector <student> students){
    int point = 0;
    string name;
    for(int i = 0; i < students.size(); i++){
        if(students[i].point > point){
            point = students[i].point;
            name = students[i].name;
        }
    }

    return make_pair(name, point);
}

int main(){

    ifstream in ("input.txt");

    int n;

    in >> n;

    vector <student> students (n);

    for(int i = 0; i < n; i++){
        in >> students[i].name >> students[i].age >> students[i].point;
    }

    cout << findlong(students).first << " Hamgiin urt nertei " << findlong(students).second << " usegtei\n" << findyoung(students).first << " Hamgiin zaluu: " << findyoung(students).second << " nastai \n" << findhighestpoint(students).first << " Hamgiin undur onootoi: "<< findhighestpoint(students).second << " onootoi\n";  


}