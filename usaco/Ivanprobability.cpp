#include <iostream>
#include <vector>

using namespace std;

#define mod 1000000007

int possibility(int n){
    if(n == 0 || n == 1)
        return 1;
    
    return possibility(n - 1) + possibility(n - 2);
}

int main(){
    int n, m;

    cin >> n >> m;

    if(n <= 3 || m <= 3){
        if(n == 3|| m == 3){
            cout << 8 << '\n';
            return 0;
        }
        if(n == 2|| m == 2){
            cout << 6 << '\n';
            return 0;
        }
        cout << 2 << '\n';
        return 0;
    }

    cout << ((possibility(n - 3) + possibility(m - 3) + possibility(n - 2) + possibility(m - 2)) * 2) % mod << '\n';
}