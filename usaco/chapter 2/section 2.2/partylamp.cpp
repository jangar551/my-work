/*
ID: jangar51
LANG: C++
TASK: lamps
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

int main(){
    ifstream in ("lamps.in");
    ofstream out ("lamps.out");
    int n, c, tmp = 0;

    cin >> n >> c;
    vector<int> location(n, -1);
    while(cin >> tmp){
        if (tmp == -1)
            break;
        location[tmp - 1] = 1;
    }
    while(cin >> tmp){
        if (tmp == -1)
            break;
        location[tmp - 1] = 0;
    }
    int arr[] = { 8, 4, 2, 1 }; 

    for (int comb = 0; comb <= c % 16; comb++){
        for (int i = 0; i < n; i++){
            if (location[i] != -1){
                location[i] = 1; 
            }
        }
        for (int i = 1; i <= n; i++)
        cout << endl;
    }   
}