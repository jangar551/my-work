/*
ID: jangar51
LANG: C++
TASK: preface
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

char roman[] = { 'I', 'V', 'X', 'L', 'C', 'D', 'M' };
vector<int> answer(7, 0);

int sizes(int a){
    if(a == 0)
        return 0;

    return 1 + sizes(a / 10);
}

int power(int a, int b){
    if(b == 1)
        return a;
    if(b == 0) 
        return 1;
    if(b % 2 == 1)
        return a * power(a * a, b / 2);
    return  power(a * a, b / 2);
}

void counter(int a){
    if(a == 0)
        return;

    int aSize = sizes(a), tmp, keep = a;
    for(int i = aSize; i > 0; i--){
        tmp = a / (power(10, i - 1));
        if(tmp <= 3){
            answer[i * 2 - 2] += tmp;
            a %= power(10, i - 1);
            continue;
        }
        if(tmp > 8){
            answer[i * 2 - 2] += 10 - tmp;
            answer[i * 2]++;
            a %= power(10, i - 1);
            continue;
        }
        answer[i * 2 - 1]++;
        answer[i * 2 - 2] += abs(5 - tmp);
        a %= power(10, i - 1);
        cout << a << '\n';
    }

    counter(keep - 1);
}

int main(){
    ifstream in ("preface.in");
    ofstream out ("preface.out");

    int n;

    in >> n;

    counter(n);

    for(int i = 0; i < answer.size(); i++){
        if(answer[i] != 0){
            out << roman[i] << ' ' << answer[i] << '\n';
        }
    }

    return 0;
}