/*
ID: jangar51
LANG: C++
TASK: subset
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

long long calc(int need, int n, vector<vector<long long> >& cache){
    if(need == 0)
        return 1;
    if(need < 0)
        return 0;
    if(n == 0)
        return 0;
    if(cache[need][n] != -1){
        return cache[need][n];
    }
    return cache[need][n] = calc(need, n - 1, cache) + calc(need - n, n - 1, cache);
}

int main(){
    ifstream in ("subset.in");
    ofstream out ("subset.out");

    int n, half;

    in >> n;

    half = n * (n + 1) / 2;

    if(half % 2 == 1){
        out << 0 << '\n';
        return 0;
    }

    half /= 2;

    vector<vector<long long> > cache(half + 1, vector<long long>(n + 1,  -1));

    out << calc(half, n, cache) / 2 << '\n';

    return 0;
}  