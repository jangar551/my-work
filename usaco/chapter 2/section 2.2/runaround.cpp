/*
ID: jangar51
LANG: C++
TASK: runround
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

void reverse(vector<int>& a){
    for(int i = 0; i < a.size() / 2; i++){
        swap(a[i], a[a.size() - i - 1]);
    }
}

bool isrunround(int n){
    int a = 0, cnt = 0;
    vector<int> digit;
    vector<bool> used(10, 0);

    while(n != 0){
        digit.push_back(n % 10);
        n /= 10;
    }
    reverse(digit);

    vector<bool> been(digit.size() + 1, 0);

    while((digit[a] != 0 && !used[digit[a]]) && ((a + digit[a]) % digit.size() != a && !been[(a + digit[a]) % digit.size()])){
        used[digit[a]] = 1;
        been[(a + digit[a]) % digit.size()] = 1;
        a = (a + digit[a]) % digit.size();
        cnt++;
    }

    return cnt == digit.size();
}

int main(){
    ifstream in ("runround.in");
    ofstream out ("runround.out");
    int n;

    in >> n;

    n++;

    while(!isrunround(n))
        n++;

    out << n << '\n';
    
    return 0;
}  