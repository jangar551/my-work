/*
ID: jangar51
LANG: C++
TASK: nocows
*/
#include <bits/stdc++.h>
#define ll long long
#define pb push_back
#define pp pop_back
#define mp make_pair
#define bb back
#define ff first
#define ss second

using namespace std;
ifstream in ("nocows.in");
ofstream out ("nocows.out");

ll allpos (vector<int> dep, int now, int n, int k, bool bot) {
    if (now == n) {
        if (bot)
            return 1;
        return 0;
    }
    if (now > n)
        return 0;
    ll ans = 0, tmp;
    for (int i = dep.size() - 1; i >= 0; i--) {
        tmp = dep[i];
        dep.pp();
        if (tmp == k) 
            ans += allpos(dep, now + 2, n, k, true);
        else {
            dep.pb(tmp + 1);
            dep.pb(tmp + 1);
            ans += allpos(dep, now + 2, n, k, bot);
            dep.pp();
            dep.pp();
        }  
    }

    return ans % 9901;
}

int main(){
    int n, k, now = 1;
    bool bot = false;
    cin >> n >> k;
    if (k == 1)
        bot = true;
    vector<int> dep;
    dep.pb(1);
    
    cout << (allpos(dep, now, n, k, bot) + 1) % 9901 << '\n'; 
    return 0;
}