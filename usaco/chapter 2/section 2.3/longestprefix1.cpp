/*
ID: jangar51
LANG: C++
TASK: prefix
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;
ifstream in ("prefix.in");
ofstream out ("prefix.out");

int main(){
    vector<string> primitive;
    string tmp, bigstr = "";

    while(in >> tmp){
        if(tmp == ".")
            break;
        primitive.push_back(tmp);
    }

    while(in >> tmp)
        bigstr += tmp;

    int length = bigstr.length();
    vector<bool> get(length + 1, 0);//cangetthere
    get[0] = 1;
    cout << length << '\n';
    for (int i = 0; i < length + 1; i++){
        if (!get[i]) continue;
        for(int k = 0; k < primitive.size(); k++){
            int j = 0;
            while ((i + j <= length) && (j < primitive[k].length() && primitive[k][j] == bigstr[i + j])) 
                j++;
            if (j == primitive[k].length())
                get[i + j] = 1;
        }
    }

    for (int i = length; i >= 0; i--)
        if(get[i]){ 
            out << i << '\n';
            return 0;
        }

    return 0;
}