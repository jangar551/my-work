/*
ID: jangar51
LANG: C++
TASK: frac1
*/
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

ifstream in ("frac1.in");
ofstream out ("frac1.out");

int gcd(int a, int b){
    while(a % b != 0){
        int tmp = a;
        a = b;
        b = tmp % b;
    }

    return b;
}

vector <pair<int, int> > merge(vector<pair<int, int> >& a, vector<pair<int, int> >& b){
    vector<pair<int, int> > ans;
    int p1 = 0, p2 = 0; //pointer
    int sizeA = a.size(), sizeB = b.size();
    while(p1 <= sizeA - 1 && p2 <= sizeB - 1){
        if(a[p1].first * b[p2].second < b[p2].first * a[p1].second){
            ans.push_back(a[p1]);
            p1++;
        }else{ 
            ans.push_back(b[p2]);
            p2++;
        }
    }

    while(p1 <= sizeA - 1){
        ans.push_back(a[p1]);
        p1++;
    }

    while(p2 <= sizeB - 1){
        ans.push_back(b[p2]);
        p2++;
    }

    return ans;
}

void create_fractions(int n){
    vector<pair<int, int> > ans;

    for(int i = 2; i <= n; i++){
        vector<pair<int, int> > tmp;
        tmp.push_back(make_pair(1, i));
        for(int k = 2; k < i; k++){
            if(gcd(i, k) == 1)
                tmp.push_back(make_pair(k, i));
        }
        ans = merge(ans, tmp);
    }

    for(int i = 0; i < ans.size(); i++)
        out << ans[i].first << '/' << ans[i].second << '\n';
}

int main(){
    int n;
    in >> n;

    out << "0/1" << '\n';
    create_fractions(n);
    out << "1/1" << '\n';
    return 0;
}