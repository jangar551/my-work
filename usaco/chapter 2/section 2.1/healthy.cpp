/*
ID: jangar51
LANG: C++
TASK: holstein
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

vector<int> combination(vector<int> now, vector<int>& need, vector<vector<int> >& feeds, int next){
    for(int i = 0; i < now.size(); i++){
        if(now[i] < need[i])
            break;
        if(i == now.size() - 1)
            return now;
    }

    cout << next << '\n';

    for(int i = next; i < feeds.size(); i++){
        vector<int> tmp = now;
        for(int k = 0; k < now.size(); k++)
            tmp[k] += feeds[i][k];
     
        tmp = combination(tmp, need, feeds, i + 1);
        if(tmp[0] < now[0])
            now = tmp;
    }

    return now;
}

int main(){
    ifstream in ("holstein.in");
    ofstream out ("holstein.out");

    int n, g; 
    cin >> n;

    vector<int> need(n), now(n, 0);

    for(int i = 0; i < n; i++)
        cin >> need[i];
    
    
    cin >> g;

    now[0] = g + 1;

    vector<vector<int> > feeds;

    for(int i = 0; i < g; i++){
        vector<int> feed(n);
        for(int k = 0; k < n; k++)
            cin >> feed[k];
            
        feeds.push_back(feed);
    }

    now = combination(now, need, feeds, 0);

    for(int i = 0; i < now[0] + 1; i++){
        cout << now[i] << ' ';
    }

    cout << '\n';

    return 0;
}