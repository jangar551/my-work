/*
ID: jangar51
LANG: C++
TASK: castle
*/
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

ifstream in ("castle.in");
ofstream out ("castle.out");
int movex[4] = {0, -1, 0, 1};
int movey[4] = {-1, 0, 1, 0};
char direction[4] = {'W', 'N', 'E', 'S'};

void wallbreak(vector<vector<int> >& roomColor, vector<vector<int> >& wall, vector<int>& count){
    int height = wall.size(), width = wall[0].size(), maxbreak = 0, answer_h, answer_w, move = 0;
    char dir;
    for(int w = 0; w < width; w++)
    for(int h = height - 1; h >= 0; h--){
        for(int i = 1; i <= 8; i *= 2){
            if((wall[h][w] & i) != 0){
                int tmph = h + movex[move], tmpw = w + movey[move];
                if((tmph < height && tmph >= 0) && (tmpw < width && tmpw >= 0))
                if(roomColor[tmph][tmpw] != roomColor[h][w])
                if(maxbreak < count[roomColor[tmph][tmpw]] + count[roomColor[h][w]]){
                    maxbreak = count[roomColor[tmph][tmpw]] + count[roomColor[h][w]];
                    answer_h = h + 1;
                    answer_w = w + 1;
                    dir = direction[move];
                }
            }
            move++;
        }
        move = 0;
    }

    out << maxbreak << '\n' << answer_h << ' ' << answer_w << ' ' << dir << '\n';
}

int discover(vector<vector<int> >& roomColor, vector<vector<int> >& wall, pair<int, int> pos, int nextcolor){
    // out << "haha: " << pos.first << ' ' << pos.second << '\n';
    int curCount = 1, move = 4;
    roomColor[pos.first][pos.second] = nextcolor;
    for(int i = 8; i >= 1; i /= 2){
        move--;
        if((wall[pos.first][pos.second] & i ) == 0){
            pair<int, int> tmp_cord = pos;
            tmp_cord.first += movex[move];
            tmp_cord.second += movey[move];
            if(roomColor[tmp_cord.first][tmp_cord.second] == -1){
                curCount += discover(roomColor, wall, tmp_cord, nextcolor);
            }   
        }
    }

    return curCount;
}

// void paint(vector<vector<int> >& wall, vector<vector<int> >& roomColor, int height, int width){
//     out << "wew\n";    
//     int nextcolor = 0, max_color = 0;
//     vector<int> count(height * width, 0);
//     for(int h = 0; h < height; h++)
//     for(int w = 0; w < width; w++){
//         if(roomColor[h][w] != -1) continue;
//         pair<int, int> pos = make_pair(h, w);
//         count[nextcolor] += discover(roomColor, wall, pos, nextcolor);
//         max_color = max(max_color, count[nextcolor]);
//         nextcolor++;
//     }

//     out << nextcolor << '\n' << max_color << '\n';

//     wallbreak(roomColor, wall, count);
// }   



int main(){
    int height, width, tmp;
    in >> width >> height;
     
    vector<vector<int> > wall(height, vector <int>(width)), roomColor(height, vector <int>(width, -1));

    for(int h = 0; h < height; h++)
        for(int w = 0; w < width; w++)
            in >> wall[h][w];

    // paint(wall, roomColor, height, width);

    int nextcolor = 0, max_color = 0;
    vector<int> count(height * width, 0);
    for(int h = 0; h < height; h++)
    for(int w = 0; w < width; w++){
        if(roomColor[h][w] != -1) continue;
        pair<int, int> pos = make_pair(h, w);
        count[nextcolor] += discover(roomColor, wall, pos, nextcolor);
        max_color = max(max_color, count[nextcolor]);
        nextcolor++;
    }

    out << nextcolor << '\n' << max_color << '\n';

    wallbreak(roomColor, wall, count);

    return 0;
}