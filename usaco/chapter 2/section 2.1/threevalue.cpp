/*
ID: jangar51
LANG: C++
TASK: sort3
*/
#include <iostream>
#include <vector>
#include <fstream>

using namespace std;    

int compare_switch(vector<int>& input, vector<int>& should, vector<int>& count){
    int answer = 0;
        
    return answer;
}   

int main(){
    ifstream in ("sort3.in");
    ofstream out ("sort3.out");

    int n;

    in >> n;

    vector<int> input(n), count(4, 0), should;

    for(int i = 0; i < n; i++){
        in >> input[i];
        count[input[i]]++;
    }
    
    for(int i = 1; i <= 3; i++)
    for(int k = 0; k < count[i]; k++)
        should.push_back(i);
        

    cout << compare_switch(input, should, count) << '\n';

    return 0;
}