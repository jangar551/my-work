#include <iostream>
#include <vector>

using namespace std;

int main(){
    string a, b;

    cin >> a >> b;

    vector < pair < int, int> > match;

    for(int i = 0; i < a.length() - b.length() + 1; i++){
        if(a[i] == b[0]){
            for(int j = i + 1; j < i + b.length(); j++){
                if(a[j] != b[j - i]){
                    break;
                }
                if(j == i + b.length() - 1){
                    match.push_back(make_pair(i, j));
                }
            }
        }
    }

    for(int i = 0; i < match.size(); i++){
        cout << match[i].first << " to " << match[i].second << '\n';
    }

    return 0;
}