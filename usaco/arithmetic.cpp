/*
ID: jangar51
LANG: C++
TASK: ariprog
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <unordered_map>

using namespace std;


vector <pair<int, int> > answer;

vector <bool> square(125001, 0);

// bool nextfew(int start, int space, int howmany){
//     for(int i = 0; i < howmany; i++){
//         // int tmp = start + space * i;
//         // if( tmp <= 62500){
//             if(!square[start + space * i]){
//                 return 0;
//             }
//         // }
//     }
//     return 1;
// }

void check(vector<int> &bisquare, int n){
    for(int i = 0; i < bisquare.size() - n; i++){
        for(int j = i + 1; j < bisquare.size() - n + 2; j++){
            if(bisquare[i] + n * (bisquare[j] - bisquare[i]) <= 125000){
                for(int k = 0; k < n; k++){
                    if(!square[bisquare[i] + (bisquare[j] - bisquare[i]) * k]){
                        break;
                    }
                    if(k == n - 1){
                        answer.push_back(make_pair(bisquare[j] - bisquare[i], bisquare[i]));
                    }
                }
            }
        }
    }
}

int main(){
    ifstream in("ariprog.in");
    ofstream out("ariprog.out");
    int n, m;
    in >> n >> m;
    vector <int> bisquare;
    for(int i = 0; i <= m; i++){
        for(int j = i; j <= m; j++){
            if(!square[i * i + j * j])
                bisquare.push_back(i * i + j * j);
                square[i * i + j * j] = 1;
        }
    }
    sort(bisquare.begin(), bisquare.end());

    check(bisquare, n);

    sort(answer.begin(), answer.end());

    if (answer.size() == 0)
        out << "NONE\n";
    else{
        for(int i = 0; i < answer.size(); i++){
            out << answer[i].second << ' ' << answer[i].first << '\n';
        }
    }

    return 0;
}
