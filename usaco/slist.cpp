#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace std;

struct student {
    string name;
    int age;
    int point;

    string nickname(){
        string namenpoint, uname = name;

        uname[0] = toupper(uname[0]);

        namenpoint = uname + '_' + to_string(point);

        return namenpoint;
    }
};



int main(){
    ifstream in ("input.txt");
    
    int n ;

    in >> n;

    vector <student> students (n);

    for(int i = 0; i < n; i++){
        in >> students[i].name >> students[i].age >> students[i].point;
    }

    for(int i = 0; i < n; i++){
        cout << students[i].nickname() << endl;
    }


}