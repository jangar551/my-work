/*
ID: jangar51
LANG: C++
TASK: runround
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

int longest(int point, string& sequence, vector<string>& primitive){
    int ans = 0;
    for(int i = 0; i < primitive.size(); i++){
        int tmp = point;
        for(int k = 0; k < primitive[i].length(); k++){
            if(primitive[i][k] == sequence[tmp])
                tmp++;
            else
                break;
        }
        if(tmp == point + primitive[i].length())
            ans = max(longest(tmp, sequence, primitive), ans);
    }
}

int main(){
    ifstream in ("runround.in");
    ofstream out ("runround.out");
    
    vector<string> primitive;
    vector<int> 
    string tmp, sequence;

    while(in >> tmp){
        if(tmp == ".")
            break;
        primitive.push_back(tmp);
    }

    in >> sequence;


    return 0;
}  