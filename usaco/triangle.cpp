/*
ID: jangar51
LANG: C++
TASK: numtri 
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <unordered_map>

using namespace std;

vector < vector <pair<int, int> > > tree;

void besthere(pair<int, int> location){
    if(location.first != tree.size() - 1){
        for(int i = 0; i < 2; i++){
            if(tree[location.first + 1][location.second + i].second < tree[location.first][location.second].second + tree[location.first][location.second].first){
                tree[location.first + 1][location.second + i].second = tree[location.first][location.second].second + tree[location.first][location.second].first;
                // cout << location.first + 1 << ' '  << location.second + i << ' ' << tree[location.first + 1][location.second + i].second << '\n';
                besthere(make_pair(location.first + 1, location.second + i));
            }
        }
    }
}

int mostoflast(){
    int answer = tree[tree.size() - 1][0].second + tree[tree.size() - 1][0].first;
    for(int i = 1; i < tree.size(); i++)
        answer = max(answer, tree[tree.size() - 1][i].second + tree[tree.size() - 1][i].first);

    return answer;
}

int main(){
    ifstream in("numtri.in");
    ofstream out("numtri.out");
    int r;
    in >> r;
    for(int i = 0; i < r; i++){
        vector <pair<int, int> > line;
        for(int j = 0; j < i + 1; j++){
            int tmp;
            in >> tmp;
            line.push_back(make_pair(tmp, 0));
        }
        tree.push_back(line);
    }

    besthere(make_pair(0, 0));

    out << mostoflast() << '\n';
}