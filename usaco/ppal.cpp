/*
ID: jangar51
LANG: C++
TASK: pprime 
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <unordered_map>
#include <sstream>

using namespace std;

int start, finish, finishlen;

vector <int> answer;

ofstream out("pprime.out");
bool is_prime(int number){
    if (number == 2 || number == 3)
        return true;

    if (number % 2 == 0 || number % 3 == 0)
        return false;

    int divisor = 6;
    while (divisor * divisor - 2 * divisor + 1 <= number){

        if (number % (divisor - 1) == 0)
            return false;

        if (number % (divisor + 1) == 0)
            return false;

        divisor += 6;

    }

    return true;

}

void create(string first, int curLen){
    for(int i = 0; i < 10; i++){
        string a = to_string(i);
        a = a + first + a;
        stringstream intnow(a);
        int number;
        intnow >> number;
    
        if(number > finish || a.size() > finishlen)
            break;
        
        if(number >= start && is_prime(number))
            answer.push_back(number);

        create(a, curLen + 2);
    }
}

int main(){
    ifstream in("pprime.in");

    in >> start >> finish;
    finishlen = to_string(finish).size();
    for(int i = 0; i < 10; i++){
        if(is_prime(i) && i >= start)
            answer.push_back(i);

        if(is_prime(i * 11) && i * 11 >= start)
            answer.push_back(i * 11);
        string odd, even;
        odd = to_string(i);
        even = odd + odd;
        create(odd, 1);
        create(even, 2);
    }

    sort(answer.begin(), answer.end());

    for(int i = 0; i < answer.size(); i++)
        out << answer[i] << '\n';

    return 0;
}