/*
ID: jangar51
LANG: C++
TASK: dualpal
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

vector <int> changebasemirrored(int input, int to){
    vector <int> changed;
    while(input != 0){
        changed.push_back(input % to);
        input /= to;
    }
    return changed; 
}

bool isitpalindrominmulticase(int input){
    int howmny = 0;
    for(int i = 2; i <= 10; i++){
        vector <int> numbers = changebasemirrored(input, i);
        for(int j = 0; j < numbers.size() / 2; j++){
            if(numbers[j] != numbers[numbers.size() - j - 1]){
                break;
            }
            if(j == numbers.size() - 1){
                howmny++;
            }
        }
        if(howmny == 2){
            return 1;
        }
    }

    return 0;
}

vector <int> ifpalindrominmulticasesave(int input, int howmany){
    int count = 0;
    vector<int> answers(howmany);
    while(count != howmany){
        if(isitpalindrominmulticase(input)){
            answers[count] = input;
            count++;
        }
        input++;
    }

    return answers;
}

int main(){
    ifstream in ("dualpal.in");
    ofstream out ("dualpal.out");
    int firstfew, higherthan;
    in >> firstfew >> higherthan;
    vector <int> answers = ifpalindrominmulticasesave(higherthan + 1, firstfew);
    for(int i = 0; i < firstfew; i++){
        out << answers[i] << '\n';
    }
    return 0;
}