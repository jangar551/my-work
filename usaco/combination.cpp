/*
ID: jangar51
LANG: C++
TASK: combo
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

int intersept(int dailnumber, vector<int> combo, vector<int> mastercombo){
    vector<int> intersept(3);
    int answer = 1;
    for(int dail = 0; dail < 3; dail++){
        int difference = abs(combo[dail] - mastercombo[dail]), difference2 = abs(combo[dail] - mastercombo[dail]) + 4 - dailnumber;
        if(difference < 5){
            intersept[dail] = 5 - difference;
        }

        if(difference2 >= 0){
            intersept[dail] += difference2 + 1;
        }
    }

    for(int i = 0; i < 3; i++){
        answer *= intersept[i];
    }

    return answer;
}

int main(){
    ifstream in ("combo.in");
    ofstream out ("combo.out");

    int dailnumber;

    in >> dailnumber;

    vector <int> combo(3);
    vector <int> mastercombo(3);

    for(int i = 0; i < 3; i++){
        in >> combo[i];
    }

    for(int i = 0; i < 3; i++){
        in >> mastercombo[i];
    }

    if(dailnumber < 5){
        out << pow(dailnumber, 3) << '\n';
    }else{
        out << 250 - intersept(dailnumber, combo, mastercombo) << '\n';
    }
    return 0;
}
