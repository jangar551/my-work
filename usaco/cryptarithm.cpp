/*
ID: jangar51
LANG: C++
TASK: crypt1
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

bool checker(int subject, vector<bool> test){
    while(subject != 0){
        if(subject % 10 != 0){
            if(!test[subject % 10 - 1])
                return 0;
        }else{
            return 0;
        }
        subject /= 10;
    }
    return 1;
}

bool lessthan3(int subject){
    return (subject < 1000 && subject > 99);
}

int answer(vector<int> digits, vector <bool> test){
    int answer = 0, size = digits.size();
    for(int i = 0; i < size; i++){
        for(int i1 = 0; i1 < size; i1++){
            for(int i2 = 0; i2 < size; i2++){
                for(int j = 0; j < size; j++){
                    int first = (digits[i] * 100 + digits[i1] * 10 + digits[i2]) * digits[j];
                    // cout << first << ": " << checker(first, test) << lessthan3(first)<< endl;
                    if(checker(first, test) && lessthan3(first)){
                        for(int j2 = 0; j2 < size; j2++){ 
                            int second = (digits[i] * 100 + digits[i1] * 10 + digits[i2]) * digits[j2];
                            // cout << "----------second-----------" <<second << ": " << checker(second, test) << lessthan3(second) << checker(second * 10 + first, test) << ' ' << second * 10 + first<< endl;
                            if((checker(second, test) && lessthan3(second)) && checker(second * 10 + first, test)){
                                answer++;    
                            }                          
                        }
                    }
                }
            }
        }
        
       
    }

    return answer;
}

int main(){
    ifstream in ("crypt1.in");
    ofstream out ("crypt1.out");
    int howmany;
    in >> howmany;
    vector <bool> test(10);
    vector <int> digits(howmany);
    for(int i = 0; i < howmany; i++){
        in >> digits[i];
        test[digits[i] - 1] = 1;
    }   

    out << answer(digits, test) <<  '\n';
}