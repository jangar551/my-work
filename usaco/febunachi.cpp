#include <iostream>

using namespace std;

int fibunachi(int n){
    if(n == 0 || n ==1)
        return 1;
    return fibunachi(n - 1) + fibunachi(n - 2);
}

int main(){
    int go, now = 0;
    cin >> go;
    // pair<int, int> last = make_pair(0, 1);
    // cout << fibunachi(go, now , last) << '\n';
    cout << fibunachi(go);
    return 0;
}