/*
ID: jangar51
LANG: C++
TASK: wormhole
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <unordered_map>

using namespace std;

int answer = 0;

vector <int> nexthole, connection;

bool bysecond(pair<int, int> a, pair<int, int> b){
    return (a.second < b.second || (a.second == b.second && a.first < b.first));
}

vector<int> next(vector<pair<int, int> > location){
    vector<int> next(location.size(), -1);
    for(int i = 0; i < location.size() - 1; i++){
        if(location[i].second == location[i + 1].second){
            next[i] = i + 1;
        }
    }

    return next;
}

int check(){
    for(int i = 0; i < connection.size(); i++){
        if(nexthole[i] != -1){
            int j = i;
            vector<bool> been(connection.size(), 1);
            while(nexthole[j] != -1 && been[j]){
                been[j] = 0;
                j = connection[nexthole[j]];
            }

            if(!been[j])
                return 1;
        }
    }
    return 0;
}

void possibility(vector <int> index){
    if(index.size() == 2){
        connection[index[0]] = index[1];
        connection[index[1]] = index[0];
        answer += check();
        // cout << "---------------\n";
    }else{
        for(int i = index.size() - 2; i >= 0; i--){
            vector <int> tmp = index;
            connection[index[i]] = index[index.size() - 1];
            connection[index[index.size() - 1]] = index[i];
            cout << index[i] << ' ' << index[index.size() - 1] << '\n';
            swap(tmp[i], tmp[index.size() - 2]);
            tmp.pop_back();
            tmp.pop_back();
            possibility(tmp);
        }
    }
}

int main(){
    ifstream in("wormhole.in");
    ofstream out("wormhole.out");

    int hole;
    in >> hole;

    vector <pair<int, int> > location(hole);
    vector <int> index(hole);

    for(int i = 0; i < hole; i++){
        in >> location[i].first >> location[i].second;
        index[i] = i;
        connection.push_back(0);
    }

    sort(location.begin(), location.end(), bysecond);

    nexthole = next(location);

    possibility(index);

    out << answer << '\n';
}