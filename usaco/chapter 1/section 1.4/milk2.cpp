/*
ID: jangar51
LANG: C++
TASK: milk
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace std;

int getmilkforhowmuch(vector<pair<int, int> > priceandhave, int want, int from){
    int got = 0, forhowmuch = 0;
    for(int i = 0; i < from; i++){
        got += priceandhave[i].second;
        forhowmuch += priceandhave[i].first * priceandhave[i].second;
        if(got >= want){
            forhowmuch -= (got - want) * priceandhave[i].first;
            break;
        }
    }

    return forhowmuch;
}

int main(){
    ifstream in("milk.in");
    ofstream out("milk.out");
    int want, from;
    in >> want >> from;
    vector<pair<int, int> > priceandhave(from);
    for(int i = 0; i < from; i++){
        in >> priceandhave[i].first >> priceandhave[i].second; 
    }

    sort(priceandhave.begin(), priceandhave.end());

    out << getmilkforhowmuch(priceandhave, want, from) << '\n';
    return 0;
}