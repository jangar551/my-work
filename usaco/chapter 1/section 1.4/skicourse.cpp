/*
ID: jangar51
LANG: C++
TASK: skidesign
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cmath>

using namespace std;

int calculate(vector <int> height){
    int price, tmpprice = 0;
    for(int i = 0; i < 83; i++){
        for(int j = 0; j < height.size(); j++){
            if(height[j] < i)
                tmpprice += (i - height[j]) * (i - height[j]);
        
            if(height[j] > i + 17)
                tmpprice += (height[j] - i - 17) * (height[j] - i - 17);
        }
        if(i == 0)
            price = tmpprice;
        price = min(price, tmpprice);
        tmpprice = 0;
    }
    return price;
}

int main(){
    ifstream in("skidesign.in");
    ofstream out("skidesign.out");
    int m;
    in >> m;
    vector <int> height(m);
    for(int i = 0; i < m; i++){
        in >> height[i];
    }
    sort(height.begin(), height.end());
    out << calculate(height) << '\n';
    return 0;
}