/*
ID: jangar51
LANG: C++
TASK: barn1
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>


using namespace std;

int minimize(vector<int> occupiedspacebetween, int board, int cow){
    int lenght = 0;
    for(int i = 0; i < cow - board; i++){
        lenght += occupiedspacebetween[i];
    }
    //cow - board ==> cow - 1 - (board - 1) = cow - 1 - board + 1

    return lenght + cow;
}

int main(){
    ifstream in("barn1.in");
    ofstream out("barn1.out");
    int board, stall, cow;

    in >> board >> stall >> cow;

    vector<int> occupiedstall(cow);
    vector<int> occupiedspacebetween(cow - 1);

    for(int i = 0; i < cow; i++){
        in >> occupiedstall[i]; 
    }

    sort(occupiedstall.begin(), occupiedstall.end());
    //sort occupied stall numbers

    for(int i = 0; i < cow - 1; i++){
        occupiedspacebetween[i] = occupiedstall[i + 1] - occupiedstall[i] - 1;
        //get space between occupied stalls
    }

    sort(occupiedspacebetween.begin(), occupiedspacebetween.end());
    //sort space between occupied stalls

    out << minimize(occupiedspacebetween, board, cow) << '\n';
    
    return 0;
}