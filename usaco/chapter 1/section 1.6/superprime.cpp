/*
ID: jangar51
LANG: C++
TASK: sprime 
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <unordered_map>

using namespace std;

int lenght = 1;

vector<int> answer;

bool isprime(int number){
    if (number == 2 || number == 3)
        return true;

    if (number % 2 == 0 || number % 3 == 0 || number == 1)
        return false;

    int divisor = 6;
    while (divisor * divisor - 2 * divisor + 1 <= number){

        if (number % (divisor - 1) == 0)
            return false;

        if (number % (divisor + 1) == 0)
            return false;

        divisor += 6;

    }

    return true;

}

void rib(int ribs){
    if(ribs / lenght != 0){
        answer.push_back(ribs);
        return;
    }

    int tmp = ribs;

    for(int i = 1; i < 10; i++){
        if(isprime(tmp * 10 + i))
            rib(tmp * 10 + i);
    }
    
}

int main(){
    ifstream in("sprime.in");
    ofstream out("sprime.out");

    int n;

    in >> n;

    while(n > 1){
        lenght *= 10;
        n--; 
    }

    rib(0);

    for(int i = 0; i < answer.size(); i++)
        out << answer[i] << '\n';

    return 0;
}