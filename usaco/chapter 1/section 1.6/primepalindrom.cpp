/*
ID: jangar51
LANG: C++
TASK: pprime 
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <unordered_map>

using namespace std;

long long start, finish;

vector<long long> answer;

bool IsPrime(long long number){
    if (number == 2 || number == 3)
        return true;

    if (number % 2 == 0 || number % 3 == 0)
        return false;

    long long divisor = 6;
    while (divisor * divisor - 2 * divisor + 1 <= number){

        if (number % (divisor - 1) == 0)
            return false;

        if (number % (divisor + 1) == 0)
            return false;

        divisor += 6;

    }

    return true;

}

long long NextPrime(long long a){
    while (!IsPrime(++a));
    return a;
}

bool IsPalindrom(long long a){
    long long tmp = a, reverse = 0;

    while(a != 0){
        reverse = reverse * 10 + a % 10;
        a /= 10;
    }

    if(reverse == tmp)
        return 1;

    return 0;
}

void nextppal(long long last){
    long long next = NextPrime(last);

    // cout << "====" << next << "====\n";

    if(next <= finish){

        if(IsPalindrom(next))
            answer.push_back(next);
        
        nextppal(next);

    }

}

int main(){
    ifstream in("pprime.in");
    ofstream out("pprime.out");
    in >> start >> finish;
    nextppal(start - 1);
    // sort(answer.begin(), answer.end());
    for(long long i = 0; i < answer.size(); i++){
        out << answer[i] << '\n';   
    }
}