/*
ID: jangar51
LANG: C++
TASK: numtri 
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <unordered_map>

using namespace std;

vector < vector <pair<int, int> > > tree;

void lookabove(int line, int index){
    if(line != tree.size()){
        if(index > 0 && index != tree[line].size() - 1)
            tree[line][index].second = max(tree[line - 1][index - 1].second + tree[line - 1][index - 1].first, tree[line - 1][index].second + tree[line - 1][index].first);
        else{    
            if(index > 0)
                tree[line][index].second = tree[line - 1][index - 1].second + tree[line - 1][index - 1].first;
            else
                tree[line][index].second = tree[line - 1][index].second + tree[line - 1][index].first;
        }      
        if(tree[line].size() == index)
            lookabove(line + 1, 0);
        else
            lookabove(line, index + 1);
    }
}

int mostoflast(){
    int answer = tree[tree.size() - 1][0].second + tree[tree.size() - 1][0].first;
    for(int i = 1; i < tree.size(); i++)
        answer = max(answer, tree[tree.size() - 1][i].second + tree[tree.size() - 1][i].first);

    return answer;
}

int main(){
    ifstream in("numtri.in");
    ofstream out("numtri.out");
    int r;
    in >> r;
    for(int i = 0; i < r; i++){
        vector <pair<int, int> > line;
        for(int j = 0; j < i + 1; j++){
            int tmp;
            in >> tmp;
            line.push_back(make_pair(tmp, 0));
        }
        tree.push_back(line);
    }

    lookabove(1, 0);

    out << mostoflast() << '\n';

}