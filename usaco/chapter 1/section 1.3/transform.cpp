/*
ID: jangar51
LANG: C++
TASK: transform
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>


using namespace std;

vector <string> turnright(vector<string> map, vector<string> newmap){
    for(int i = 0; i < map.size(); i++){
        for(int j = 0; j < map.size(); j++){
            newmap[j][map.size()- i - 1] = map[i][j];
        }
    }
    return newmap;
}

vector <string> mirror(vector<string> map, vector<string> newmap){
    for(int i = 0; i < map.size(); i++){
        for(int j = 0; j < map.size(); j++){
            newmap[i][map.size() - 1 - j] = map[i][j];
        }
    }
    return newmap;
}

int main(){
    ifstream in ("transform.in");
    ofstream out ("transform.out");

    int n;

    in >> n;

    vector < string > map(n), change(n), copy(n);

    for(int i = 0; i < n; i++){
        in >> map[i];
        copy[i] = map[i];
    }


    for(int i = 0; i < n; i++){
        in >> change[i];
    }

    for(int i = 0; i < 3; i++){
        copy = turnright(copy, map);
        if(copy == change){
            out << i + 1 << endl;
            return 0;
        }
    }

    if(change == mirror(map, map)){
        out << 4 << endl;
        return 0;
    }

    copy = mirror(map, map);

    for(int i = 0; i < 3; i++){
        copy = turnright(copy, map);
        if(change == copy){
            out << 5 << endl;
            return 0;
        }
    }

    if(change == map){
        out << 6 << endl;
        return 0;
    }else{
        out << 7 << endl;
    }

    return 0;
}