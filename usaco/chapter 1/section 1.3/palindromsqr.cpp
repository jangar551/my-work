/*
ID: jangar51
LANG: C++
TASK: palsquare
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

bool ifpalindrom(vector <int> numbers){
    for(int i = 0; i < numbers.size(); i++){
        if(numbers[i] != numbers[numbers.size() - i - 1]){
            return 0;
        }
    }

    return 1;
}

vector <int> changebase(int input, int to){
    vector <int> changed;
    while(input != 0){
        changed.push_back(input % to);
        input /= to;
    }

    return changed;
}

int main(){
    ifstream in ("palsquare.in");
    ofstream out ("palsquare.out");
    int base;
    in >> base;
    for(int i = 1; i <= 300; i++){
        if(ifpalindrom(changebase(i * i, base))){
            vector <int> changed1 = changebase(i, base);
            vector <int> changed2 = changebase(i * i, base);
            for(int i = changed1.size() - 1; i >= 0 ; i--){
                if(changed1[i] > 9){
                    char a = changed1[i] + 55;
                    out << a;
                }else{
                    out << changed1[i];
                }
            }

            out << ' ';

            for(int i = changed2.size() - 1; i >= 0 ; i--){
                if(changed2[i] > 9){
                    char a = changed2[i] + 55;
                    out << a;
                }else{
                    out << changed2[i];
                }
            }

            out << '\n';
        }
    }
}