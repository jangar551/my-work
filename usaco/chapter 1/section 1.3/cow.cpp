/*
ID: jangar51
LANG: C++
TASK: milk2
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>


using namespace std;

int main(){
    ifstream in ("milk2.in");
    ofstream out ("milk2.out");
    int n, p1, p2;
    in >> n;
    vector<pair<int, int> > timetable(n);

    for(int i = 0; i < n; i++){
        in >> timetable[i].first >> timetable[i].second;
    } 

    sort(timetable.begin(), timetable.end());

    p1 = timetable[0].first;
    p2 = timetable[0].second;
    int most = p2 - p1, least = 0;
    for(int i = 1; i < n; i++){
        if (p2 >= timetable[i].first){
            p2 = max(p2, timetable[i].second);
            most = max(most, p2 - p1);
        }else{
            least = max(timetable[i].first - p2, least);  
            p1 = timetable[i].first;
            p2 = timetable[i].second; 
        }
    }

    out << most << ' ' << least << endl;
    return 0;
}