/*
ID: jangar51
LANG: C++
TASK: friday
*/
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int main(){
    ifstream in ("friday.in");
    ofstream out ("friday.out", ios_base::app);

    vector <int> week (7, 0);
    int n, firstofmonth = 0;
    in >> n;
    int year = 1900;
    for(int i = 0; i < n; i++){//
        for(int j = 1; j <= 12; j++){//
            week[(firstofmonth + 5) % 7]++;
            if((j == 4 || j == 6 )||(j == 9 || j == 11)){//
                firstofmonth = (firstofmonth + 2) % 7;//
            }else{
                if(j == 2){//
                    if(((year + i) % 100 != 0 && (year + i) % 4 == 0) || (year + i) % 400 == 0){ //
                        firstofmonth = (firstofmonth + 1) % 7;//
                    }
                }else{
                    firstofmonth = (firstofmonth + 3) % 7;//
                }   
            }      
        }
    }


    out << week[5] << ' ';

    out << week[6] << ' ';
    for(int i = 0; i < 4; i++){
        out << week[i] << ' ';
    }
//monday

    out << week[4] << endl;
    

    return 0;
}