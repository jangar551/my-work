/*
ID: jangar51
LANG: C++
TASK: ride
*/
#include <iostream>
#include <fstream>

using namespace std;

int multiply(string a){
    int result = 1;
    for(int i = 0; i < a.length(); i++){
        int tmp = a[i];
        result = result * (tmp - 64) % 47;
    }
    return result;
}

int main(){
    ifstream in ("ride.in");
    ofstream out ("ride.out", ios_base::app);
    string a, b;

    in >> a >> b;

    if(multiply(a) == multiply(b)){
        out << "GO\n";
    }else{
        out << "STAY\n";
    }
    return 0;
}