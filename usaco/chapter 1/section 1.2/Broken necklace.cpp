/*
ID: jangar51
LANG: C++
TASK: beads
*/
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int main(){
    ifstream in ("beads.in");
    ofstream out ("beads.out");
    int n, mx = 0, white = 0, str = 0, last = 0, one = 0, combst;
    string neck;
    in >> n >> neck;
    char now = 'w';
    neck += neck;
    n += n;

    for(int i  = 0; i < n / 2; i++){
        if(neck[i] != 'w'){
            combst = i;
            now = neck[i];
            break;
        }
        if(i == n / 2 - 1){
            cout << n / 2 << endl;
        }
    }

    for(int i = 0; i < n; i++){
        if(neck[i] == 'w'){
            white++;
            if(white + str >= n / 2){
                out << n / 2 << endl;
                return 0;
            }
        }else{
            if(neck[i] == now){
                str += white + 1;
                if(str >= n / 2){
                    out << n / 2 << endl;
                    return 0;
                }
            }else{
                str += white;
                if(last + str - one >= n / 2){
                    out << n / 2 << endl;
                    return 0;
                }
                mx = max(mx, last + str - one);
                combst = i;
                one = white;
                last = str;
                str = 1 + white;
                now = neck[i];
            }

            white = 0;
        }

    }

    out << mx << endl;

    return 0;
}