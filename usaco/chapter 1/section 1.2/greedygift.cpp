/*
ID: jangar51
LANG: C++
TASK: gift1
*/
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;



int main(){
    ifstream in ("gift1.in");
    ofstream out ("gift1.out", ios_base::app);

    int n;

    in >> n;

    vector < pair<string, int> > human (n);

    for(int i = 0; i < n; i++){
        in >> human[i].first;
        human[i].second = 0;
    }

    for(int i = 0; i < n; i++){
        int mon, giv;
        string me;
        in >> me >> mon >> giv;
        if(giv != 0){
            vector <string> reciever (giv);
            for(int i = 0; i < giv; i++){
                in >> reciever[i];
            }

            for(int i = 0; i < n; i++){
                for(int j = 0; j < giv; j++){
                    if(human[i].first == reciever[j]){
                        human[i].second += mon / giv;
                    }

                }
                if(human[i].first == me){
                    human[i].second += mon % giv;
                    human[i].second -= mon;
                }
            }
        }
        
    }

    for(int i =0; i < n; i++){
        out << human[i].first << ' ' << human[i].second << '\n'; 
    }

}