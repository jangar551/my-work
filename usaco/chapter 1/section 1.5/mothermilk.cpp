/*
ID: jangar51
LANG: C++
TASK: milk3 
*/
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <unordered_map>

using namespace std;

vector <bool> havebeen(202020, 0);

vector <int> limit(3, 0);

vector <int> answer;

void possibility(vector <int> now){
    for(int i = 0; i < 3; i++){
        if(now[i] > 0){
            for(int j = i + 1; j < i + 3; j++){
                int to = j % 3;
                vector <int> tmp = now;
                if(limit[to] < now[to] + now[i]){
                    tmp[i] -= limit[to] - tmp[to];
                    tmp[to] = limit[to];
                }else{
                    tmp[to] += tmp[i];
                    tmp[i] = 0;
                }
                if(!havebeen[tmp[0] * 10000 + tmp[1] * 100 + tmp[2]]){
                    havebeen[tmp[0] * 10000 + tmp[1] * 100 + tmp[2]] = 1;
                    if(tmp[0] == 0){         
                        answer.push_back(tmp[2]);
                    }      
                    possibility(tmp);
                }
                
            }
        }
    }
}

int main(){
    ifstream in("milk3.in");
    ofstream out("milk3.out");
    in >> limit[0] >> limit[1] >> limit[2];
    vector<int> now (3, 0);
    now[2] = limit[2];
    possibility(now);
    sort(answer.begin(), answer.end());
    for(int i = 0; i < answer.size() - 1; i++)
        out << answer[i] << ' ';
    out << answer[answer.size() - 1] << '\n';
}